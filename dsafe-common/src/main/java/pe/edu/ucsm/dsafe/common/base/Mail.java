package pe.edu.ucsm.dsafe.common.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

public class Mail {

    private Properties mailConfig = new Properties();

    public void enviarEmail(String correo, String titulo, String mensaje) throws MessagingException {
        Message message = new MimeMessage(getSession());

        message.addRecipient(RecipientType.TO, new InternetAddress(correo));
        message.addFrom(new InternetAddress[]{new InternetAddress(mailConfig.getProperty("mail.user"))});
        message.setSubject(titulo);
        message.setContent(mensaje, "text/html; charset=ISO-8859-1");
        Transport.send(message);
    }

    public void enviarEmailConCopia(String correo, String cc, String bcc,
            String titulo, String mensaje) throws MessagingException {
        Message message = new MimeMessage(getSession());

        message.addRecipient(RecipientType.TO, new InternetAddress(correo));
        if (cc != null && cc.length() > 0) {
            message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc));
        }
        if (bcc != null && bcc.length() > 0) {
            message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc));
        }
        message.addFrom(new InternetAddress[]{new InternetAddress(mailConfig.getProperty("mail.user"))});
        message.setSubject(titulo);
        message.setContent(mensaje, "text/html; charset=ISO-8859-1");

        Transport.send(message);
    }

    private Session getSession() {
        fetchConfig();
        Authenticator authenticator = new Authenticator();

        Properties properties = mailConfig;
        properties.setProperty("mail.smtp.submitter", authenticator.getPasswordAuthentication().getUserName());


        return Session.getInstance(properties, authenticator);
    }

    private class Authenticator extends javax.mail.Authenticator {

        private PasswordAuthentication authentication;

        public Authenticator() {
            String username = mailConfig.getProperty("mail.user");
            String password = mailConfig.getProperty("mail.clave");
            authentication = new PasswordAuthentication(username, password);
        }

        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
            return authentication;
        }
    }

    /**
     * Open a specific text file containing mail server
     * parameters, and populate a corresponding Properties object.
     */
    private void fetchConfig() {
        InputStream input = null;
        try {
            //If possible, one should try to avoid hard-coding a path in this
            //manner; in a web application, one should place such a file in
            //WEB-INF, and access it using ServletContext.getResourceAsStream.
            //Another alternative is Class.getResourceAsStream.
            //This file contains the javax.mail config properties mentioned above.
            input = this.getClass().getResourceAsStream("/mail.properties");
            mailConfig.load(input);
        } catch (IOException ex) {
            System.err.println("Cannot open and load mail server properties file.");
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException ex) {
                System.err.println("Cannot close mail server properties file.");
            }
        }
    }
}
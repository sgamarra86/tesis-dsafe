package pe.edu.ucsm.dsafe.common.base;

public class Constants {
    public final static String FORMATO_PNG = "png";
    public final static String FORMATO_JPG = "jpg";
    public final static String FORMATO_FECHA = "dd/MM/yyyy";
    public final static String FORMATO_FECHA_HORA = "dd/MM/yyyy HH:mm";
    public final static String FORMATO_FECHA_ISO8601 = "yyyy-MM-dd";
    public final static String FORMATO_HORA = "HH:mm";
    public final static String LOCALE_ES = "ES";
    public final static String MSG_ERROR_AUTHENTICATION = "msgErrorAuthentication";
    
    /**
     * REDIRECT
     */
    public static final String REDIRECT_WEB_LOGIN = "";
    public static final String REDIRECT_WEB_PRINCIPAL = "main.html";
    
    public static final short FIRMA_REQUERIDA = (short)0;
    public static final short FIRMA_REALIZADA = (short)1;
    
    /**
     * TIPOS DE NOTIFICACION
     */
    public static final short DOCUMENTO_REGISTRADO = (short)0;
    public static final short DOCUMENTO_FIRMADO = (short)1;
    public static final short DOCUMENTO_REPORTADO = (short)2;
    public static final short DOCUMENTO_ELIMINADO = (short)3;
    
    /**
     * TAMAÑOS DE LISTAS
     */
    public static final int MAIN_LISTA_MAX = 5;
    public static final int GENERAL_LISTA_MAX = 4;
    
    /**
     * TIPOS DE DATO
     */
    public static final String FORMATO_PDF = "application/pdf";
}

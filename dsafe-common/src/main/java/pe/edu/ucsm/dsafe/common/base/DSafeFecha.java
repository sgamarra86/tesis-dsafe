/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.common.base;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Sergio Gamarra 2
 */
public class DSafeFecha {
    public static String standard(Date date){
        return new SimpleDateFormat(Constants.FORMATO_FECHA).format(date);
    }
    
    public static String standard(Calendar date){
        return new SimpleDateFormat(Constants.FORMATO_FECHA).format(date.getTime());
    }
    
    public static String standard_hora(Date date){
        return new SimpleDateFormat(Constants.FORMATO_FECHA_HORA).format(date);
    }
    
    public static String standard_hora(Calendar date){
        return new SimpleDateFormat(Constants.FORMATO_FECHA_HORA).format(date.getTime());
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.common.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sergio Gamarra
 */
public class DSafeMessage implements Serializable  {
    
    public static enum MessageType {SUCCESS, WARNING, ERROR}
    private MessageType messageType;
    private List<String> messages;
    private Serializable data;
    private Boolean success;

    public DSafeMessage() {
        messages = new ArrayList<String>();
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        if(messageType == MessageType.SUCCESS) {
            setSuccess((Boolean) true);
        }
        else {
            setSuccess((Boolean) false);
        }
        this.messageType = messageType;
    }

    public List getMessages() {
        return messages;
    }

    public void addMessages(String message) {
        this.messages.add(message);
    }

    public Serializable getData() {
        return data;
    }

    public void setData(Serializable data) {
        this.data = data;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public boolean isEmpty() {
        if(this.messages.isEmpty()) {
            return true;
        }
        else {
            return false;
        }
    }    
}
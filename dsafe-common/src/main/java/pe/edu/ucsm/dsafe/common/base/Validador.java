/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.common.base;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Pattern;


/**
 *
 * @author Sergio Gamarra
 */
public class Validador {

    private static final String regexAlfanumerico = "^[a-zA-Z0-9]{%i,}$";
    private static final String regexAlfanumerico2 = "^[a-zA-Z0-9]*$";
    private static final String regexSoloLetras = "^[a-zA-ZÑñ\\s]*";
    private static final String regexSoloNumeros ="^([0-9])*$";
    private static final String regexEmail = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String regexMAC="^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$";

    public static boolean noNuloNoVacio(String test) {
        return (test != null) && !(test.trim().isEmpty());
    }

    public static boolean noNulo(Object test) {
        return (test != null);
    }

    public static boolean esAlfanumerico(String test, int longitudMinima) {
        return (Pattern.matches(String.format(regexAlfanumerico, longitudMinima), test));
    }
    
    public static boolean esAlfanumerico(String test) {
        return (Pattern.matches(String.format(regexAlfanumerico2), test));
    }
  
    public static boolean esEmail(String test) {
        return (Pattern.matches(regexEmail, test));
    }

    public static boolean esCero(Integer test) {
        return (test != null) && (test == 0);
    }

    public static boolean esMayorCero(Integer test) {
        return (test != null) && (test > 0);
    }

    public static boolean esMayorIgualCero(Integer test) {
        return (test != null) && (test >= 0);
    }

    public static boolean esCero(BigInteger test) {
        return (test != null) && (test.compareTo(BigInteger.ZERO) == 0);
    }

    public static boolean esMayorCero(BigInteger test) {
        return (test != null) && (test.compareTo(BigInteger.ZERO) > 0);
    }

    public static boolean esMayorCero(BigDecimal test)
    {
        return (test != null) && (test.compareTo(BigDecimal.ZERO) > 0);
    }
     
    public static boolean listaNoVacia(List test) {
        return (test != null && !test.isEmpty());
    }

    public static boolean esFecha(String test, String formatoFecha) {
        boolean correcto = false;
        try {
            if ((test != null) && !(test.trim().isEmpty())) {
                SimpleDateFormat dateFormat = new SimpleDateFormat(formatoFecha);
                dateFormat.parse(test);
                correcto = true;
            }
        } catch (Exception e) {
            correcto = false;
        }
        return correcto;

    }
    
    //Ademas de comprobar formato de fecha, comprueba que la fecha no sea mayor a la actual
    public static boolean esFechaAnteriorAHoy(String test, String formatoFecha) {
        boolean correcto = false;
        try {
            if ((test != null) && !(test.trim().isEmpty())) {
                SimpleDateFormat dateFormat = new SimpleDateFormat(formatoFecha);
                Calendar calTest=new GregorianCalendar();
                calTest.setTime(dateFormat.parse(test));
                if(calTest.after(Calendar.getInstance()))
                {correcto=false;}
                else
                {correcto = true;}
            }
        } catch (Exception e) {
            correcto = false;
        }
        return correcto;

    }
    
     public static boolean esArchivoVacio(long test) {
       boolean correcto=false;
       if(test==0)
       {
           correcto=true;
       }
       return correcto;
    }

    public static boolean esDNI(String test)
    {
        boolean correcto=false;
        if(contieneSoloNumeros(test)&& test.length()==8)
            correcto=true;
        return correcto;
    }
    public static boolean esLibretaMilitar(String test)
    {
        String regexAlphaNumeric="^[a-zA-Z0-9]*$";
        boolean correcto=false;
        if(!noNuloNoVacio(test))
            return correcto;
        if(Pattern.matches(regexAlphaNumeric, test)&& test.length()==9)
            correcto=true;
        return correcto;
    }
    public static boolean esCarnetExtranjeria(String test)
    {
        boolean correcto=false;
        if(contieneSoloNumeros(test)&& test.length()==12)
            correcto=true;
        return correcto;
    }
    public static boolean esPasaporte(String test)
    {
        boolean correcto=false;
        if(contieneSoloNumeros(test)&& test.length()==7)
            correcto=true;
        return correcto;
    }
    public static boolean esNumColegiaturaMedica(String test)
    {
        boolean correcto=false;
        if(contieneSoloNumeros(test)&& test.length()==6)
            correcto=true;
        return correcto;
    }
    public static boolean esNumeroPlaca(String test) {
        String regexNumeroPlaca = "^([A-Z0-9]+\\-[A-Z0-9]+)$";
        return (Pattern.matches(regexNumeroPlaca, test));
    }

     
    public static boolean esMAC(String test) {
        return (Pattern.matches(regexMAC, test));
    }
    
   
    
    public static boolean contieneSoloLetras(String test) {
        if (noNuloNoVacio(test)) {
            return (Pattern.matches(regexSoloLetras, test));
        } else {
            return false;
        }
    }
    
    public static boolean contieneSoloNumeros(String test)
    {
         if (noNuloNoVacio(test)) {
            return (Pattern.matches(regexSoloNumeros, test));
        } else {
            return false;
        }
    }
}

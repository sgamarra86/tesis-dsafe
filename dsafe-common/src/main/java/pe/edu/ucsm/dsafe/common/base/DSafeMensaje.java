/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.common.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sergio Gamarra
 */
public class DSafeMensaje implements Serializable {
    
    public static enum TipoMensaje {SUCCESS, WARNING, ERROR}
    private TipoMensaje tipo;
    private List<String> mensajes;

    public DSafeMensaje() {
        mensajes = new ArrayList<String>();
    }
    
    /**
     * @return the tipo
     */
    public TipoMensaje getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(TipoMensaje tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the mensajes
     */
    public List<String> getMensajes() {
        return mensajes;
    }

    /**
     * @param mensajes the mensajes to set
     */
    public void setMensajes(List<String> mensajes) {
        this.mensajes = mensajes;
    }

    public void addMensaje(String mensaje){
        this.mensajes.add(mensaje);
                
    }    
}

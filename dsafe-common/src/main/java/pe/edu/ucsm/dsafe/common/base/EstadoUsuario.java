/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.common.base;

/**
 *
 * @author Sergio Gamarra
 */
public enum EstadoUsuario {

    DESCONECTADO((short) 0), CONECTADO((short) 1), OCUPADO((short) 2), AUSENTE((short) 3);
    private final short val;

    private EstadoUsuario(short val) {
        this.val = val;
    }

    public short getVal() {
        return val;
    }
}

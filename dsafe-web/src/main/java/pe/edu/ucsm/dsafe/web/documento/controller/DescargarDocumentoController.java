/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.documento.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pe.edu.ucsm.dsafe.common.base.Constants;
import pe.edu.ucsm.dsafe.dataaccess.model.DocumentoVO;
import pe.edu.ucsm.dsafe.service.documento.GestionarDocumento;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/descargar_documento")
public class DescargarDocumentoController extends BaseController {

    @Autowired
    private GestionarDocumento gestionarDocumento;

    @RequestMapping(method = RequestMethod.GET)
    public void descargarDocumento(@RequestParam(value = "idDocumento", required = true) Integer idDocumento, HttpServletResponse response) throws IOException {
        DocumentoVO vo = this.gestionarDocumento.obtenerDocumentoPorIdDocumento(idDocumento);
        ByteArrayInputStream in;
        response.setHeader("Content-Disposition", "attachment; filename=\"" + vo.getNombre() + "\"");
        response.setContentType(vo.getExtension());
        if (vo.getExtension().equals(Constants.FORMATO_PDF)) {
            response.setContentLength(vo.getArchivoPdf().length);
            in = new ByteArrayInputStream(vo.getArchivoPdf());
        } else {
            response.setContentLength(vo.getArchivo().length);
            in = new ByteArrayInputStream(vo.getArchivo());
        }
        IOUtils.copyLarge(in, response.getOutputStream());

        response.flushBuffer();
    }
}

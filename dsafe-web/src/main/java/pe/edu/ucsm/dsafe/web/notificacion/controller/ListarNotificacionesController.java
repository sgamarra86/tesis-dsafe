/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.notificacion.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pe.edu.ucsm.dsafe.service.documento.GestionarDocumento;
import pe.edu.ucsm.dsafe.service.notificacion.GestionarNotificacion;
import pe.edu.ucsm.dsafe.service.notificacion.dto.ListarNotificacionDTO;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/listar_notificaciones")
public class ListarNotificacionesController extends BaseController {

    private static final String REDIRECCIONAR = "redirect:/listar_notificaciones.html";
    private static final String NOREDIRECCIONAR = "listar_notificaciones";
    private static final String NOTIFICACIONES = "notificaciones";
    @Autowired
    private GestionarDocumento gestionarDocumento;
    @Autowired
    private GestionarNotificacion gestionarNotificacion;

    @ModelAttribute(NOTIFICACIONES)
    public List initNotificaciones() {
        List<ListarNotificacionDTO> notificaciones;
        Integer[] idDocumentos = this.gestionarDocumento.obtenerIdDocumentosPorIdUsuario(this.getDatosUsuario().getId());
        if (idDocumentos.length > 0) {
            notificaciones = this.gestionarNotificacion.obtenerNotificacionesPorIdDocumentos(idDocumentos);
        } else {
            notificaciones = new ArrayList<ListarNotificacionDTO>();
        }
        return notificaciones;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String listarNotificaciones() {
        return NOREDIRECCIONAR;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.documento.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.edu.ucsm.dsafe.dataaccess.model.DocumentoVO;
import pe.edu.ucsm.dsafe.dataaccess.model.ObservadoVO;
import pe.edu.ucsm.dsafe.service.documento.GestionarDocumento;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/obtener_observados")
public class ObtenerObservadosController extends BaseController{
    
    @Autowired
    private GestionarDocumento gestionarDocumento;
    
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    List obtenerObservados(
            @RequestParam(value = "idDocumento", required = true) Integer idDocumento) {
        List<String> observadores = new ArrayList<String>();
        DocumentoVO documento = this.gestionarDocumento.obtenerDocumentoPorIdDocumento(idDocumento);
        for (ObservadoVO obs : documento.getObservados()) {
            String nombre_completo = obs.getUsuario().getNombres() + " " + obs.getUsuario().getApellidos();
            observadores.add(nombre_completo);
        }
        return observadores;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.documento.bean;

import java.security.Security;
import javax.annotation.PostConstruct;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.stereotype.Component;

/**
 *
 * @author Sergio Gamarra
 */
@Component
public class RegistrarBouncyCastle {
    @PostConstruct
    public void initIt() throws Exception {
        System.out.println("INICIALIZANDO EL BEAN!!!!!!!!!!!");
        Security.addProvider(new BouncyCastleProvider());
    }
}

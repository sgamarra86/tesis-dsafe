/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.documento.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pe.edu.ucsm.dsafe.common.base.Constants;
import pe.edu.ucsm.dsafe.common.base.DSafeFecha;
import pe.edu.ucsm.dsafe.dataaccess.model.DocumentoVO;
import pe.edu.ucsm.dsafe.dataaccess.model.FirmaVO;
import pe.edu.ucsm.dsafe.dataaccess.model.ObservadoVO;
import pe.edu.ucsm.dsafe.service.documento.GestionarDocumento;
import pe.edu.ucsm.dsafe.service.documento.dto.FirmarDocumentoDTO;
import pe.edu.ucsm.dsafe.service.documento.dto.ListarDocumentoDTO;
import pe.edu.ucsm.dsafe.service.firma.GestionarFirma;
import pe.edu.ucsm.dsafe.service.notificacion.GestionarNotificacion;
import pe.edu.ucsm.dsafe.service.observado.GestionarObservado;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/listar_documento")
public class ListarDocumentoController extends BaseController {

    private static final String REDIRECCIONAR = "redirect:/listar_documento.html";
    private static final String NOREDIRECCIONAR = "listar_documento";
    private static final String PENDIENTES = "pendientes";
    private static final String FIRMADOS = "firmados";
    private static final String COMPARTIDOS = "compartidos";
    private static final String OBSERVADOS = "observados";
    private static final String FIRMAR_DOCUMENTO = "firmarDocumento";
    @Autowired
    private GestionarFirma gestionarFirma;
    @Autowired
    private GestionarObservado gestionarObservado;
    @Autowired
    private GestionarDocumento gestionarDocumento;
    @Autowired
    private GestionarNotificacion gestionarNotificacion;

    //TODO: MOVER ESTOS MEDOTOS AL SERVICIO
    @ModelAttribute(PENDIENTES)
    public List initListaPendientes() {
        List<ListarDocumentoDTO> pendientes = new ArrayList<ListarDocumentoDTO>();
        List<FirmaVO> firmas = this.gestionarFirma.obtenerFirmasPendientesPorIdUsuario(this.getDatosUsuario().getId());
        DocumentoVO doc;
        ListarDocumentoDTO dto;
        for (FirmaVO vo : firmas) {
            dto = new ListarDocumentoDTO();
            doc = this.gestionarDocumento.obtenerDocumentoPorIdDocumento(vo.getDocumento().getId());
            dto.setId(doc.getId());
            dto.setNombre(doc.getNombre());
            dto.setFecha(new SimpleDateFormat(Constants.FORMATO_FECHA).format(doc.getFecha().getTime()));
            dto.setFirmasNecesarias(doc.getFirmas().size());
            dto.setFirmasRealizadas(this.obtenerCantidadFirmasRealizadas(doc.getFirmas()));
            dto.setDestinatarios(doc.getObservados().size());
            dto.setDescripcion(doc.getDescripcion());
            dto.setReportado(this.gestionarNotificacion.esDocumentoReportado(doc.getId()));
            pendientes.add(dto);
        }
        return pendientes;
    }

    @ModelAttribute(FIRMADOS)
    public List initListaFirmados() {
        List<ListarDocumentoDTO> firmados = new ArrayList<ListarDocumentoDTO>();
        List<FirmaVO> firmas = this.gestionarFirma.obtenerFirmasRealizadasPorIdUsuario(this.getDatosUsuario().getId());
        DocumentoVO doc;
        ListarDocumentoDTO dto;
        for (FirmaVO vo : firmas) {
            dto = new ListarDocumentoDTO();
            doc = this.gestionarDocumento.obtenerDocumentoPorIdDocumento(vo.getDocumento().getId());
            dto.setId(doc.getId());
            dto.setNombre(doc.getNombre());
            dto.setFecha(new SimpleDateFormat(Constants.FORMATO_FECHA).format(vo.getFecha().getTime()));
            dto.setFirmasNecesarias(doc.getFirmas().size());
            dto.setFirmasRealizadas(this.obtenerCantidadFirmasRealizadas(doc.getFirmas()));
            dto.setDestinatarios(doc.getObservados().size());
            dto.setDescripcion(doc.getDescripcion());
            dto.setReportado(this.gestionarNotificacion.esDocumentoReportado(doc.getId()));
            firmados.add(dto);
        }
        return firmados;
    }

    @ModelAttribute(OBSERVADOS)
    public List initListaObservados() {
        List<ListarDocumentoDTO> observados = new ArrayList<ListarDocumentoDTO>();
        List<ObservadoVO> observadosVO = this.gestionarObservado.obtenerObservadosPorIdUsuario(this.getDatosUsuario().getId());
        DocumentoVO doc;
        ListarDocumentoDTO dto;
        for (ObservadoVO vo : observadosVO) {
            dto = new ListarDocumentoDTO();
            doc = this.gestionarDocumento.obtenerDocumentoPorIdDocumento(vo.getDocumento().getId());
            dto.setId(doc.getId());
            dto.setNombre(doc.getNombre());
            dto.setFecha(DSafeFecha.standard(doc.getFecha()));
            dto.setFirmasNecesarias(doc.getFirmas().size());
            dto.setFirmasRealizadas(this.obtenerCantidadFirmasRealizadas(doc.getFirmas()));
            dto.setDestinatarios(doc.getObservados().size());
            dto.setDescripcion(doc.getDescripcion());
            dto.setReportado(this.gestionarNotificacion.esDocumentoReportado(doc.getId()));
            observados.add(dto);
        }
        return observados;
    }

    @ModelAttribute(COMPARTIDOS)
    public List initListaCompartidos() {
        return null;
    }

    @ModelAttribute(FIRMAR_DOCUMENTO)
    public FirmarDocumentoDTO initFirmarDocumento() {
        return new FirmarDocumentoDTO();
    }

    @RequestMapping(method = RequestMethod.GET)
    public String vista(@RequestParam(value = "tab", required = false) Integer tab, Model model, HttpServletRequest request) {
        System.out.println("ListarDocumentoController - Get View");
        request.getSession().setAttribute("tab", tab);
        return NOREDIRECCIONAR;
    }

    private Integer obtenerCantidadFirmasRealizadas(Set<FirmaVO> firmas) {
        Integer cantidad = 0;
        for (FirmaVO vo : firmas) {
            if (vo.getEstado() == (short) 1) {
                cantidad++;
            }
        }
        return cantidad;
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.main.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import pe.edu.ucsm.dsafe.service.autenticacion.dto.UserDetailsDTO;
import pe.edu.ucsm.dsafe.service.contacto.GestionarContacto;
import pe.edu.ucsm.dsafe.service.contacto.dto.ContactoDTO;
import pe.edu.ucsm.dsafe.service.documento.GestionarDocumento;
import pe.edu.ucsm.dsafe.service.firma.GestionarFirma;
import pe.edu.ucsm.dsafe.service.notificacion.GestionarNotificacion;
import pe.edu.ucsm.dsafe.service.observado.GestionarObservado;
import pe.edu.ucsm.dsafe.service.notificacion.dto.ListarNotificacionDTO;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/main")
@SessionAttributes({"userDetails"})
public class MainController extends BaseController {

    private static final String REDIRECCIONAR = "redirect:/main.html";
    private static final String NOREDIRECCIONAR = "main";
    private static final String CONTACTOS = "contactos";
    private static final String PENDIENTES = "pendientes";
    private static final String OBSERVADOS = "observados";
    private static final String NOTIFICACIONES = "notificaciones";
    @Autowired
    private GestionarNotificacion gestionarNotificacion;
    @Autowired
    private GestionarContacto gestionarContacto;
    @Autowired
    private GestionarFirma gestionarFirma;
    @Autowired
    private GestionarObservado gestionarObservado;
    @Autowired
    private GestionarDocumento gestionarDocumento;

    @ModelAttribute(NOTIFICACIONES)
    public List initNotificaciones() {
        List<ListarNotificacionDTO> notificaciones;
        Integer[] idDocumentos = this.gestionarDocumento.obtenerIdDocumentosPorIdUsuario(this.getDatosUsuario().getId());
        if (idDocumentos.length > 0) {
            notificaciones = this.gestionarNotificacion.obtenerNotificacionesPorIdDocumentosResumido(idDocumentos);
        } else {
            notificaciones = new ArrayList<ListarNotificacionDTO>();
        }
        return notificaciones;
    }

    @ModelAttribute(CONTACTOS)
    public List initContactos() {
        List<ContactoDTO> contactos = new ArrayList<ContactoDTO>();
        List<ContactoDTO> aux = this.gestionarContacto.obtenerSolicitudesRecibidasPorEmail(this.getDatosUsuario().getEmail());
        int i = 0;
        for (ContactoDTO contacto : aux) {
            if (i < 2) {
                contactos.add(contacto);
                i++;
            } else {
                break;
            }
        }

        return contactos;
    }

    @ModelAttribute(PENDIENTES)
    public List initPendientesResumen() {
        return this.gestionarFirma.obtenerFirmasPendientesPorIdUsuarioResumido(this.getDatosUsuario().getId());
    }

    @ModelAttribute(OBSERVADOS)
    public List initObservdo() {
        return this.gestionarObservado.obtenerObservadosPorIdUsuarioResumido(this.getDatosUsuario().getId());
    }

    @RequestMapping(method = RequestMethod.GET)
    public String vista(Model model, HttpServletRequest request) {
        System.out.println("MainController - Get View");

        UserDetailsDTO userDetailsDTO = this.getDatosUsuario();
        model.addAttribute("userDetails", userDetailsDTO);

        return NOREDIRECCIONAR;
    }
}

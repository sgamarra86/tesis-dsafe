/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.index.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pe.edu.ucsm.dsafe.common.base.DSafeMensaje;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/index")
public class IndexController {

    private static final String REDIRECCIONAR = "redirect:/index.html";
    private static final String NOREDIRECCIONAR = "index";
    private static final String MENSAJE = "mensaje";
    private static final String MSG_ERROR_AUTHENTICATION = "msgErrorAuthentication";

    @ModelAttribute(MENSAJE)
    public DSafeMensaje initMensaje(HttpServletRequest request) {
        DSafeMensaje mensaje = new DSafeMensaje();
//        String error = (String) request.getSession().getAttribute(MSG_ERROR_AUTHENTICATION);
//        request.getSession().removeAttribute(MSG_ERROR_AUTHENTICATION);
//        if (error != null) {
//            mensaje.setTipo(DSafeMensaje.TipoMensaje.ERROR);
//            mensaje.addMensaje(error);
//        }
        
        if (request.getSession().getAttribute(MSG_ERROR_AUTHENTICATION) != null) {
            mensaje.setTipo(DSafeMensaje.TipoMensaje.ERROR);
            mensaje.addMensaje((String) request.getSession().getAttribute(MSG_ERROR_AUTHENTICATION));
            request.getSession().removeAttribute(MSG_ERROR_AUTHENTICATION);
        }
        //FIX: deberia ser un solo mensaje, uno viene del login otro del registrar.
        if (request.getSession().getAttribute(MENSAJE) != null) {
            mensaje = (DSafeMensaje) request.getSession().getAttribute(MENSAJE);
            request.getSession().removeAttribute(MENSAJE);
        }
        return mensaje;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String obtenerIndexView(Model model, HttpServletRequest request) {
        return NOREDIRECCIONAR;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.notificacion.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.edu.ucsm.dsafe.common.base.DSafeMensaje;
import pe.edu.ucsm.dsafe.dataaccess.model.FirmaVO;
import pe.edu.ucsm.dsafe.service.firma.GestionarFirma;
import pe.edu.ucsm.dsafe.service.mail.GestionarMail;
import pe.edu.ucsm.dsafe.service.mail.dto.DatosMailDTO;
import pe.edu.ucsm.dsafe.service.notificacion.GestionarNotificacion;
import pe.edu.ucsm.dsafe.service.notificacion.dto.RegistrarNotificacionDTO;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/registrar_notificacion")
public class RegistrarNotificacionController extends BaseController {

    private static final String REDIRECCIONAR = "redirect:/ver_documento.html?idDocumento=";
    private static final String REGISTRAR_NOTIFICACION = "registrarNotificacion";
    @Autowired
    private GestionarFirma gestionarFirma;
    @Autowired
    private GestionarMail gestionarMail;
    @Autowired
    private GestionarNotificacion gestionarNotificacion;

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    String registrarNotificacion(@Valid @ModelAttribute(REGISTRAR_NOTIFICACION) RegistrarNotificacionDTO notificacionDTO,
            HttpServletRequest request, BindingResult result) {
        DSafeMensaje mensaje = new DSafeMensaje();
        notificacionDTO.setIdUsuario(this.getDatosUsuario().getId());
        notificacionDTO.setNomUsuario(this.getDatosUsuario().getNombres() + " " + this.getDatosUsuario().getApellidos());

        Integer id = this.gestionarNotificacion.registrarNotificacion(notificacionDTO);

        //Preparar mensaje.
        mensaje.setTipo(DSafeMensaje.TipoMensaje.SUCCESS);
        mensaje.addMensaje("El reporte ha sido registrado satisfactoriamente.");
        request.getSession().setAttribute(MENSAJE, mensaje);

        //Enviar mail de reporte.
        Integer[] idUsuarios = this.obtenerIdUsuarios(notificacionDTO.getIdDocumento());
        DatosMailDTO datosMail = new DatosMailDTO(idUsuarios, notificacionDTO.getIdDocumento(), notificacionDTO.getNomDocumento(), notificacionDTO.getDescripcion());
        this.gestionarMail.enviarMailDocumentoReportado(datosMail);
        
        return notificacionDTO.getIdDocumento().toString();

    }
    
    private Integer[] obtenerIdUsuarios(Integer idDocumento){
        List<Integer> idUsuarios = new ArrayList<Integer>();
        List<FirmaVO> firmas = this.gestionarFirma.obtenerFirmasRequeridasPorIdDocumento(idDocumento);
        for (FirmaVO firma : firmas) {
            idUsuarios.add(firma.getUsuario().getId());
        }        
        
        //Agregar observadores, otra lista de observadores y agregarla a la de idUsuarios.
        
        return idUsuarios.toArray(new Integer[idUsuarios.size()]);
    }
}

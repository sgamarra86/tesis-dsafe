/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.grupo.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pe.edu.ucsm.dsafe.service.grupo.GestionarGrupo;
import pe.edu.ucsm.dsafe.service.grupo.dto.RegistrarGrupoDTO;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/listar_grupo")
public class ListarGrupoController extends BaseController{  
    private static final String REDIRECCIONAR = "redirect:/listar_grupo.html";
    private static final String NOREDIRECCIONAR = "listar_grupo";
    private static final String REGISTRAR_GRUPO = "registrarGrupo";
    private static final String GRUPOS = "grupos";

    @Autowired
    private GestionarGrupo gestionarGrupo;
    
    @ModelAttribute(REGISTRAR_GRUPO)
    public RegistrarGrupoDTO initRegistrarGrupoDTO() {
        return new RegistrarGrupoDTO();
    }
    
    @ModelAttribute(GRUPOS)
    public List initListaGrupos() {
        //TODO: aca sacar el id de usuario de la sesion :D, luego de login.
        return gestionarGrupo.obtenerGruposPorUsuario(5);
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String vista(Model model, HttpServletRequest request) {
        System.out.println("ListarGrupoController - Get View");
        return NOREDIRECCIONAR;
    }
}

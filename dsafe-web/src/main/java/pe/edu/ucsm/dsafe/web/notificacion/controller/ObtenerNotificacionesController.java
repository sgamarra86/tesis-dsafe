/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.notificacion.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.edu.ucsm.dsafe.service.documento.GestionarDocumento;
import pe.edu.ucsm.dsafe.service.notificacion.GestionarNotificacion;
import pe.edu.ucsm.dsafe.service.notificacion.dto.ListarNotificacionDTO;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/obtener_notificaciones")
public class ObtenerNotificacionesController extends BaseController {
    @Autowired
    private GestionarDocumento gestionarDocumento;
    @Autowired
    private GestionarNotificacion gestionarNotificacion;

    @RequestMapping(value = "/documento", method = RequestMethod.POST)
    public @ResponseBody
    List obtenerNotificacionesPorDocumentos(
            @RequestParam(value = "idDocumentos", required = true) Integer[] idDocumentos) {       
        List<ListarNotificacionDTO> notificaciones = this.gestionarNotificacion.obtenerNotificacionesPorIdDocumentos(idDocumentos);
        return notificaciones;
    }
    
    @RequestMapping(value = "/usuario", method = RequestMethod.POST)
    public @ResponseBody
    List obtenerNotificacionesPorUsuario() {       
        Integer[] idDocumentos = this.gestionarDocumento.obtenerIdDocumentosPorIdUsuario(this.getDatosUsuario().getId());
        List<ListarNotificacionDTO> notificaciones = this.gestionarNotificacion.obtenerNotificacionesPorIdDocumentos(idDocumentos);
        return notificaciones;
    }
}

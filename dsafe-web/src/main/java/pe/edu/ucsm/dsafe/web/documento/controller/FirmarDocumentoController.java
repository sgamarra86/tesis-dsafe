/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.documento.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pe.edu.ucsm.dsafe.common.base.Constants;
import pe.edu.ucsm.dsafe.common.base.DSafeMensaje;
import pe.edu.ucsm.dsafe.dataaccess.model.FirmaVO;
import pe.edu.ucsm.dsafe.service.documento.GestionarDocumento;
import pe.edu.ucsm.dsafe.service.documento.dto.FirmarDocumentoDTO;
import pe.edu.ucsm.dsafe.service.documento.dto.RegistrarFirmaRealizadaDTO;
import pe.edu.ucsm.dsafe.service.firma.GestionarFirma;
import pe.edu.ucsm.dsafe.service.firma.dto.FirmarDocumentoRequerimientoDTO;
import pe.edu.ucsm.dsafe.service.firma.dto.FirmarDocumentoResultadoDTO;
import pe.edu.ucsm.dsafe.service.mail.GestionarMail;
import pe.edu.ucsm.dsafe.service.mail.dto.DatosMailDTO;

import pe.edu.ucsm.dsafe.service.notificacion.GestionarNotificacion;
import pe.edu.ucsm.dsafe.service.notificacion.dto.RegistrarNotificacionDTO;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/firmar_documento")
public class FirmarDocumentoController extends BaseController {

    private static final String REDIRECCIONAR = "redirect:/listar_documento.html";
    private static final String FIRMAR_DOCUMENTO = "firmarDocumento";
    @Autowired
    private GestionarFirma gestionarFirma;
    @Autowired
    private GestionarNotificacion gestionarNotificacion;
    @Autowired
    private GestionarMail gestionarMail;

    @RequestMapping(method = RequestMethod.POST)
    public String firmarDocumento(@Valid @ModelAttribute(FIRMAR_DOCUMENTO) FirmarDocumentoDTO firmarDTO,
            HttpServletRequest request, BindingResult result) throws IOException {
        DSafeMensaje mensaje = new DSafeMensaje();
        FirmarDocumentoRequerimientoDTO requerimiento = this.cargarRequerimiento(firmarDTO.getPkcs12().getBytes(), firmarDTO.getPassword(), request.getServletContext());
        Integer[] idDocumentos = this.stringToInteger(firmarDTO.getDocumentos());
        for (Integer idDocumento : idDocumentos) {
            //Firmar Documento por Usuario
            requerimiento.setIdDocumento(idDocumento);
            FirmarDocumentoResultadoDTO resultado = this.gestionarFirma.firmarDocumento(requerimiento);
            if (resultado.isCorrecto()) {
                //Registrar firma realizada.
                this.registrarFirmaRealizada(idDocumento, resultado.getFirma());

                //Registrar notificacion de firma realizada.
                this.registrarNotificacion(idDocumento, resultado.getDocumento());

                //Enviar correos a firmantes.
                this.enviarMailFirmaRealizada(idDocumento, resultado.getDocumento());

                //Crear mensaje Success
                mensaje.setTipo(DSafeMensaje.TipoMensaje.SUCCESS);
                mensaje.addMensaje("El documento [ " + resultado.getDocumento() + " ] ha sido firmado satisfactoriamente.");
            } else {
                //Crear mensaje Error
                mensaje.setTipo(DSafeMensaje.TipoMensaje.ERROR);
                mensaje.addMensaje("El documento [ " + resultado.getDocumento() + " ] no ha sido firmado. " + resultado.getMensaje());
            }
        }

        request.getSession().setAttribute(MENSAJE, mensaje);

        if (request.getHeader("Referer").contains("ver_documento")) {
            return "redirect:" + request.getHeader("Referer");
        } else {
            return REDIRECCIONAR;
        }
    }

    private FirmarDocumentoRequerimientoDTO cargarRequerimiento(byte[] pkcs12, String password, ServletContext servletContext) throws IOException {
        FirmarDocumentoRequerimientoDTO requerimiento = new FirmarDocumentoRequerimientoDTO();
        requerimiento.setPkcs12(pkcs12);
        requerimiento.setPkcs12Password(password);
        requerimiento.setCertificadoRaiz(IOUtils.toByteArray(servletContext.getResourceAsStream("/crt/caCertRoot.crt")));
        return requerimiento;
    }

    private void registrarFirmaRealizada(Integer idDocumento, byte[] firma) {
        RegistrarFirmaRealizadaDTO rfrDTO = new RegistrarFirmaRealizadaDTO(this.getDatosUsuario().getId(), idDocumento, firma);
        this.gestionarFirma.registrarFirmaRealizada(rfrDTO);
    }

    private void registrarNotificacion(Integer idDocumento, String nomDocumento) {
        RegistrarNotificacionDTO notificacion = new RegistrarNotificacionDTO();
        notificacion.setIdDocumento(idDocumento);
        notificacion.setNomDocumento(nomDocumento);
        notificacion.setIdUsuario(this.getDatosUsuario().getId());
        notificacion.setNomUsuario(this.getDatosUsuario().getNombres() + " " + this.getDatosUsuario().getApellidos());
        notificacion.setTipo(Constants.DOCUMENTO_FIRMADO);
        this.gestionarNotificacion.registrarNotificacion(notificacion);
    }

    private void enviarMailFirmaRealizada(Integer idDocumento, String nomDocumento) {
        List<Integer> idUsuarios = new ArrayList<Integer>();
        List<FirmaVO> firmas = this.gestionarFirma.obtenerFirmasRequeridasPorIdDocumento(idDocumento);
        for (FirmaVO firma : firmas) {
            idUsuarios.add(firma.getUsuario().getId());
        }
        DatosMailDTO datosMail = new DatosMailDTO(idUsuarios.toArray(new Integer[idUsuarios.size()]), idDocumento, nomDocumento,null);
        this.gestionarMail.enviarMailDocumentoFirmado(datosMail);
    }
}

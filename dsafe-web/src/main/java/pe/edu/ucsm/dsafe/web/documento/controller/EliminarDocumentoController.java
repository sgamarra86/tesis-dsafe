/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.documento.controller;

import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pe.edu.ucsm.dsafe.common.base.Constants;
import pe.edu.ucsm.dsafe.common.base.DSafeMensaje;
import pe.edu.ucsm.dsafe.dataaccess.model.DocumentoVO;
import pe.edu.ucsm.dsafe.service.documento.GestionarDocumento;
import pe.edu.ucsm.dsafe.service.notificacion.GestionarNotificacion;
import pe.edu.ucsm.dsafe.service.notificacion.dto.RegistrarNotificacionDTO;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/eliminar_documento")
public class EliminarDocumentoController extends BaseController {

    private static final String REDIRECCIONAR = "redirect:/administrar_documento.html";
    private static final String NOREDIRECCIONAR = "administrar_documento.html";
    @Autowired
    private GestionarDocumento gestionarDocumento;
    @Autowired
    private GestionarNotificacion gestionarNotificacion;

    @RequestMapping(method = RequestMethod.GET)
    public String eliminarDocumento(@RequestParam(value = "idDocumento", required = true) Integer idDocumento, HttpServletResponse response) {
        DSafeMensaje mensaje = new DSafeMensaje();
        DocumentoVO vo = this.gestionarDocumento.obtenerDocumentoPorIdDocumento(idDocumento);
        Integer rows = this.gestionarDocumento.eliminarDocumentoPorIdDocumento(idDocumento);
        if (rows > 0) {
            this.registrarNotificacion(vo.getId(), vo.getNombre());
            mensaje.setTipo(DSafeMensaje.TipoMensaje.SUCCESS);
            mensaje.addMensaje("El documento [ " + vo.getNombre() + " ] ha sido eliminado satisfactoriamente.");
        } else {
            mensaje.setTipo(DSafeMensaje.TipoMensaje.ERROR);
            mensaje.addMensaje("El documento [ " + vo.getNombre() + " ] no ha sido eliminado.");
        }
        return REDIRECCIONAR;
    }

    private void registrarNotificacion(Integer idDocumento, String nomDocumento) {
        RegistrarNotificacionDTO notificacion = new RegistrarNotificacionDTO();
        notificacion.setIdDocumento(idDocumento);
        notificacion.setNomDocumento(nomDocumento);
        notificacion.setIdUsuario(this.getDatosUsuario().getId());
        notificacion.setNomUsuario(this.getDatosUsuario().getNombres() + " " + this.getDatosUsuario().getApellidos());
        notificacion.setTipo(Constants.DOCUMENTO_FIRMADO);
        this.gestionarNotificacion.registrarNotificacion(notificacion);
    }
}

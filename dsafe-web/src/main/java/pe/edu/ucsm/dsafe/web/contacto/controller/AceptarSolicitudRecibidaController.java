/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.contacto.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.edu.ucsm.dsafe.service.contacto.GestionarContacto;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/aceptar_solicitud_recibida")
public class AceptarSolicitudRecibidaController extends BaseController {

    private static final String ID_CONTACTO = "idContacto";
    @Autowired
    private GestionarContacto gestionarContacto;

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    String vista(Model model, @RequestParam(value = ID_CONTACTO, required = true) Integer idContacto,
            HttpServletRequest request) {
        System.out.println("AceptarSolicitudRecibidaController - POST");
        this.gestionarContacto.aceptarSolicitud(this.getDatosUsuario().getId(), idContacto);
        return "aceptado";
    }
}

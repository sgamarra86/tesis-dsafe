/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.documento.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pe.edu.ucsm.dsafe.common.base.Constants;
import pe.edu.ucsm.dsafe.common.base.DSafeMensaje;
import pe.edu.ucsm.dsafe.service.contacto.GestionarContacto;
import pe.edu.ucsm.dsafe.service.contacto.dto.ContactoDTO;
import pe.edu.ucsm.dsafe.service.documento.GestionarDocumento;
import pe.edu.ucsm.dsafe.service.documento.dto.RegistrarDocumentoDTO;
import pe.edu.ucsm.dsafe.service.firma.GestionarFirma;
import pe.edu.ucsm.dsafe.service.mail.GestionarMail;
import pe.edu.ucsm.dsafe.service.notificacion.GestionarNotificacion;
import pe.edu.ucsm.dsafe.service.notificacion.dto.RegistrarNotificacionDTO;
import pe.edu.ucsm.dsafe.service.observado.GestionarObservado;
import pe.edu.ucsm.dsafe.service.usuario.GestionarUsuario;
import pe.edu.ucsm.dsafe.service.documento.validator.RegistrarDocumentoValidator;
import pe.edu.ucsm.dsafe.sign.data.ResultadoPrepararPdfDTO;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/registrar_documento")
public class RegistrarDocumentoController extends BaseController {

    private static final String REDIRECCIONAR = "redirect:/listar_documento.html";
    private static final String NOREDIRECCIONAR = "registrar_documento";
    private static final String CONTACTOS = "contactos";
    private static final String REGISTRAR_DOCUMENTO = "registrarDocumento";
    @Autowired
    private GestionarDocumento gestionarDocumento;
    @Autowired
    private GestionarFirma gestionarFirma;
    @Autowired
    private GestionarContacto gestionarContacto;
    @Autowired
    private GestionarObservado gestionarObservado;
    @Autowired
    private GestionarNotificacion gestionarNotificacion;
    @Autowired
    private GestionarUsuario gestionarUsuario;
    @Autowired
    private GestionarMail gestionarMail;
    @Autowired
    private RegistrarDocumentoValidator registrarDocumentoValidator;
    

    @ModelAttribute(CONTACTOS)
    public List initListaContactos() {
        ContactoDTO dto = new ContactoDTO();
        dto.setId(this.getDatosUsuario().getId());
        dto.setNombres(this.getDatosUsuario().getNombres());
        dto.setApellidos(this.getDatosUsuario().getApellidos());

        List<ContactoDTO> contactos = gestionarContacto.obtenerContactosPorEmail(this.getDatosUsuario().getEmail());
        contactos.add(dto);
        return contactos;
    }

    @ModelAttribute(REGISTRAR_DOCUMENTO)
    public RegistrarDocumentoDTO initRegistrarDocumentoDTO() {
        return new RegistrarDocumentoDTO();
    }

    @RequestMapping(method = RequestMethod.GET)
    public String vista(Model model, HttpServletRequest request) {
        System.out.println("RegistrarDocumentoController - Get View");
        return NOREDIRECCIONAR;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String registrarDocumento(@Valid @ModelAttribute(REGISTRAR_DOCUMENTO) RegistrarDocumentoDTO documentoDTO,
            HttpServletRequest request, BindingResult result) {
        String view = REDIRECCIONAR;
        DSafeMensaje mensaje = new DSafeMensaje();
        this.registrarDocumentoValidator.validate(documentoDTO, result);       
        if (!result.hasErrors()) {

            Integer idDocumento = 0;
            String nomDocumento = documentoDTO.getArchivo().getOriginalFilename();
            Integer idUsuario = this.getDatosUsuario().getId();
            Integer[] idFirmantes = this.stringToInteger(documentoDTO.getIdFirmantes().split(","));
            Integer[] idObservadores =
                    documentoDTO.getIdObservadores().isEmpty() ? new Integer[0]
                    : this.stringToInteger(documentoDTO.getIdObservadores().split(","));
            documentoDTO.setPropietario(idUsuario);

            //Proceso de registro de documento
            if (documentoDTO.getArchivo().getContentType().equals(Constants.FORMATO_PDF)) {
                ResultadoPrepararPdfDTO resultado = this.gestionarDocumento.prepararDocumentoPdf(idFirmantes, documentoDTO.getArchivo().getBytes());
                if (resultado.isCorrecto()) {
                    idDocumento = this.gestionarDocumento.registrarDocumentoPdf(documentoDTO, resultado.getDocumentoPdf());
                } else {
                    mensaje.setTipo(DSafeMensaje.TipoMensaje.ERROR);
                    mensaje.addMensaje("El documento [ " + nomDocumento + " ] no ha sido registraro. " + resultado.getMensaje());
                    request.getSession().setAttribute(MENSAJE, mensaje);
                    return REDIRECCIONAR;
                }
            } else {
                idDocumento = this.gestionarDocumento.registrarDocumento(documentoDTO);
            }

            //Registrar firmas requeridas
            this.gestionarFirma.registrarFirmasRequeridas(idFirmantes, idDocumento);

            //Registrar observados
            this.gestionarObservado.registrarObservados(idObservadores, idDocumento);

            /*Removido ahora se debe agregar al que registra siempre, ya no es automático.
             //Registrar como observador si no se requiere su firma
             if (!firmaUsuarioRequerida(idUsuario, idFirmantes)) {
             this.gestionarObservado.registrarObservado(idUsuario, idDocumento);
             }
             */

            //Registrar notificacion de registro de documento
            this.registrarNotificacion(idDocumento, nomDocumento);


            //Enviar correos.
            this.gestionarMail.enviarMailFirmantes(idFirmantes, nomDocumento, documentoDTO.getMailFirmantes());
            this.gestionarMail.enviarMailObservadores(idObservadores, nomDocumento, documentoDTO.getMailObservadores());

            //Preparar mensaje.
            mensaje.setTipo(DSafeMensaje.TipoMensaje.SUCCESS);
            mensaje.addMensaje("El documento [ " + nomDocumento + " ] ha sido registrado satisfactoriamente.");

        } else {
//            mensaje.setTipo(DSafeMensaje.TipoMensaje.ERROR);
//            mensaje.addMensaje("Se encontraron errores en el formulario.");
            view = NOREDIRECCIONAR;
        }
        request.getSession().setAttribute(MENSAJE, mensaje);
        return view;
    }

    private boolean firmaUsuarioRequerida(Integer idUsuario, Integer[] idUsuarios) {
        for (Integer id : idUsuarios) {
            if (idUsuario.equals(id)) {
                return true;
            }
        }
        return false;
    }

    private void registrarNotificacion(Integer idDocumento, String nomDocumento) {
        RegistrarNotificacionDTO notificacion = new RegistrarNotificacionDTO();
        notificacion.setIdDocumento(idDocumento);
        notificacion.setNomDocumento(nomDocumento);
        notificacion.setIdUsuario(this.getDatosUsuario().getId());
        notificacion.setNomUsuario(this.getDatosUsuario().getNombres() + " " + this.getDatosUsuario().getApellidos());
        notificacion.setTipo(Constants.DOCUMENTO_REGISTRADO);
        this.gestionarNotificacion.registrarNotificacion(notificacion);
    }
}

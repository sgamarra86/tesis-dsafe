/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.usuario.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pe.edu.ucsm.dsafe.common.base.DSafeMensaje;
import pe.edu.ucsm.dsafe.service.mail.GestionarMail;
import pe.edu.ucsm.dsafe.service.usuario.GestionarUsuario;
import pe.edu.ucsm.dsafe.service.usuario.dto.RegistrarUsuarioDTO;
import pe.edu.ucsm.dsafe.service.usuario.validator.RegistrarUsuarioValidator;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/registrar_usuario")
public class RegistrarUsuarioController {

    private static final String REDIRECCIONAR = "redirect:/index.html";
    private static final String NOREDIRECCIONAR = "registrar_usuario";
    private static final String REGISTRAR_USUARIO = "registrarUsuario";
    private static final String MENSAJE = "mensaje";
    @Autowired
    private GestionarUsuario gestionarUsuario;
    @Autowired
    private GestionarMail gestionarMail;
    @Autowired
    private RegistrarUsuarioValidator registrarUsuarioValidator;

    @ModelAttribute(MENSAJE)
    public DSafeMensaje initMensaje(HttpServletRequest request) {
        DSafeMensaje mensaje = new DSafeMensaje();
        if (request.getSession().getAttribute(MENSAJE) != null) {
            mensaje = (DSafeMensaje) request.getSession().getAttribute(MENSAJE);
            request.getSession().removeAttribute(MENSAJE);
        }
        return mensaje;
    }
    
    @ModelAttribute(REGISTRAR_USUARIO)
    public RegistrarUsuarioDTO initRegistrarUsuarioDTO() {
        return new RegistrarUsuarioDTO();
    }

    @RequestMapping(method = RequestMethod.GET)
    public String vista(Model model, HttpServletRequest request) {
        System.out.println("RegistrarUsuarioController - Get View");
        return NOREDIRECCIONAR;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String registrarUsuario(@Valid @ModelAttribute(REGISTRAR_USUARIO) RegistrarUsuarioDTO usuarioDTO,
            HttpServletRequest request, BindingResult result) {
        String view = REDIRECCIONAR;
        DSafeMensaje message = new DSafeMensaje();
        this.registrarUsuarioValidator.validate(usuarioDTO, result);

        if (!result.hasErrors()) {
            int idUsuario = gestionarUsuario.registrarUsuario(usuarioDTO);
            if (idUsuario > 0) {
                message.setTipo(DSafeMensaje.TipoMensaje.SUCCESS);
                message.addMensaje("Registro realizado con éxito.");
                this.gestionarMail.enviarMailNuevoUsuario(idUsuario);
            } else {
                message.setTipo(DSafeMensaje.TipoMensaje.ERROR);
                message.addMensaje("Registro no realizado.");
            }
        } else {
//            message.setTipo(DSafeMensaje.TipoMensaje.ERROR);
//            message.addMensaje("Se encontraron errores en el formulario.");
            view = NOREDIRECCIONAR;
        }

        request.getSession().setAttribute(MENSAJE, message);

        return view;
    }
}

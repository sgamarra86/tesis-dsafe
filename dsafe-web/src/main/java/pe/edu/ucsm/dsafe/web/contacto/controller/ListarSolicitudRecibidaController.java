/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.contacto.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pe.edu.ucsm.dsafe.service.contacto.GestionarContacto;
import pe.edu.ucsm.dsafe.service.contacto.dto.ContactoDTO;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/listar_solicitud_recibida")
public class ListarSolicitudRecibidaController extends BaseController {

    private static final String REDIRECCIONAR = "redirect:/listar_solicitud_recibida.html";
    private static final String NOREDIRECCIONAR = "listar_solicitud_recibida";
    private static final String CONTACTOS = "contactos";
    @Autowired
    private GestionarContacto gestionarContacto;

    @ModelAttribute(CONTACTOS)
    public List initListaContactos() {
        List<ContactoDTO> contactos = gestionarContacto.obtenerSolicitudesRecibidasPorEmail(this.getDatosUsuario().getEmail());
        return contactos;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String vista(Model model, HttpServletRequest request) {
        System.out.println("ListarSolicitudesContactoController - Get View");
        return NOREDIRECCIONAR;
    }
}

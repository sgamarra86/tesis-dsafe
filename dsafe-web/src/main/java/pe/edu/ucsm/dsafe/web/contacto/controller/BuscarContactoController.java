/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.contacto.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pe.edu.ucsm.dsafe.dataaccess.model.UsuarioVO;
import pe.edu.ucsm.dsafe.service.contacto.GestionarContacto;
import pe.edu.ucsm.dsafe.service.contacto.dto.ContactoDTO;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/buscar_contacto")
public class BuscarContactoController extends BaseController {

    private static final String REDIRECCIONAR = "redirect:/buscar_contacto.html";
    private static final String NOREDIRECCIONAR = "buscar_contacto";
    private static final String PARAMETRO = "parametro";
    private static final String CONTACTOS = "contactos";
    @Autowired
    private GestionarContacto gestionarContacto;

    @ModelAttribute(CONTACTOS)
    public List initListaContactos(String parametro) {
        System.out.println("(Modal)Parametro:" + parametro);
        Integer idUsuario = this.getDatosUsuario().getId();
        List<ContactoDTO> contactos_nuevos = new ArrayList<ContactoDTO>();
        List<ContactoDTO> contactos_registrados = new ArrayList<ContactoDTO>();
        
        if (parametro != null) {
            contactos_nuevos = gestionarContacto.obtenerContactosPorParametro(parametro);
            contactos_registrados = gestionarContacto.obtenerContactosPorEmail(this.getDatosUsuario().getEmail());
            contactos_nuevos = this.eliminarContactoDeListaContactos(idUsuario, contactos_nuevos);
            for (ContactoDTO contactoDTO : contactos_registrados) {
                contactos_nuevos = this.eliminarContactoDeListaContactos(contactoDTO.getId(), contactos_nuevos);
            }
        }
        return contactos_nuevos;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String vista(Model model, @RequestParam(value = PARAMETRO, required = false) String parametro,
            HttpServletRequest request) {
        System.out.println("BuscarContactoController - Get View");
        return NOREDIRECCIONAR;
    }
    
    private List<ContactoDTO> eliminarContactoDeListaContactos(Integer idContacto, List<ContactoDTO> contactos){
        List<ContactoDTO> aux = new ArrayList<ContactoDTO>();
        for (ContactoDTO contacto : contactos) {
            if (contacto.getId() != idContacto) {
                aux.add(contacto);
            }
        }
        return aux;
    }
}

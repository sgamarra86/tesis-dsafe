/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.usuario.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/perfil_usuario")
public class PerfilUsuarioController extends BaseController{
    
    private static final String REDIRECCIONAR = "redirect:/perfil_usuario.html";
    private static final String NOREDIRECCIONAR = "perfil_usuario";
    private static final String USUARIO = "usuario";


    @RequestMapping(method = RequestMethod.GET)
    public String vista(Model model, HttpServletRequest request) {
        System.out.println("PerfilUsuarioController - Get View");
        return NOREDIRECCIONAR;
    }
}

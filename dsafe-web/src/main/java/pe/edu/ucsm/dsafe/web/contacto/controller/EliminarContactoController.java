/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.contacto.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.edu.ucsm.dsafe.service.contacto.GestionarContacto;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/eliminar_contacto")
public class EliminarContactoController extends BaseController {

    private static final String ID_CONTACTO = "idContacto";
    @Autowired
    private GestionarContacto gestionarContacto;

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    String eliminarContacto(@RequestParam(value = ID_CONTACTO, required = true) Integer idContacto,
            HttpSession session, HttpServletRequest request) {
        this.gestionarContacto.eliminarContacto(this.getDatosUsuario().getId(), idContacto);
        return "eliminado";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.documento.controller;

import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pe.edu.ucsm.dsafe.common.base.DSafeMensaje;
import pe.edu.ucsm.dsafe.dataaccess.model.DocumentoVO;
import pe.edu.ucsm.dsafe.dataaccess.model.FirmaVO;
import pe.edu.ucsm.dsafe.dataaccess.model.ObservadoVO;
import pe.edu.ucsm.dsafe.service.documento.GestionarDocumento;
import pe.edu.ucsm.dsafe.service.documento.dto.DatosVerificarDocumentoDTO;
import pe.edu.ucsm.dsafe.service.documento.dto.FirmarDocumentoDTO;
import pe.edu.ucsm.dsafe.service.notificacion.dto.RegistrarNotificacionDTO;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/ver_documento")
public class VerDocumentoController extends BaseController {

    private static final String REDIRECCIONAR = "redirect:/listar_documento.html";
    private static final String NOREDIRECCIONAR = "ver_documento";
    private static final String DOCUMENTO = "documento";
    private static final String ID_DOCUMENTO = "idDocumento";
    private static final String FIRMAR_DOCUMENTO = "firmarDocumento";
    private static final String REGISTRAR_NOTIFICACION = "registrarNotificacion";
    @Autowired
    private GestionarDocumento gestionarDocumento;

    @ModelAttribute(DOCUMENTO)
    public DatosVerificarDocumentoDTO initVerDocumentoDTO(Integer idDocumento, HttpServletRequest request) {
        Integer idUsuario = this.getDatosUsuario().getId();
        String path = request.getServletContext().getRealPath("");
        //Potencial cambio para enviar la ruta del certificado root.
        //IOUtils.toByteArray(servletContext.getResourceAsStream("/crt/caCertRoot.crt"));
        return this.gestionarDocumento.verificarDocumentoPorIdDocumento(idDocumento, idUsuario, path);
    }

    @ModelAttribute(FIRMAR_DOCUMENTO)
    public FirmarDocumentoDTO initFirmarDocumento() {
        return new FirmarDocumentoDTO();
    }

    @ModelAttribute(REGISTRAR_NOTIFICACION)
    public RegistrarNotificacionDTO initRegistrarNotificacion() {
        return new RegistrarNotificacionDTO();
    }

    @RequestMapping(method = RequestMethod.GET)
    public String verDocumento(@RequestParam(value = ID_DOCUMENTO, required = true) Integer idDocumento,
            HttpSession session, HttpServletRequest request) {
        System.out.println("VerificarDocumentoController - Get View");

        DSafeMensaje mensaje = new DSafeMensaje();
        DocumentoVO documento = this.gestionarDocumento.obtenerDocumentoPorIdDocumento(idDocumento);
        if (this.esFirmante(documento.getFirmas()) || this.esObservador(documento.getObservados())) {
            return NOREDIRECCIONAR;
        } else {
            mensaje.setTipo(DSafeMensaje.TipoMensaje.ERROR);
            mensaje.addMensaje("Lo sentimos usted no tiene acceso a este documento.");
            request.getSession().setAttribute(MENSAJE, mensaje);
            return REDIRECCIONAR;
        }
    }

    public boolean esFirmante(Set<FirmaVO> firmas) {
        for (FirmaVO firma : firmas) {
            if (firma.getUsuario().getId().equals(this.getDatosUsuario().getId())) {
                return true;
            }
        }
        return false;
    }

    public boolean esObservador(Set<ObservadoVO> observados) {
        for (ObservadoVO obs : observados) {
            if (obs.getUsuario().getId().equals(this.getDatosUsuario().getId())) {
                return true;
            }
        }
        return false;
    }
}
package pe.edu.ucsm.dsafe.web.base.controller;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ModelAttribute;
import pe.edu.ucsm.dsafe.common.base.DSafeMensaje;
import pe.edu.ucsm.dsafe.service.autenticacion.dto.UserDetailsDTO;
import pe.edu.ucsm.dsafe.service.contacto.GestionarContacto;
import pe.edu.ucsm.dsafe.service.documento.dto.PieChartDTO;
import pe.edu.ucsm.dsafe.service.firma.GestionarFirma;
import pe.edu.ucsm.dsafe.service.observado.GestionarObservado;

public class BaseController {

    private MessageSourceAccessor messages;
    private ServletContext servletContext;
//    protected Locale locale = new Locale(Constants.LOCALE_ES);
    protected Locale locale = new Locale("ES");
    protected final String MENSAJE = "mensaje";
    private static final String DATASOURCE = "dataSource";
    private static final String NUM_CONTACTOS = "numContactos";
    private static final String NUM_PENDIENTES = "numPendientes";
    private static final String NUM_FIRMADOS = "numFirmados";
    private static final String NUM_OBSERVADOS = "numObservados";
    @Autowired
    private GestionarContacto gestionarContacto;
    @Autowired
    private GestionarFirma gestionarFirma;
    @Autowired
    private GestionarObservado gestionarObservado;

    @ModelAttribute(MENSAJE)
    public DSafeMensaje initMensaje(HttpServletRequest request) {
        DSafeMensaje mensaje = new DSafeMensaje();
        if (request.getSession().getAttribute(MENSAJE) != null) {
            mensaje = (DSafeMensaje) request.getSession().getAttribute(MENSAJE);
            request.getSession().removeAttribute(MENSAJE);
        }
        return mensaje;
    }

    @ModelAttribute(NUM_CONTACTOS)
    public Integer initNumContactos() {
        return this.gestionarContacto.obtenerSolicitudesRecibidasPorEmail(this.getDatosUsuario().getEmail()).size();
    }

    @ModelAttribute(NUM_PENDIENTES)
    public Integer initNumPendientes() {
        return this.gestionarFirma.obtenerFirmasPendientesPorIdUsuario(this.getDatosUsuario().getId()).size();
    }

    @ModelAttribute(NUM_FIRMADOS)
    public Integer initNumFirmados() {
        return this.gestionarFirma.obtenerFirmasRealizadasPorIdUsuario(this.getDatosUsuario().getId()).size();
    }

    @ModelAttribute(NUM_OBSERVADOS)
    public Integer initNumCompartidos() {
        return this.gestionarObservado.obtenerObservadosPorIdUsuario(this.getDatosUsuario().getId()).size();
    }

    @ModelAttribute(DATASOURCE)
    public String initDataSource() {
        Integer pendientes = this.gestionarFirma.obtenerFirmasPendientesPorIdUsuario(this.getDatosUsuario().getId()).size();
        Integer firmados = this.gestionarFirma.obtenerFirmasRealizadasPorIdUsuario(this.getDatosUsuario().getId()).size();
        Integer observados = this.gestionarObservado.obtenerObservadosPorIdUsuario(this.getDatosUsuario().getId()).size();
        List<PieChartDTO> dataSource = new ArrayList<PieChartDTO>();
        dataSource.add(new PieChartDTO("Pendientes", pendientes));
        dataSource.add(new PieChartDTO("Firmados", firmados));
        dataSource.add(new PieChartDTO("Observados", observados));
        return new Gson().toJson(dataSource);
    }

    @Autowired
    public void setMessages(MessageSource messageSource) {
        messages = new MessageSourceAccessor(messageSource);
    }

    public Locale getLocate() {
        return locale;
    }

    public String getText(String msgKey) {
        return messages.getMessage(msgKey, locale);
    }

    public String getText(String msgKey, String arg) {
        return getText(msgKey, new Object[]{arg});
    }

    public String getText(String msgKey, Object[] args) {
        return messages.getMessage(msgKey, args, locale);
    }

    public ServletContext getServletContext() {
        return servletContext;
    }

    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    public UserDetailsDTO getDatosUsuario() {
        UserDetailsDTO userDetailDTO;
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            userDetailDTO = (UserDetailsDTO) authentication.getDetails();
        } catch (Exception ex) {
            userDetailDTO = null;
        }
        return userDetailDTO;
    }
    
    public Integer[] stringToInteger(String[] sarr) {
        Integer[] iarr = new Integer[sarr.length];
        for (int i = 0; i < sarr.length; i++) {
            iarr[i] = Integer.valueOf(sarr[i]);
        }
        return iarr;
    }
}
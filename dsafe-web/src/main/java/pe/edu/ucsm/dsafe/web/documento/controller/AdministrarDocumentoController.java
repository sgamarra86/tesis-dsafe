/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.documento.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pe.edu.ucsm.dsafe.service.documento.GestionarDocumento;
import pe.edu.ucsm.dsafe.service.documento.dto.ListarDocumentoDTO;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/administrar_documento")
public class AdministrarDocumentoController extends BaseController {

    private static final String REDIRECCIONAR = "redirect:/administrar_documento.html";
    private static final String NOREDIRECCIONAR = "administrar_documento";
    private static final String DOCUMENTOS = "documentos";
    @Autowired
    private GestionarDocumento gestionarDocumento;

    @ModelAttribute(DOCUMENTOS)
    public List initListaDocumentos() {
        List<ListarDocumentoDTO> documentos = this.gestionarDocumento.obtenerDocumentosPorIdUsuario(this.getDatosUsuario().getId());
        return documentos;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getVistaMisDocumentos(Model model, HttpServletRequest request) {
        System.out.println("AdministrarDocumentoController - Get View");
        return NOREDIRECCIONAR;
    }
}
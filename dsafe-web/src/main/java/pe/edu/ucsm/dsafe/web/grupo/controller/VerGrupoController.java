/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.grupo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pe.edu.ucsm.dsafe.service.grupo.GestionarGrupo;
import pe.edu.ucsm.dsafe.service.grupo.dto.VerGrupoDTO;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
public class VerGrupoController extends BaseController{
    private static final String REDIRECCIONAR = "redirect:/listar_grupo.html";
    private static final String NOREDIRECCIONAR = "ver_grupo";
    private static final String ID_GRUPO = "idGrupo";
    private static final String GRUPO = "grupo";

    @Autowired
    private GestionarGrupo gestionarGrupo;
    
    @ModelAttribute(GRUPO)
    public VerGrupoDTO initGrupo(Integer idGrupo) {
        return gestionarGrupo.obtenerGrupoPorId(idGrupo);
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String vista(@RequestParam(value = ID_GRUPO, required = true) Integer idGrupo,
            HttpSession session, HttpServletRequest request) {
        return NOREDIRECCIONAR;
    }
    
}

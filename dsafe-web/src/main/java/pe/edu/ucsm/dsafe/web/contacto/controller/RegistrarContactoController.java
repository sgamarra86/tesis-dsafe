/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.contacto.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.edu.ucsm.dsafe.service.contacto.GestionarContacto;
import pe.edu.ucsm.dsafe.service.mail.GestionarMail;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/registrar_contacto")
public class RegistrarContactoController extends BaseController {

    private static final String ID_CONTACTO = "idContacto";
    @Autowired
    private GestionarContacto gestionarContacto;
    @Autowired
    private GestionarMail gestionarMail;

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    String vista(Model model, @RequestParam(value = ID_CONTACTO, required = true) Integer idContacto,
            HttpServletRequest request) {
        System.out.println("RegistrarContactoController - POST");
        this.gestionarContacto.registrarSolicitud(this.getDatosUsuario().getId(), idContacto);
        this.gestionarMail.enviarMailNuevoContacto(idContacto);
        //TODO: wadafak, seguro lo use en un ajax y el mensaje es irrelevante.
        return "esto registrando";
    }
}

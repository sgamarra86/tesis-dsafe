/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.web.grupo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pe.edu.ucsm.dsafe.common.base.DSafeMessage;
import pe.edu.ucsm.dsafe.service.grupo.GestionarGrupo;
import pe.edu.ucsm.dsafe.service.grupo.dto.RegistrarGrupoDTO;
import pe.edu.ucsm.dsafe.web.base.controller.BaseController;

/**
 *
 * @author Sergio Gamarra
 */
@Controller
@RequestMapping("/registrar_grupo")
public class RegistrarGrupoController extends BaseController {

    private static final String REDIRECCIONAR = "redirect:/listar_grupo.html";
    private static final String NOREDIRECCIONAR = "listar_grupo";
    private static final String REGISTRAR_GRUPO = "registrarGrupo";
    protected final String MENSAJE = "mensaje";
    @Autowired
    private GestionarGrupo gestionarGrupo;
    
    @ModelAttribute(REGISTRAR_GRUPO)
    public RegistrarGrupoDTO initRegistrarGrupoDTO() {
        return new RegistrarGrupoDTO();
    }

    @RequestMapping(method = RequestMethod.GET)
    public String vista(Model model, HttpServletRequest request) {
        System.out.println("RegistrarUsuarioController - Get View");

        return NOREDIRECCIONAR;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String registrarGrupo(@Valid @ModelAttribute(REGISTRAR_GRUPO) RegistrarGrupoDTO grupoDTO,
            HttpServletRequest request, BindingResult result) {
        String view = REDIRECCIONAR;
        DSafeMessage message = new DSafeMessage();
        //TODO: aca agregar el ID del usuario logeado en el grupoDTO
        grupoDTO.setIdUsuario(5);
        int idGrupo = gestionarGrupo.registrarGrupo(grupoDTO);
        if (idGrupo > 0) {
            message.setSuccess(true);
            message.addMessages("Registro realizado con éxito...");
        } else {
            message.setSuccess(false);
            message.addMessages("Registro no realizado...");
        }

        request.getSession().setAttribute(MENSAJE, message);

        return view;
    }
}

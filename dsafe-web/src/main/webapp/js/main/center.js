$(document).ready(function() {
    $("#tblPendientes tr").tooltip({placement: 'right'});
    $("#tblObservados tr").tooltip({placement: 'right'});

    $('#lnkHistorial').click(function() {
        $.ajax({
            dataType: 'text',
            type: 'POST',
            url: 'obtener_notificaciones/usuario.html',
            success: function(data) {
                var DOM = "";
                var notificaciones = JSON.parse(data);
                $.each(notificaciones, function(i, noti) {
                    var estilo = "";
                    switch (noti.tipo) {
                        case 0:
                            estilo = "info";
                            break;
                        case 1:
                            estilo = "info";
                            break;
                        case 2:
                            estilo = "error";
                            break;
                    }
                    DOM += '<tr class=' + estilo + '>';
                    DOM += '<td>' + noti.fecha + '</td>';
                    DOM += '<td>' + noti.nomDocumento + '</td>';
                    DOM += '<td>' + noti.nomUsuario + '</td>';
                    DOM += '<td>' + noti.descripcion + '</td>';
                    DOM += '</tr>';
                });
                $('#tblHistorial tbody').html(DOM);
                $('#mdlHistorial').modal('show');
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
                alert(error);
            }
        });
    });

    $('.a_contacto_aceptar').click(function() {
        var contacto = $(this);
        var nombres = contacto.parents("tr").children("td").eq(0).html();
        //var apellidos = contacto.parents("tr").children("td").eq(3).html();
        if (!confirm('¿Desea aceptar la solicitud de ' + nombres + '?'))
            return false;

        $.ajax({
            dataType: 'text',
            type: 'POST',
            url: 'aceptar_solicitud_recibida.html',
            data: {
                idContacto: contacto.attr('id')
            },
            success: function(data) {
                //contacto.parents("tr").remove();
                location.reload();
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
                alert(error);
            }
        });
    });

    $('.a_contacto_rechazar').click(function() {
        var contacto = $(this);
        var nombres = contacto.parents("tr").children("td").eq(0).html();
        //var apellidos = contacto.parents("tr").children("td").eq(3).html();
        if (!confirm('¿Desea rechazar la solicitud de ' + nombres + '?'))
            return false;

        $.ajax({
            dataType: 'text',
            type: 'POST',
            url: 'rechazar_solicitud_recibida.html',
            data: {
                idContacto: contacto.attr('id')
            },
            success: function(data) {
                //contacto.parents("tr").remove();
                location.reload();
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
                alert(error);
            }
        });
    });
});
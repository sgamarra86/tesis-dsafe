$(document).ready(function() {
    $("#loading").css('visibility', 'hidden');    
    $('#frmRegistrarDocumento').submit(function(){
        $("#loading").css('visibility', 'visible');
       return true; 
    });
    
    $('#inpNomFirmantes').click(function() {
        $('#modalFirmas').modal('show');
    });
    
    $('#inpNomObservadores').click(function() {
        $('#modalObservadores').modal('show');
    });

    $('#selContactos').multiSelect({
        selectableHeader: "<div class='custom-header'><h5>Contactos disponibles<h5></div><input type='text' class='search-input' autocomplete='off' placeholder='Nombres y/o apellidos'>",
        selectionHeader: "<div class='custom-header'><h5>Firmas requeridas<h5></div><input type='text' class='search-input' autocomplete='off' placeholder='Nombres y/o apellidos'>",
        afterInit: function(ms) {
            var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function(e) {
                if (e.which === 40) {
                    that.$selectableUl.focus();
                    return false;
                }
            });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function(e) {
                if (e.which == 40) {
                    that.$selectionUl.focus();
                    return false;
                }
            });
        },
        afterSelect: function() {
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function() {
            this.qs1.cache();
            this.qs2.cache();
        }
    });
    
    $('#selObservadores').multiSelect({
        selectableHeader: "<div class='custom-header'><h5>Contactos disponibles<h5></div><input type='text' class='search-input' autocomplete='off' placeholder='Nombres y/o apellidos'>",
        selectionHeader: "<div class='custom-header'><h5>Observadores requeridos<h5></div><input type='text' class='search-input' autocomplete='off' placeholder='Nombres y/o apellidos'>",
        afterInit: function(ms) {
            var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function(e) {
                if (e.which === 40) {
                    that.$selectableUl.focus();
                    return false;
                }
            });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function(e) {
                if (e.which == 40) {
                    that.$selectionUl.focus();
                    return false;
                }
            });
        },
        afterSelect: function() {
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function() {
            this.qs1.cache();
            this.qs2.cache();
        }
    });

    $('.ms-container').css('width', '530');
    $('.search-input').css('width', '225');

    $('#modalFirmas .btn-primary').click(function() {
        var nombres = "";
        var usuarios = $('#modalFirmas #selContactos').val();
        $.each(usuarios, function(key, line) {
            nombres = nombres + $('#selContactos #opt'+line).html() + ", ";
        });
        $('#idFirmantes').val(usuarios);
        $('#inpNomFirmantes').val(nombres);
        $('#modalFirmas').modal('hide');
    });
    
    $('#modalObservadores .btn-primary').click(function() {
        var nombres = "";
        var usuarios = $('#modalObservadores #selObservadores').val();
        $.each(usuarios, function(key, line) {
            nombres = nombres + $('#selObservadores #opt'+line).html() + ", ";
        });
        $('#idObservadores').val(usuarios);
        $('#inpNomObservadores').val(nombres);
        $('#modalObservadores').modal('hide');
    });
});


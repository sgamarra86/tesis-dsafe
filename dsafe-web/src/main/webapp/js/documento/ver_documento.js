$(document).ready(function() {
    $(document).ajaxStart(function() {
        $("#loading").css('visibility', 'visible');
    }).ajaxStop(function() {
        $("#loading").css('visibility', 'hidden');
    });


    $('#btnDescargar').click(function() {
        window.location = "descargar_documento.html?idDocumento=" + $('#hidDocumento').val();
    });

    $('#btnFirmar').click(function() {
        $('#inpDocumentos').val($('.lnkFirmas').attr('id'));
        $('#mdlFirmarDocumento').modal('show');
    });

    $('#btnAceptar').click(function() {
        $('#frmFirmarDocumento').submit();
        $('#mdlFirmarDocumento').modal('hide');
        $("#loading").css('visibility', 'visible');
    });

    $('#btnReportar').click(function() {
        $('#mdlReportar').modal('show');
    });

    $('#btnHistorial').click(function() {
        var idDocumento = $('#hidDocumento').val();
        $.ajax({
            dataType: 'text',
            type: 'POST',
            url: 'obtener_notificaciones/documento.html',
            data: {
                idDocumentos: idDocumento
            },
            success: function(data) {
                var DOM = "";
                var notificaciones = JSON.parse(data);
                $.each(notificaciones, function(i, noti) {
                    var estilo = "";
                    switch (noti.tipo) {
                        case 0:
                            estilo = "info";
                            break;
                        case 1:
                            estilo = "info";
                            break;
                        case 2:
                            estilo = "error";
                            break;
                    }
                    DOM += '<tr class=' + estilo + '>';
                    DOM += '<td>' + noti.fecha + '</td>';
                    DOM += '<td>' + noti.nomUsuario + '</td>';
                    DOM += '<td>' + noti.descripcion + '</td>';
                    DOM += '</tr>';
                });

                $('#tblHistorial tbody').html(DOM);
                $('#mdlHistorial').modal('show');
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
                alert(error);
            }
        });
    });

    $('#btnReportarAceptar').click(function() {
        var descripcion = $('#mdlReportar #descripcion').val();
        if (descripcion.length == 0) {
            alert('Debe ingresar una descripción del reporte.')
        } else {
            $.ajax({
                type: "POST",
                url: "registrar_notificacion.html",
                data: $('#frmRegistrarNotificacion').serialize(),
                success: function(data) {
                    location.reload();
                },
                dataType: "text"
            });
        }
    });

    $('.lnkFirmas').click(function() {
        var documento = $(this);
        //var nombre = "Documento: " + ;
        $.ajax({
            dataType: 'text',
            type: 'POST',
            url: 'obtener_firmas.html',
            data: {
                idDocumento: documento.attr('id')
            },
            success: function(data) {
                var DOM = "";
                var firmas = JSON.parse(data);
                var realizadas = 0;
                var requeridas = firmas.length;
                $.each(firmas, function(i, firma) {
                    DOM += '<tr>';
                    DOM += '<td>' + firma.usuario + '</td>';
                    if (firma.estado == '0') {
                        DOM += '<td style="text-align: center"><span class="label label-warning">Pendiente</span></td>';
                    } else {
                        realizadas++;
                        DOM += '<td style="text-align: center"><span class="label label-info">Firmado</span></td>';
                    }
                    DOM += '</tr>';
                });

                var progreso = (100 * (realizadas / requeridas)).toString().substr(0, 5) + '%';
                $("#mdlFirmas .progress .bar").css('width', progreso);
                $("#mdlFirmas .progress .bar").css('background', '#428bca');
                $("#mdlFirmas .progress .bar").html(progreso);

                $('#tblFirmas tbody').html(DOM);
                //$('#mdlFirmas h3').html(nombre);
                $('#mdlFirmas').modal('show');
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
                alert(error);
            }
        });
    });


});
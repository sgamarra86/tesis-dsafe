$(document).ready(function() {
    $(document).ajaxStart(function() {
        $("#loading").css('visibility', 'visible');
    }).ajaxStop(function() {
        $("#loading").css('visibility', 'hidden');
    });

    $("#tblPendientes tr").tooltip({placement: 'bottom'});
    $("#tblFirmados tr").tooltip({placement: 'bottom'});
    $("#tblObservados tr").tooltip({placement: 'bottom'});

    $('#myTab a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
    });

    var tab = $('#myTab').attr('tabSeleccionado');
    if (tab == 0) {
        $('#lnkTabPendientes').trigger("click");
    }
    if (tab == 1) {
        $('#lnkTabFirmados').trigger("click");
    }
    if (tab == 2) {
        $('#lnkTabObservados').trigger("click");
    }


    $('#btnFirmar').click(function() {
        cargaDocumentosSeleccionados();
        $('#mdlFirmarDocumento').modal('show');
    });

    $('#btnAceptar').click(function() {
        $('#frmFirmarDocumento').submit();
        $('#mdlFirmarDocumento').modal('hide');
    });

    $('.lnkFirmas').click(function() {
        var documento = $(this);
        var nombre = "Documento: " + $(this).parents("tr").children("td").eq(1).html();
        $.ajax({
            dataType: 'text',
            type: 'POST',
            url: 'obtener_firmas.html',
            data: {
                idDocumento: documento.attr('id')
            },
            success: function(data) {
                var DOM = "";
                var firmas = JSON.parse(data);
                var realizadas = 0;
                var requeridas = firmas.length;
                $.each(firmas, function(i, firma) {
                    DOM += '<tr>';
                    DOM += '<td>' + firma.usuario + '</td>';
                    if (firma.estado == '0') {
                        DOM += '<td style="text-align: center"><span class="label label-warning">Pendiente</span></td>';
                    } else {
                        realizadas++;
                        DOM += '<td style="text-align: center"><span class="label label-info">Firmado</span></td>';
                    }
                    DOM += '</tr>';
                });

                var progreso = (100 * (realizadas / requeridas)).toString().substr(0, 5) + '%';
                $("#mdlFirmas .progress .bar").css('width', progreso);
                $("#mdlFirmas .progress .bar").css('background', '#428bca');
                $("#mdlFirmas .progress .bar").html(progreso);

                $('#tblFirmas tbody').html(DOM);
                $('#mdlFirmas h3').html(nombre);
                $('#mdlFirmas').modal('show');
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
                alert(error);
            }
        });
    });

    $('.lnkObservados').click(function() {
        var documento = $(this);
        var nombre = "Documento: " + $(this).parents("tr").children("td").eq(1).html();
        $.ajax({
            dataType: 'text',
            type: 'POST',
            url: 'obtener_observados.html',
            data: {
                idDocumento: documento.attr('id')
            },
            success: function(data) {
                var DOM = "";
                var observadores = JSON.parse(data);
                $.each(observadores, function(i, observador) {
                    DOM += '<tr>';
                    DOM += '<td>' + observador + '</td>';
                    DOM += '</tr>';
                });
                $('#tblObservadores tbody').html(DOM);
                $('#mdlObservadores h3').html(nombre);
                $('#mdlObservadores').modal('show');
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
                alert(error);
            }
        });
    });

    inicializarPaginador($('#tblPendientes tbody tr'), $('#paginadorPendientes'), 5);
    inicializarPaginador($('#tblFirmados tbody tr'), $('#paginadorFirmados'), 5);
    inicializarPaginador($('#tblObservados tbody tr'), $('#paginadorObservados'), 5);

});

function cargaDocumentosSeleccionados() {
    var documentos = [];
    var nombres = "";
    $('#tblPendientes :checked').each(function() {
        nombres += '<li>' + $('#tdNombre' + $(this).val()).html() + '</li>';
        documentos.push($(this).val());
    });
    $('#lstDocumentos').html(nombres);
    $('#inpDocumentos').val(documentos);
}

function inicializarPaginador(tabla, paginador, tamanio) {
    tabla.hide();
    tabla.each(function(i) {
        if (i < tamanio) {
            $(this).show();
        }
    });
    var paginas = Math.ceil(tabla.length / 5);
    var options = {
        currentPage: 1,
        totalPages: paginas,
        alignment: 'center',
        onPageClicked: function(e, originalEvent, type, page) {
            var bot = (page - 1) * tamanio;
            var top = (page * tamanio);
            tabla.each(function(i) {
                if (i >= bot && i < top) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        }
    };
    paginador.bootstrapPaginator(options);
}
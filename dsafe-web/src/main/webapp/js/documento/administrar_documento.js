$(document).ready(function() {
    $('.lnkEliminar').click(function() {
        if(confirm("¿Esta seguro que desea eliminar el documento?")){
            window.location = "eliminar_documento.html?idDocumento=" + $(this).attr('id');
        }
    });
    
    $('.lnkFirmas').click(function() {
        var documento = $(this);
        var nombre = "Documento: " + $(this).parents("tr").children("td").eq(1).html();
        $.ajax({
            dataType: 'text',
            type: 'POST',
            url: 'obtener_firmas.html',
            data: {
                idDocumento: documento.attr('id')
            },
            success: function(data) {
                var DOM = "";
                var firmas = JSON.parse(data);
                var realizadas = 0;
                var requeridas = firmas.length;
                $.each(firmas, function(i, firma) {
                    DOM += '<tr>';
                    DOM += '<td>' + firma.usuario + '</td>';
                    if (firma.estado == '0') {
                        DOM += '<td style="text-align: center"><span class="label label-warning">Pendiente</span></td>';
                    } else {
                        realizadas++;
                        DOM += '<td style="text-align: center"><span class="label label-info">Firmado</span></td>';
                    }
                    DOM += '</tr>';
                });

                var progreso = (100 * (realizadas / requeridas)).toString().substr(0, 5) + '%';
                $("#mdlFirmas .progress .bar").css('width', progreso);
                $("#mdlFirmas .progress .bar").css('background', '#428bca');
                $("#mdlFirmas .progress .bar").html(progreso);

                $('#tblFirmas tbody').html(DOM);
                $('#mdlFirmas h3').html(nombre);
                $('#mdlFirmas').modal('show');
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
                alert(error);
            }
        });
    });
    
    $('.lnkObservados').click(function() {
        var documento = $(this);
        var nombre = "Documento: " + $(this).parents("tr").children("td").eq(1).html();
        $.ajax({
            dataType: 'text',
            type: 'POST',
            url: 'obtener_observados.html',
            data: {
                idDocumento: documento.attr('id')
            },
            success: function(data) {
                var DOM = "";
                var observadores = JSON.parse(data);
                $.each(observadores, function(i, observador) {
                    DOM += '<tr>';
                    DOM += '<td>' + observador + '</td>';
                    DOM += '</tr>';
                });
                $('#tblObservadores tbody').html(DOM);
                $('#mdlObservadores h3').html(nombre);
                $('#mdlObservadores').modal('show');
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
                alert(error);
            }
        });
    });
    
    inicializarPaginador($('#tblDocumentos tbody tr'),$('#paginador'),5);
});

function inicializarPaginador(tabla, paginador, tamanio){
    tabla.hide();
    tabla.each(function(i) {
        if (i < tamanio) {
            $(this).show();
        }
    });
    var paginas = Math.ceil(tabla.length / 5);
    var options = {
        currentPage: 1,
        totalPages: paginas,
        alignment: 'center',
        onPageClicked: function(e, originalEvent, type, page) {
            var bot = (page - 1) * tamanio;
            var top = (page * tamanio);
            tabla.each(function(i) {
                if (i >= bot && i < top) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        }
    };
    paginador.bootstrapPaginator(options);
}
function valSoloDigitos(e) {
    tecla = e.which || e.keyCode;
    patron = /\d/; // Solo acepta números
    te = String.fromCharCode(tecla);
    return !bloquearOtros(te) && ((patron.test(te) || tecla == 9 || tecla == 8 || tecla == 0 || tecla == 37 || tecla == 39 || tecla == 46 || tecla == 35 || tecla == 36));
}

function valSoloLetras(e) {
    tecla = e.which || e.keyCode;
    te = String.fromCharCode(tecla);
    return !bloquearOtros(te) && ((tecla >= 60 && tecla <= 90) || (tecla >= 97 && tecla <= 122) || tecla == 9 || tecla == 8 || tecla == 0 || tecla == 32 || tecla == 37 || tecla == 39 || tecla == 46 || tecla == 35 || tecla == 36);
}

function valSoloAlfanumerico(e) {
    tecla = e.which || e.keyCode;
    patron = /^[0-9a-zA-Z]*$/;
    te = String.fromCharCode(tecla);
    return !bloquearOtros(te) && ((patron.test(te) || tecla == 9 || tecla == 8 || tecla == 0));
}

function valEmail(e) {
    tecla = e.which || e.keyCode;
    patron = /^[0-9a-zA-Z@.]*$/;
    te = String.fromCharCode(tecla);
    return !bloquearOtrosEmail(te) && ((patron.test(te) || tecla == 9 || tecla == 8 || tecla == 0 ));
}

function valTelMovil(e) {
    tecla = e.which || e.keyCode;
    patron = /\d/; // Solo acepta números
    te = String.fromCharCode(tecla);
    return !bloquearOtrosTelMovil(te) && ((patron.test(te) || tecla == 9 || tecla == 8 || tecla == 0 || tecla == 37 || tecla == 39 || tecla == 46 || tecla == 35 || tecla == 36));
}

function bloquearOtros(valor) {
    var bloquea = false;
    if(valor.toString() == '$')
        bloquea = true;
    else if(valor.toString() == '%')
        bloquea = true;
    else if(valor.toString() == '.')
        bloquea = true;
    else if(valor.toString() == '\'')
        bloquea = true;
    else if(valor.toString() == '#')
        bloquea = true;

    return bloquea;
}

function bloquearOtrosEmail(valor) {
    var bloquea = false;
    if(valor.toString() == '$')
        bloquea = true;
    else if(valor.toString() == '%')
        bloquea = true;
    else if(valor.toString() == '\'')
        bloquea = true;
    else if(valor.toString() == '#')
        bloquea = true;

    return bloquea;
}

function bloquearOtrosTelMovil(valor) {
    var bloquea = false;
    if(valor.toString() == '$')
        bloquea = true;
    else if(valor.toString() == '%')
        bloquea = true;
    else if(valor.toString() == '\'')
        bloquea = true;

    return bloquea;
}


function esPorcentaje() {
    range = /^\d{1,2}(\.\d{1,1})?$/;
    hundred = /^100$/;
    if (!(range.test(campo) || hundred.test(campo))) {
         return false;
    } else {
        return true;
    }
}
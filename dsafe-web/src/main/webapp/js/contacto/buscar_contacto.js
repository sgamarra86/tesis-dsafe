$(document).ready(function() {
    $(document).ajaxStart(function () {
        $("#loading").css('visibility', 'visible');
    }).ajaxStop(function () {
        $("#loading").css('visibility', 'hidden');
    });
    
    $('#btnBuscar').click(function() {
        window.location = "buscar_contacto.html?parametro=" + $('#parametro').val();
    });

    $('.a_contacto').click(function() {
        var contacto = $(this);
        var nombres = contacto.parents("tr").children("td").eq(1).html();
        var apellidos = contacto.parents("tr").children("td").eq(2).html();
        if (!confirm('¿Desea enviar una solicitud de contacto a ' + nombres + '?'))
            return false;

        $.ajax({
            dataType: 'text',
            type: 'POST',
            url: 'registrar_contacto.html',
            data: {
                idContacto: contacto.attr('id')
            },
            success: function(data) {
                window.location = "buscar_contacto.html";
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
                alert(error);
            }
        });
    });

    inicializarPaginador($('#tblContactos tbody tr'), $('#paginador'), 5);
});

function inicializarPaginador(tabla, paginador, tamanio) {
    tabla.hide();
    tabla.each(function(i) {
        if (i < tamanio) {
            $(this).show();
        }
    });
    var paginas = Math.ceil(tabla.length / 5);
    var options = {
        currentPage: 1,
        totalPages: paginas,
        alignment: 'center',
        onPageClicked: function(e, originalEvent, type, page) {
            var bot = (page - 1) * tamanio;
            var top = (page * tamanio);
            tabla.each(function(i) {
                if (i >= bot && i < top) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        }
    };
    paginador.bootstrapPaginator(options);
}
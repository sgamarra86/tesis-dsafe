$(document).ready(function() {
    $('.a_contacto_aceptar').click(function() {
        var contacto = $(this);
        var nombres = contacto.parents("tr").children("td").eq(1).html();
        var apellidos = contacto.parents("tr").children("td").eq(2).html();
        if (!confirm('¿Desea aceptar la solicitud de ' + nombres + '?'))
            return false;

        $.ajax({
            dataType: 'text',
            type: 'POST',
            url: 'aceptar_solicitud_recibida.html',
            data: {
                idContacto: contacto.attr('id')
            },
            success: function(data) {
                //contacto.parents("tr").remove();
                window.location = "listar_solicitud_recibida.html";
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
                alert(error);
            }
        });
    });

    $('.a_contacto_rechazar').click(function() {
        var contacto = $(this);
        var nombres = contacto.parents("tr").children("td").eq(1).html();
        var apellidos = contacto.parents("tr").children("td").eq(2).html();
        if (!confirm('¿Desea rechazar la solicitud de ' + nombres + '?'))
            return false;
        
        $.ajax({
            dataType: 'text',
            type: 'POST',
            url: 'rechazar_solicitud_recibida.html',
            data: {
                idContacto: contacto.attr('id')
            },
            success: function(data) {
                //contacto.parents("tr").remove();
                window.location = "listar_solicitud_recibida.html";
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
                alert(error);
            }
        });
    });

    inicializarPaginador($('#tblSolicitudes tbody tr'), $('#paginador'), 5);
});

function inicializarPaginador(tabla, paginador, tamanio) {
    tabla.hide();
    tabla.each(function(i) {
        if (i < tamanio) {
            $(this).show();
        }
    });
    var paginas = Math.ceil(tabla.length / 5);
    var options = {
        currentPage: 1,
        totalPages: paginas,
        alignment: 'center',
        onPageClicked: function(e, originalEvent, type, page) {
            var bot = (page - 1) * tamanio;
            var top = (page * tamanio);
            tabla.each(function(i) {
                if (i >= bot && i < top) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        }
    };
    paginador.bootstrapPaginator(options);
}
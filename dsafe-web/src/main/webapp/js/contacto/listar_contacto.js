$(document).ready(function() {
    $('.a_contacto_eliminar').click(function() {
        var contacto = $(this);
        var nombres = contacto.parents("tr").children("td").eq(2).html();
        var apellidos = contacto.parents("tr").children("td").eq(3).html();
        if (!confirm('¿Desea eliminar al contacto ' + nombres + '?'))
            return false;
        
        $.ajax({
            dataType: 'text',
            type: 'POST',
            url: 'eliminar_contacto.html',
            data: {
                idContacto: contacto.attr('id')
            },
            success: function(data) {
                contacto.parents("tr").remove();
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
                alert(error);
            }
        });
    });
    
    inicializarPaginador($('#tblContactos tbody tr'),$('#paginador'),5);
});

function inicializarPaginador(tabla, paginador, tamanio){
    tabla.hide();
    tabla.each(function(i) {
        if (i < tamanio) {
            $(this).show();
        }
    });
    var paginas = Math.ceil(tabla.length / 5);
    var options = {
        currentPage: 1,
        totalPages: paginas,
        alignment: 'center',
        onPageClicked: function(e, originalEvent, type, page) {
            var bot = (page - 1) * tamanio;
            var top = (page * tamanio);
            tabla.each(function(i) {
                if (i >= bot && i < top) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        }
    };
    paginador.bootstrapPaginator(options);
}
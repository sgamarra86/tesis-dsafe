$(document).ready(function() {
    $("#loading").css('visibility', 'hidden');    
    $('#frmRegistrarUsuario').submit(function(){
        $("#loading").css('visibility', 'visible');
       return true; 
    });
    
    $('span[id*="errors"]').parents('div[class*="control-group"]').addClass('error');
    
    $('#btnCancelar').click(function() {
        window.location = "index.html";
    });
});


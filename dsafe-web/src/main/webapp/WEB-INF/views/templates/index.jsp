<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
    <head>
        <title>DSafe - Seguridad a tu alcance</title>
        <link rel="shortcut icon" type="image/png" href="img/d-icon.png" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/smoothness/jquery-ui-1.8.24.custom.css"/>" />
        <link rel="stylesheet" type="text/css"  href="<c:url value="/css/bootstrap.css"/>" media="screen" >
        <script src="<c:url value='/js/jquery.js'/>" type="text/javascript"></script>
        <script src="<c:url value='/js/jquery-ui.js'/>" type="text/javascript"></script>
        <script src="<c:url value='/js/bootstrap.js'/>" type="text/javascript"></script>
        <style type="text/css">
            body {
                padding-top: 20px;
                padding-bottom: 40px;
            }
            
            #loading {
                position: fixed;
                opacity: 0.5;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url("../dsafe-web/img/loading.gif") no-repeat scroll 50% 50% / 50px 50px rgb(0, 0, 0);
                visibility: hidden;
            }
        </style>
    </head>
    <body>
        <div><tiles:insertAttribute name="indexHeader" /></div>
        <div class="container">
            <div><tiles:insertAttribute name="indexContent" /></div>
            <div><tiles:insertAttribute name="indexFooter" /></div>
        </div>
        <div id="loading"></div>

    </body>
</html>
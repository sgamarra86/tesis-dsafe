<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
    <head>
        <title>DSafe - Seguridad a tu alcance</title>
        <link rel="shortcut icon" type="image/png" href="img/d-icon.png" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link  rel="stylesheet" type="text/css"  href="<c:url value="/css/bootstrap.css"/>" media="screen" >
        <link  rel="stylesheet" type="text/css"  href="<c:url value="/css/global.css"/>" media="screen" >
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/smoothness/jquery-ui-1.8.24.custom.css"/>" />
        <script src="<c:url value='/js/jquery.js'/>" type="text/javascript"></script>
        <script src="<c:url value='/js/jquery-ui.js'/>" type="text/javascript"></script>
        <script src="<c:url value='/js/bootstrap.js'/>" type="text/javascript"></script>
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }

            @media (max-width: 980px) {
                /* Enable use of floated navbar text */
                .navbar-text.pull-right {
                    float: none;
                    padding-left: 5px;
                    padding-right: 5px;
                }
            }
        </style>
        <style type="text/css">
            body {
                background-color: #0B496E;

            }
            .form-background {
                border: 1px solid #E5E5E5;
            }
            .footer-container {
                color: #999999;
                font-size: small;
                margin-bottom: 0px;
                padding-top: 5px;
                padding-bottom: 5px;
                padding-left: 15px;
                padding-right: 15px;
            }
            #loading {
                position: fixed;
                opacity: 0.5;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url("../dsafe-web/img/loading.gif") no-repeat scroll 50% 50% / 50px 50px rgb(0, 0, 0);
                visibility: hidden;
            }

        </style>
    </head>
    <body id="body" >
        <div class="container">
            <div><tiles:insertAttribute name="mainHeader" /></div> 
            <div><tiles:insertAttribute name="mainMenu" /></div>
            <div><tiles:insertAttribute name="mainMensaje" /></div>
            <div class="row-fluid" style="margin-top: -6px;">
                <div class="span3 well sidebar-nav form-background" style="height: 550px">
                    <tiles:insertAttribute name="mainLeft" />
                </div> 
                <div class="span9 well form-background" style="height: 550px; font-size: 12px;">
                    <tiles:insertAttribute name="mainCenter" />
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12 well sidebar-nav form-background footer-container"style="margin-top: -5px;">
                    <tiles:insertAttribute name="mainFooter" />
                </div>
            </div>
        </div>
        <div id="loading"></div>
    </body>

</html>

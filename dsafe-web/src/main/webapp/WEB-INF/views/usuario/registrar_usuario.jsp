<%-- 
    Document   : center
    Created on : 01/10/2013, 10:35:13 PM
    Author     : Sergio Gamarra
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script src="<c:url value='/js/usuario/registrar_usuario.js'/>" type="text/javascript"></script> 

<style type="text/css">
    body {
        background-color: #0B496E;
    }

    .controls .help-block{
        color: red;
        margin-bottom: 10px;
    }
    
    .form-center .form-signin-heading, .form-center .checkbox {
        margin-bottom: 10px;
    }
    .form-center input[type="text"], .form-center input[type="password"] {
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
    }
    .form-center input[type="file"] {
        margin-bottom: 15px;
    }
    .form-center {
        background-color: #E5E5E5;
        border: 1px solid #E5E5E5;
        border-radius: 5px 5px 5px 5px;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
        max-width: 500px;
        margin: 0 auto 20px;
        padding: 19px 29px 29px;
    }
    

</style>

<h1 class="text-center">DSafe <small>Seguridad a tu alcance</small></h1>
<hr/>
<div style="width: 560px; margin: 0 auto 20px"><jsp:include page="../includes/mensaje.jsp"/></div>

<c:url var="url" value="/registrar_usuario.html" />
<form:form id="frmRegistrarUsuario" modelAttribute="registrarUsuario" method="post" action="${url}" cssClass="form-center form-horizontal" enctype="multipart/form-data">
    <h2 class="form-signin-heading">Nuevo usuario</h2>
    <div class="form-group" >
        <label  class="control-label" for="nombres"><spring:message code="lbl.nombres"/>*</label>
        <div class="controls">
            <form:input id="nombres" path="nombres" cssClass="form-control  span4" placeholder="Nombres"/>
            <form:errors cssClass="help-block" path="nombres" cssStyle="margin-top:-10px;" />
        </div>
    </div>

    <div class="form-group" >
        <label  class="control-label" for="apellidos"><spring:message code="lbl.apellidos"/>*</label>
        <div class="controls">
            <form:input id="apellidos" path="apellidos" cssClass="form-control span4" placeholder="Apellidos"/>
            <form:errors cssClass="help-block" path="apellidos" cssStyle="margin-top:-10px;" />
        </div>
    </div>

    <div class="form-group" >
        <label  class="control-label" for="email"><spring:message code="lbl.email"/>*</label>
        <div class="controls">
            <form:input id="email" path="email" cssClass="form-control span4" placeholder="Correo electr�nico"/>
            <form:errors cssClass="help-block" path="email" cssStyle="margin-top:-10px;" />
        </div>
    </div>

    <div class="form-group" >
        <label  class="control-label" for="password"><spring:message code="lbl.password"/>*</label>
        <div class="controls">
            <form:password id="password" path="password" cssClass="form-control span4" placeholder="Contrase�a"/>
            <form:errors cssClass="help-block" path="password" cssStyle="margin-top:-10px;" />
        </div>
    </div>

    <div class="form-group" >
        <label  class="control-label" for="dni"><spring:message code="lbl.dni"/></label>
        <div class="controls">
            <form:input id="dni" path="dni" cssClass="form-control span4" placeholder="N�m. de documento"/>
            <form:errors cssClass="help-block" path="dni" cssStyle="margin-top:-10px;" />
        </div>
    </div>

    <div class="form-group" >
        <label  class="control-label" for="fono"><spring:message code="lbl.fono"/></label>
        <div class="controls">
            <form:input id="fono" path="fono" cssClass="form-control span4" placeholder="N�m. de telef�no"/>
            <form:errors cssClass="help-block" path="fono" cssStyle="margin-top:-10px;" />
        </div>
    </div>

    <div class="form-group">
        <label for="avatar" class="col-sm-2 control-label">Foto</label>
        <div class="controls">
            <form:input id="avatar" path="avatar" type="file"  cssClass="form-control  span9" placeholder="Seleccione un archivo"/>
            <form:errors cssClass="help-block" path="avatar" cssStyle="margin-top:-10px;" />
        </div>
    </div>

    <div class="form-group">
        <label for="firma" class="col-sm-2 control-label">Firma</label>
        <div class="controls">
            <form:input id="firma" path="firma" type="file"  cssClass="form-control  span9" placeholder="Seleccione un archivo"/>
            <form:errors cssClass="help-block" path="firma" cssStyle="margin-top:-10px;" />
        </div>
    </div>


    <div class="text-center">
        <button type="submit" class="btn btn-large btn-primary"><spring:message code="btn.guardar"/></button>
        <button id="btnCancelar" type="button" class="btn btn-large btn-info"><spring:message code="btn.cancelar"/></button>
    </div>
</form:form>


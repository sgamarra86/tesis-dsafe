<%-- 
    Document   : perfil_usuario
    Created on : 15/04/2014, 09:54:58 AM
    Author     : Sergio Gamarra
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script src="<c:url value='/js/bootstrap-paginator.js'/>" type="text/javascript"></script> 
<script src="<c:url value='/js/contacto/listar_contacto.js'/>" type="text/javascript"></script> 

<style type="text/css">
    .usuario dt{
        width : 80px;
        margin-bottom: 10px;
    }
    .usuario dd{
        margin-left : 100px;
        margin-bottom: 10px;
    }
</style>


<h3 class="titulo">Mi Perfil</h3>
<div class="well-small well alert-info">
    En esta secci�n puede revisar sus datos personales.
</div>
<div class="well-small well main_sub_contenedor_largo clearfix" style="padding: 10px; font-size: 14px;">
    <div class="span3" style="margin-left: 50px;">
        <div>
            <h5>Avatar:</h5>
            <img src="img/<c:url value="${userDetails.email}"/>_avatar.jpg" style="margin-left: 6px; width: available">
        </div>
    </div>
    <div class="span6" style="margin-left: 50px;">
        <h5>Datos de usuario:</h5>
        <dl class="dl-horizontal usuario">
            <dt>Usuario</dt>
            <dd>${userDetails.nombres} ${userDetails.apellidos}</dd>
            <dt>Email</dt>
            <dd>${userDetails.email}</dd>
            <dt>DNI</dt>
            <dd>${userDetails.dni}</dd>
            <dt>Fono</dt>
            <dd>${userDetails.fono}</dd>
            <dt>Firma</dt>
            <dd><img src="img/<c:url value="${userDetails.email}"/>_firma.jpg" style="width: 150px;"></dd>
            
        </dl>
    </div>

</div>

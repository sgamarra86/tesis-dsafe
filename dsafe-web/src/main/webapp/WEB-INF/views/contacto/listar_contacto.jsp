<%-- 
    Document   : center
    Created on : 01/10/2013, 10:35:13 PM
    Author     : Sergio Gamarra
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script src="<c:url value='/js/bootstrap-paginator.js'/>" type="text/javascript"></script> 
<script src="<c:url value='/js/contacto/listar_contacto.js'/>" type="text/javascript"></script> 


<h3 class="titulo">Mis contactos</h3>
<div class="well-small well alert-info">
    En esta secci�n puede revisar los contactos agregados a su cuenta adem�s de poder eliminarlos.
</div>
<div class="well-small well main_sub_contenedor_largo">
    <c:if test="${empty contactos }">
        <p>No se cuenta con contactos.</p>
    </c:if>    
    <c:if test="${not empty contactos }">
        <table id="tblContactos" class="table table-hover main_table">
            <thead>
                <tr>
                    <th>Email</th>
                    <th>DNI</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
<!--                    <th style="text-align: center">Estado</th>-->
                    <th style="text-align: center">Eliminar</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="contacto" items="${contactos}"  varStatus="status">
                    <tr>
                        <td>${contacto.email}</td>
                        <td>${contacto.dni}</td>
                        <td>${contacto.nombres}</td>
                        <td>${contacto.apellidos}</td>
                        <%--
                        <c:if test="${contacto.estado eq 0 }">
                            <td style="text-align: center"><img src="<c:url value='/img/status/offline.gif'/>" title="Desconectado"></td>
                            </c:if>
                            <c:if test="${contacto.estado eq 1 }">
                            <td style="text-align: center"><img src="<c:url value='/img/status/online.gif'/>" title="Conectado"></td>
                            </c:if>
                            <c:if test="${contacto.estado eq 2 }">
                            <td style="text-align: center"><img src="<c:url value='/img/status/bussy.gif'/>" title="Ocupado"></td>
                            </c:if>
                            <c:if test="${contacto.estado eq 3 }">
                            <td style="text-align: center"><img src="<c:url value='/img/status/away.gif'/>" title="Ausente"></td>
                            </c:if>
                        --%>
                        <td style="text-align: center">
                            <a id="${contacto.id}" href='#' class="a_contacto_eliminar">
                                <img src="<c:url value="/img/icon-trash.png"/>">
                            </a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

        <div id="paginador"></div>
    </c:if>
</div>

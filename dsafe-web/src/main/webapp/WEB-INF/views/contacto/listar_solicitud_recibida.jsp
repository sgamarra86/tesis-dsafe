<%-- 
    Document   : center
    Created on : 01/10/2013, 10:35:13 PM
    Author     : Sergio Gamarra
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script src="<c:url value='/js/bootstrap-paginator.js'/>" type="text/javascript"></script> 
<script src="<c:url value='/js/contacto/listar_solicitud_recibida.js'/>" type="text/javascript"></script> 

<h3 class="titulo">Solicitudes de contacto</h3>

<div class="well-small well alert-info">
    En esta secci�n puede aceptar o rechazar las solicitudes de amistad recibidas.
</div>
<div class="well-small well main_sub_contenedor_largo">
    <c:if test="${empty contactos }">
        No se han recibido solicitudes de contacto.
    </c:if>    
    <c:if test="${not empty contactos }">
        <table id="tblSolicitudes" class="table table-hover main_table">
            <thead>
                <tr>
                    <th>Email</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th style="text-align: center">Agregar</th>
                    <th style="text-align: center">Rechazar</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="contacto" items="${contactos}"  varStatus="status">
                    <tr>
                        <td>${contacto.email}</td>
                        <td>${contacto.nombres}</td>
                        <td>${contacto.apellidos}</td>
                        <td style="text-align: center">
                            <a id="${contacto.id}" href='#' class="a_contacto_aceptar">
                                <img src="<c:url value="/img/icon-plus.png"/>">
                            </a>
                        </td>
                        <td style="text-align: center">
                            <a id="${contacto.id}" href='#' class="a_contacto_rechazar">
                                <img src="<c:url value="/img/icon-cross.png"/>">
                            </a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

        <div id="paginador"></div>    
    </c:if>
</div>
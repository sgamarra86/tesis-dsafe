<%-- 
    Document   : center
    Created on : 01/10/2013, 10:35:13 PM
    Author     : Sergio Gamarra
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script src="<c:url value='/js/bootstrap-paginator.js'/>" type="text/javascript"></script> 
<script src="<c:url value='/js/contacto/buscar_contacto.js'/>" type="text/javascript"></script> 

<h3 class="titulo">A�adir contactos</h3>
<div class="well-small well alert-info">
    Ingrese el nombre, apellido o correo del contacto para realizar la b�squeda.
</div>

<div class="input-append" style="width: 600px; margin-bottom: 20px;">
    <input class="span6" id="parametro" type="text">
    <button id="btnBuscar" class="btn" type="button">Buscar</button>
</div>
<c:if test="${not empty contactos }">
    <div class="well-small well main_sub_contenedor_largo">
        <table id="tblContactos" class="table table-hover main_table">
            <thead>
                <tr>
                    <th>Email</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th style="text-align: center">Agregar</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="contacto" items="${contactos}"  varStatus="status">
                    <tr>
                        <td>${contacto.email}</td>
                        <td>${contacto.nombres}</td>
                        <td>${contacto.apellidos}</td>
                        <td style="text-align: center">
                            <a id="${contacto.id}" href='#' class="a_contacto">
                                <img src="<c:url value="/img/icon-plus.png"/>" style="height: 16px;">
                            </a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

        <div id="paginador"></div>
    </div>
</c:if>
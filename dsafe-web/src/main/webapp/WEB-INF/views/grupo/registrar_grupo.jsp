<%-- 
    Document   : center
    Created on : 01/10/2013, 10:35:13 PM
    Author     : Sergio Gamarra
--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<style type="text/css">
    body {
        background-color: #F5F5F5;

    }
    .form-signin {
        background-color: #FFFFFF;
        border: 1px solid #E5E5E5;
        border-radius: 5px 5px 5px 5px;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
        margin: 0 auto 20px;
        max-width: 300px;
        padding: 19px 29px 29px;
    }
    .form-signin .form-signin-heading, .form-signin .checkbox {
        margin-bottom: 10px;
    }
    .form-signin input[type="text"], .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
    }
    .form-center {
        background-color: #FFFFFF;
        border: 1px solid #E5E5E5;
        border-radius: 5px 5px 5px 5px;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
        margin: 0 auto 20px;
        padding: 19px 29px 29px;
    }

</style>
<div class="span9">
    <div class="form-center">
        <h2 class="form-signin-heading">Nuevo grupo</h2>
        <form class="form-horizontal">
            <div class="control-group">
                <label class="control-label" for="inputNombre">Nombre</label>
                <div class="controls">
                    <input type="text" id="inputNombre" placeholder="Nombre">
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn btn-primary">Aceptar</button>
                    <button type="button" class="btn">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</div>

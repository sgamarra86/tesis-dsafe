<%-- 
    Document   : center
    Created on : 01/10/2013, 10:35:13 PM
    Author     : Sergio Gamarra
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<style type="text/css">
    body {
        background-color: #F5F5F5;

    }
    .form-signin {
        background-color: #FFFFFF;
        border: 1px solid #E5E5E5;
        border-radius: 5px 5px 5px 5px;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
        margin: 0 auto 20px;
        max-width: 300px;
        padding: 19px 29px 29px;
    }
    .form-signin .form-signin-heading, .form-signin .checkbox {
        margin-bottom: 10px;
    }
    .form-signin input[type="text"], .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
    }
    .form-center {
        background-color: #FFFFFF;
        border: 1px solid #E5E5E5;
        border-radius: 5px 5px 5px 5px;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
        margin: 0 auto 20px;
        padding: 19px 29px 29px;
    }

</style>
<div class="span9">
    <div class="form-center">
        <h2 class="form-signin-heading">Grupos</h2>
        <c:url var="url" value="/registrar_grupo.html" />
        <form:form id="frmRegistrarGrupo" modelAttribute="registrarGrupo" method="post" action="${url}" class="form-horizontal">
            <div class="input-append">
                <!--            TOFIX: Problema con el span de la barra de busqueda, no carga el span8-->
                <form:input id="nombre" path="nombre" placeholder="Nombre de Grupo"/>
                <form:errors cssClass="help-block" path="nombre" />
                <button class="btn" type="submit">Agregar</button>
            </div>
        </form:form>

        <c:if test="${empty grupos }">
            <p>No se cuenta con grupos a�n...</p>
        </c:if>
        <c:if test="${not empty grupos }">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th style="text-align: center">Contactos</th>
                        <th style="text-align: center">Ver</th>
                        <th style="text-align: center">Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="grupo" items="${grupos}"  varStatus="status">
                        <tr>
                            <td>${status.index + 1}</td>
                            <td>${grupo.nombre}</td>
                            <td style="text-align: center">${grupo.cantidad}</td>
                            <td style="text-align: center">
                                <a href='ver_grupo.html?idGrupo=${grupo.id}' title="Ver"><i class="icon-eye-open"></i></a>
                            </td>
                            <td style="text-align: center">
                                <a href='eliminar_grupo.html?idGrupo=${grupo.id}' title="Eliminar" onclick="if (!confirm('Esta acci�n eliminar� el Grupo. �Desea continuar?'))
                                            return false;"><i class="icon-trash"></i></a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <div class="pagination">
                <ul>
                    <li><a href="#">Prev</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">Next</a></li>
                </ul>
            </div>
        </c:if>
    </div>
</div>

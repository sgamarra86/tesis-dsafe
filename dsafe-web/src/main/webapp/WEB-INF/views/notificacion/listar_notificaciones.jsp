<%-- 
    Document   : listar_notificaciones
    Created on : 10/04/2014, 09:21:27 AM
    Author     : Sergio Gamarra
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script src="<c:url value='/js/bootstrap-paginator.js'/>" type="text/javascript"></script> 
<script src="<c:url value='/js/notificacion/listar_notificacion.js'/>" type="text/javascript"></script> 


<h3 class="titulo">Notificaciones</h2>

<div class="well-small well alert-info">
    En esta secci�n puede revisar las notificaciones generadas.
</div>
<div class="well well-small main_sub_contenedor_largo">
    <c:if test="${empty notificaciones }">
        <p>No se cuenta con notificiones.</p>
    </c:if>
    <c:if test="${not empty notificaciones }">
        <div style ="height:300px; overflow-x:hidden; overflow-y:auto;">
            <table id="tblNotificaciones" class="table table-hover table-condensed" style="font-size: 12px;">
                <thead>
                    <tr>
                        <th class="span2">Fecha</th>
                        <th class="span2">Documento</th>
                        <th class="span2">Usuario</th>
                        <th>Evento</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="notificacion" items="${notificaciones}"  varStatus="status">
                        <c:if test="${notificacion.tipo eq 0}">
                            <c:set var="tipo" value="info"/>
                        </c:if>
                        <c:if test="${notificacion.tipo eq 1}">
                            <c:set var="tipo" value="info"/>
                        </c:if>
                        <c:if test="${notificacion.tipo eq 2}">
                            <c:set var="tipo" value="error"/>
                        </c:if>
                        <tr class="${tipo}">
                            <td >${notificacion.fecha}</td>
                            <td >
                                <a id="ver" href="ver_documento.html?idDocumento=${notificacion.idDocumento}">
                                    ${notificacion.nomDocumento}
                                </a>
                            </td>
                            <td >${notificacion.nomUsuario}</td>
                            <td >${notificacion.descripcion}</td>

                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <div id="paginador"></div>
    </c:if>
</div>
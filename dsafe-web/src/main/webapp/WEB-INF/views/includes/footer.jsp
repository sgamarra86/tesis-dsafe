<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div style="float: left">
    <span><b>DSafe</b>, Seguridad a tu alcance</span>
</div>
<div style="float: right">
    <img src="<c:url value="/img/icon/java-icon.png"/>">
    <img src="<c:url value="/img/icon/netbeans-icon.png"/>">
    <img src="<c:url value="/img/icon/pgsql-icon.png"/>">
    <img src="<c:url value="/img/icon/tomcat-icon.png"/>">
    <img src="<c:url value="/img/icon/spring-icon.png"/>">
    <img src="<c:url value="/img/icon/hibernate-icon.png"/>">
    <img src="<c:url value="/img/icon/bitbucket-icon.png"/>">
    <img src="<c:url value="/img/icon/trello-icon.png"/>">
</div>

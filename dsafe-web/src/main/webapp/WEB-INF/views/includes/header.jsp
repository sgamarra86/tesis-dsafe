<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<sec:authorize access="isAuthenticated()">
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="<c:url value="main.html"/>" class="brand">DSafe</a>
                <div class="nav-collapse collapse">
                    <ul class="nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contactos <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<c:url value="listar_contacto.html"/>">Mis contactos</a></li>
                                <li><a href="<c:url value="listar_solicitud_recibida.html"/>">Mis solicitudes</a></li>
                                <li><a href="<c:url value="buscar_contacto.html"/>">Añadir contactos</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Documentos <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<c:url value='administrar_documento.html'/>">Mis documentos</a></li>
                                <li><a href="<c:url value='registrar_documento.html'/>">Añadir documento</a></li>
                            </ul>
                        </li>
                        <a href="<c:url value="/registrar_documento.html"/>" class="btn btn-success"><i class="icon-plus-sign"></i> Añadir Documento</a>
                    </ul>
                    
                    <ul class="nav pull-right">
                        <li><img src="<c:url value="/img/${userDetails.email}_avatar.jpg"/>" style="height: 30px; padding: 5px; border-radius: 10px;"></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><c:out value="${userDetails.nombres}"/> <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<c:url value='perfil_usuario.html'/>">Mi perfil</a></li>
                                <li class="divider"></li>
                                <li><a href="<c:url value='/logout'/>">Cerrar sesión </a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</sec:authorize>
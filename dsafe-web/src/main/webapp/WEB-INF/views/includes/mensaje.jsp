<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : mensaje
    Created on : 09/10/2013
    Author     : Sergio Gamarra
--%>
<c:if test="${ not empty mensaje}">
    <c:if test="${mensaje.tipo eq 'SUCCESS'}">
        <c:set var="msgStyle" value="alert alert-success"/>
    </c:if>
    <c:if test="${mensaje.tipo eq 'WARNING'}">
        <c:set var="msgStyle" value="alert alert"/>
    </c:if>
    <c:if test="${mensaje.tipo eq 'ERROR'}">
        <c:set var="msgStyle" value="alert alert-error"/>
    </c:if>
    <div class="${msgStyle}">
        <c:if test="${not empty  mensaje.mensajes}">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </c:if> 
        <c:forEach items="${mensaje.mensajes}" var="message">
            <strong>  <c:out value="${message}"/></strong>
        </c:forEach>
    </div>
</c:if>
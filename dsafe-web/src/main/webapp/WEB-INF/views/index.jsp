<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<script src="<c:url value='/js/usuario/login_usuario.js'/>" type="text/javascript"></script> 

<style type="text/css">
    body {
        background-color: #0B496E;

    }
    .form-signin {
        background-color: #E5E5E5;
        border: 1px solid #E5E5E5;
        border-radius: 5px 5px 5px 5px;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
        margin: 0 auto 20px;
        max-width: 300px;
        padding: 19px 29px 29px;
    }
    .form-signin .form-signin-heading, .form-signin .checkbox {
        margin-bottom: 10px;
    }
    .form-signin input[type="text"], .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
    }
</style>

<h1 class="text-center">DSafe <small>Seguridad a tu alcance</small></h1>
<hr/>
<div style="width: 360px; margin: 0 auto 20px"><jsp:include page="../views/includes/mensaje.jsp"/></div>

<form class="form-signin" action="<c:url value='/j_spring_security_check'/>" method="post">
    <h2 class="form-signin-heading">Login</h2>
    <input id="j_username" name="j_username" type="text" placeholder="Usuario" class="input-block-level">
    <input id="j_password" name="j_password" type="password" placeholder="Contraseņa" class="input-block-level">
    <div class="text-center">
        
    <button type="submit" class="btn btn-large btn-primary">Ingresar</button>
    <button id="btnRegistrar" type="button" class="btn btn-large btn-info">Registrar</button>
    </div>
</form>
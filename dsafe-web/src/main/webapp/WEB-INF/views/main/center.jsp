<%-- 
    Document   : center
    Created on : 01/10/2013, 10:35:13 PM
    Author     : Sergio Gamarra
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script src="<c:url value='/js/main/center.js'/>" type="text/javascript"></script> 

<style type="text/css">
    .no-left-margin{
        margin-left: 0px;
    }

    .notificacion{
        font-size: 12px;
        line-height: 12px;
    }

    .letra{
        font-size: 12px;
    }
</style>

<h3 class="titulo">DSafe <small>Seguridad a tu alcance</small></h3>
<div id="divNotificaciones" class="well well-small span12 main_sub_contenedor_largo" style="margin-left: 0px; margin-bottom: 10px; height: 258px;">
    <h5 class="main_subtitulo">Notificaciones</h5>
    <c:if test="${empty notificaciones }">
        <p><i>Sin nuevas notificaciones.</i></p>
    </c:if>
    <c:if test="${not empty notificaciones }">
        <div style ="height:190px; overflow-x:hidden; overflow-y:auto; margin-bottom: 5px;">
            <table id="tblNotificaciones" class="table table-hover table-condensed main_table">
                <thead>
                    <tr>
                        <th class="span2">Fecha</th>
                        <th class="span2">Documento</th>
                        <th class="span2">Usuario</th>
                        <th>Evento</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="notificacion" items="${notificaciones}"  varStatus="status">
                        <c:if test="${notificacion.tipo eq 0}">
                            <c:set var="tipo" value="info"/>
                        </c:if>
                        <c:if test="${notificacion.tipo eq 1}">
                            <c:set var="tipo" value="info"/>
                        </c:if>
                        <c:if test="${notificacion.tipo eq 2}">
                            <c:set var="tipo" value="error"/>
                        </c:if>
                        <tr class="${tipo}">
                            <td >${notificacion.fecha}</td>
                            <td >
                                <a id="ver" href="ver_documento.html?idDocumento=${notificacion.idDocumento}">
                                    ${notificacion.nomDocumento}
                                </a>
                            </td>
                            <td >${notificacion.nomUsuario}</td>
                            <td >${notificacion.descripcion}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <!--        <div class="pull-right letra"><a id="lnkHistorial" href="#">Ver m�s...</a></div>-->
        <div class="pull-right letra"><a href="<c:url value="listar_notificaciones.html"/>">Ver m�s...</a></div>
    </c:if>
</div>

<%--
<div id="divContactos" class="well well-small span12" style="margin-left: 0px; background-color: #cccccc; height: 140px;">
    <h5>Solicitudes de contactos</h5>
    <c:if test="${empty contactos }">
        <p>Sin solicitudes de contactos pendientes...</p>
    </c:if>
    <c:if test="${not empty contactos }">
        <table id="tblSolicitudes" class="table table-hover table-condensed" style="margin-bottom: 0px;">
            <tbody>
                <c:forEach var="contacto" items="${contactos}"  varStatus="status">
                    <tr>
                        <td>${contacto.nombres} ${contacto.apellidos}</td>
                        <td>${contacto.email}</td>
                        <td style="text-align: center">
                            <a id="${contacto.id}" href='#' class="a_contacto_aceptar">
                                <i class="icon-plus"></i>
                            </a>
                        </td>
                        <td style="text-align: center">
                            <a id="${contacto.id}" href='#' class="a_contacto_rechazar">
                                <i class="icon-remove"></i>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <div class="pull-right"><a href="<c:url value="listar_solicitud_recibida.html"/>">Ver m�s...</a></div>
    </c:if>
</div>
--%>

<div id="divPendientes" class="well well-small span6 main_sub_contenedor" style="margin-left: 0px;">
    <h5 class="main_subtitulo">Documentos Pendientes</h5>
    <c:if test="${empty pendientes }">
        <p><i>Sin documentos pendientes.</i></p>
    </c:if>
    <c:if test="${not empty pendientes }">
        <table id="tblPendientes" class="table table-hover table-condensed main_table">
            <tbody>
                <c:forEach var="pendiente" items="${pendientes}"  varStatus="status">
                    <tr title="${pendiente.mensaje}">
                        <td id="${pendiente.id}">
                            <a id="ver" href="ver_documento.html?idDocumento=${pendiente.id}">
                                ${pendiente.nombre}
                            </a>
                        </td>
                        <td style="text-align: center">
                            <a id="ver" href="ver_documento.html?idDocumento=${pendiente.id}"><i class="icon-eye-open"></i></a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <div class="pull-right"><a href="<c:url value="listar_documento.html"/>">Ver m�s...</a></div>
    </c:if>
</div>

<div id="divObservados" class="well well-small span6 main_sub_contenedor">
    <h5 class="main_subtitulo">Documentos en Observaci�n</h5>
    <c:if test="${empty observados }">
        <p><i>Sin documentos en observaci�n.</i></p>
    </c:if>
    <c:if test="${not empty observados }">
        <table id="tblObservados" class="table table-hover table-condensed main_table">
            <tbody>
                <c:forEach var="observado" items="${observados}"  varStatus="status">
                    <tr title="${observado.mensaje}">
                        <td id="${observado.id}">
                            <a id="ver" href="ver_documento.html?idDocumento=${observado.id}">
                                ${observado.nombre}
                            </a>
                        </td>
                        <td style="text-align: center">
                            <a id="ver" href="ver_documento.html?idDocumento=${observado.id}"><i class="icon-eye-open"></i></a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <div class="pull-right"><a href="<c:url value="listar_documento.html"/>">Ver m�s...</a></div>
    </c:if>
</div>

<div id="mdlHistorial" class="modal hide fade" style="width: 700px; margin-left: -330px">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Notificaciones</h3>
    </div>
    <div class="modal-body">
        <table id="tblHistorial" class="table table-hover table-condensed" style="margin-bottom: 0px; font-size: 12px;">
            <thead>
                <tr>
                    <th style="line-height: 12px;" class="span2">Fecha</th>
                    <th style="line-height: 12px;" class="span2">Documento</th>
                    <th style="line-height: 12px;" class="span2">Usuario</th>
                    <th style="line-height: 12px;">Evento</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
    </div>
</div>
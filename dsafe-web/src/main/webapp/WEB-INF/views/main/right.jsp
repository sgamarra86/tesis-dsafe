<%-- 
    Document   : center
    Created on : 01/10/2013, 10:35:13 PM
    Author     : Sergio Gamarra
--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<style type="text/css">
    body {
        background-color: #F5F5F5;

    }
    .form-signin {
        background-color: #FFFFFF;
        border: 1px solid #E5E5E5;
        border-radius: 5px 5px 5px 5px;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
        margin: 0 auto 20px;
        max-width: 300px;
        padding: 19px 29px 29px;
    }
    .form-signin .form-signin-heading, .form-signin .checkbox {
        margin-bottom: 10px;
    }
    .form-signin input[type="text"], .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
    }
    .form-background {
        background-color: #FFFFFF;
        border: 1px solid #E5E5E5;
    }
</style>
<div class="span2">
    <div class="well sidebar-nav form-background">
        <ul class="nav nav-list">
            <li class="nav-header">Promociones</li>
            <li><a href="#">PubliLed 2x1</a></li>
            <li><a href="#">Safari 3x1</a></li>
            <li><a href="#">Sauron 4x1</a></li>
            <li class="nav-header">Novedades</li>
            <li><a href="#">Novedad 1</a></li>
            <li><a href="#">Novedad 2</a></li>
            <li><a href="#">Novedad 3</a></li>
            <li><a href="#">Novedad 4</a></li>
            <li class="nav-header">Etc</li>
            <li><a href="#">Link</a></li>
            <li><a href="#">Link</a></li>
            <li><a href="#">Link</a></li>
        </ul>
    </div>
</div>

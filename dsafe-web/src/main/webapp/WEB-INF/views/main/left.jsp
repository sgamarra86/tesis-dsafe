<%-- 
    Document   : left
    Created on : 01/10/2013, 10:35:13 PM
    Author     : Sergio Gamarra
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script src="<c:url value='/js/globalize.js'/>" type="text/javascript"></script> 
<script src="<c:url value='/js/dx.chartjs.js'/>" type="text/javascript"></script> 

<!--<div class="span3" style="margin-left: 0px">-->
<!--    <div class="well sidebar-nav form-background">-->
<ul class="nav nav-list">
    <li class="nav-header">Contactos</li>
    <li><a href="<c:url value="listar_solicitud_recibida.html"/>">Solicitudes de contacto<span class="badge badge-info pull-right">${numContactos}</span></a></li>
    <li class="nav-header">Documentos</li>
    <li><a href="<c:url value="listar_documento.html?tab=0"/>">Pendientes<span class="badge badge-info pull-right">${numPendientes}</span></a></li>
    <li><a href="<c:url value="listar_documento.html?tab=1"/>">Firmados<span class="badge badge-info pull-right">${numFirmados}</span></a></li>
    <li><a href="<c:url value="listar_documento.html?tab=2"/>">Observando<span class="badge badge-info pull-right">${numObservados}</span></a></li>
    <li class="divider"></li>
    <!--    Aca el piechart-->
    <div id="chartContainer" style="max-width:800px;height: 300px;"></div>
    
    <script type="text/javascript">
        $(document).ready(function() {
            var modelAttributeValue = '${dataSource}';
            var datos = JSON.parse(modelAttributeValue);
            $("#chartContainer").dxPieChart({
                size: {
                    width: 175
                },
                dataSource: datos,
                series: [
                    {
                        argumentField: "tipo",
                        valueField: "cantidad",
                        type: 'doughnut',
                        label: {
                            visible: true,
                            connector: {
                                visible: true,
                                width: 1
                            }
                        }
                    }
                ],
                title: {
                    text: 'Gr�fico Documentos',
                    font: {
                        size: 16,
                        weight: 500
                    }
                },
                legend: {
                    horizontalAlignment: 'center',
                    verticalAlignment: 'bottom'
                }

            });
        });
    </script>
</ul>


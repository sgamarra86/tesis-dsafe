<%-- 
    Document   : ver_documento
    Created on : 14/10/2013, 06:25:21 PM
    Author     : Sergio Gamarra
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script src="<c:url value='/js/documento/ver_documento.js'/>" type="text/javascript"></script> 

<style type="text/css">
    .documento{
        font-size: 12px;
    }
    .documento dt{
        width : 120px;
    }
    .documento dd{
        margin-left : 140px;
    }
    .firma{
        font-size: 12px;
    }
    .firma dl{
        margin-top: 0px;
    }
    .firma dt{
        width : 70px;
    }
    .firma dd{
        margin-left : 80px;
    }
    .btnAccion{
        margin-bottom: 2px;
        width: 100px;
        text-align: left;
    }
    dl h6{
        margin: 0;
    }
</style>


<h3 class="titulo" <c:if test="${documento.reportado}">style="color: red"</c:if>>Verificación de documento</h3>
<input type="hidden" id="hidDocumento" value="${documento.id}">
<fieldset>
    <dl class="dl-horizontal span10 documento" style="margin-top: -10px; margin-bottom: 5px;">
        <dt>Nombre</dt>
        <dd>${documento.nombre} <c:if test="${documento.reportado}"><br><span style="color:red">(Documento reportado, revise el historial para más detalle.)</span></c:if></dd>
            <dt>Descripción</dt>
            <dd style="overflow-y: auto; max-height: 40px;">${documento.descripcion}</dd>
        <dt>Fecha Creación</dt>
        <dd>${documento.fechaRegistro}</dd>
        <dt>Firmas</dt>
        <dd><a id="${documento.id}" href="#" class="lnkFirmas">${documento.firmasRealizadas}/${documento.firmasRequeridas}</a></dd>
    </dl>
    <dl class="span2" style="margin-top: -10px; margin-bottom: 5px;">
        <c:if test="${documento.firmaRequerida eq true }">
            <button id="btnFirmar" class="btn btn-small btn-primary btnAccion" type="button" style="margin-bottom: 5px;"><i class=" icon-check icon-white"></i> Firmar</button>
        </c:if>
        <button id="btnDescargar" class="btn btn-small btn-info btnAccion" type="button"><i class=" icon-download icon-white"></i> Descargar</button>       
        <button id="btnReportar" class="btn btn-small btn-info btnAccion" type="button"><i class=" icon-exclamation-sign icon-white"></i> Reportar</button>       
        <button id="btnHistorial" class="btn btn-small btn-info btnAccion" type="button"><i class=" icon-time icon-white"></i> Historial</button>       
    </dl>
</fieldset>

<h4 class="subtitulo">Firmas realizadas</h4>
<fieldset>
    <div style="margin-top:-10px;height:320px; overflow-y: auto; padding-right: 2px;">
        <div></div>
        <c:if test="${empty documento.firmas }">
            <p>No se han realizado firmas en este documento...</p>
        </c:if>
        <c:forEach items="${documento.firmas}" var="firma">

            <c:if test="${firma.correcto eq true}">
                <c:set var="estadoFirma" value="alert alert-success"/>
            </c:if>
            <c:if test="${firma.correcto eq false}">
                <c:set var="estadoFirma" value="alert alert-error"/>
            </c:if>

            <div class="table-bordered span12 firma ${estadoFirma}" style="margin-left: 0px;margin-bottom: 5px; padding: 0px;">
                <div class="span3" style="padding: 10px;">
                    <c:if test="${empty firma.urlAvatar }">
                        <img src="<c:url value="img/unknown_user.png"/>">
                    </c:if>
                    <c:if test="${not empty firma.urlAvatar }">
                        <img src="<c:url value="${firma.urlAvatar}"/>" style="margin-left: 6px;">
                    </c:if>
                </div>
                <div class="span9" style="margin-top: 10px; min-height: 0px;">
                    <c:if test="${firma.correcto eq true}">
                        <span class="label label-info"><b>Estado:</b>&nbsp;${firma.mensaje}.</span>
                    </c:if>
                    <c:if test="${firma.correcto eq false}">
                        <span class="label label-important"><b>Estado:</b>&nbsp;${firma.mensaje}.</span>
                    </c:if>
                </div>
                <div class="span4">
                    <dl class="dl-horizontal">
                        <h6>Datos de Usuario</h6>
                        <dt>Usuario</dt>
                        <dd>${firma.usuario}</dd>
                        <dt>Email</dt>
                        <dd>${firma.email}</dd>
                        <dt>Fecha</dt>
                        <dd>${firma.fechaFirma}</dd>
                        <dt>Firma</dt>
                        <dd><img src="<c:url value="${firma.urlFirma}"/>" style="height: 58px;"></dd>
                    </dl>
                </div>
                <c:if test="${firma.correcto eq true}">
                    <div class="span5">
                        <dl class="dl-horizontal">
                            <h6>Datos del Certificado</h6>
                            <dt>Propietario</dt>
                            <dd>${firma.propietario}</dd>
                            <dt>Autorizador</dt>
                            <dd>${firma.autorizador}</dd>
                            <dt>Expedición</dt>
                            <dd>${firma.fechaExpedicion}</dd>
                            <dt>Expiración</dt>
                            <dd>${firma.fechaExpiracion}</dd>
                            <dt>Cert. Digital</dt>
                            <dd><a id="lnkDescargar" href="<c:url value="${firma.urlCertificado}"/>">Descargar&nbsp;<span class="icon-download"></span></a></dd>
                        </dl>
                    </div>
                </c:if>
            </div>
        </c:forEach>
    </div>
</fieldset>


<div id="mdlFirmas" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id ="titulo">Documento: ${documento.nombre}</h3>
    </div>
    <div class="modal-body">
        <div class="well-small well main_sub_contenedor_largo" style="margin-bottom: 0px;">
            <table id="tblFirmas" class="table table-hover">
                <thead>
                    <tr>
                        <th style="text-align: center">Firmante</th>
                        <th style="text-align: center">Estado</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
            <h5>Progreso:</h5>
            <div class="progress">
                <div class="bar" style="width: 0%;"><div>0%</div></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cerrar</button>
    </div>
</div>



<div id="mdlFirmarDocumento" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Firmar documento</h3>
    </div>
    <div class="modal-body">
        <div class="well-small well alert-info">
            Ingrese los datos solicitados para realizar la firma del documento.
        </div>
        <c:url var="url" value="/firmar_documento.html" />
        <form:form id="frmFirmarDocumento" modelAttribute="firmarDocumento" method="post" action="${url}" cssClass="form-horizontal"  enctype="multipart/form-data">
            <div class="control-group">
                <label for="pkcs12" class="control-label">PKCS12</label>
                <div class="controls">
                    <form:input id="pkcs12" path="pkcs12" type="file"  placeholder="Seleccione su PKCS12"/>
                    <form:errors cssClass="help-block" path="pkcs12" />
                </div>
            </div>
            <div class="control-group">
                <label for="password" class="col-sm-2 control-label">Contraseña</label>
                <div class="controls">
                    <form:password id="password" path="password" cssClass="span9" placeholder="Ingrese su contraseña."/>
                    <form:errors cssClass="help-block" path="password" />
                </div>
            </div>
            <form:hidden id="inpDocumentos" path="documentos" cssClass="form-control  span9" placeholder="Documentos"/>
        </form:form>
    </div>
    <div class="modal-footer">
        <button id="btnAceptar" class="btn btn-primary">Aceptar</button>
        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cerrar</button>
    </div>
</div>


<div id="mdlReportar" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Reportar Documento</h3>
    </div>
    <div class="modal-body">
        <div class="well-small well alert-info">
            Ingrese la descripción del problema encontrado en la verificación de este documento.
        </div>
        <c:url var="url" value="/registrar_notificacion.html" />
        <form:form id="frmRegistrarNotificacion" modelAttribute="registrarNotificacion" method="post" action="${url}" cssClass="form-horizontal"  enctype="multipart/form-data">

            <form:hidden id="inpIdDocumento" path="idDocumento" value="${documento.id}"/>
            <form:hidden id="inpNomDocumento" path="nomDocumento" value="${documento.nombre}"/>
            <form:hidden id="inpTipo" path="tipo" value="2"/>

            <div class="control-group">
                <label for="nombre" class="control-label">Documento</label>
                <div class="controls">
                    <input type="text" readonly="true" value="${documento.nombre}" class="span9">
                </div>
            </div>
            <div class="control-group">
                <label for="descripcion" class="control-label">Descripción</label>
                <div class="controls">
                    <form:textarea id="descripcion" path="descripcion" cssClass="form-control  span9" placeholder="Descripción de la notificación."/>
                    <span id="descripcion.errors" class="help-block hide">Debe ingresar una descripción del problema.</span>
                </div>
            </div>
        </div>
    </form:form>
    <div class="modal-footer">
        <button id="btnReportarAceptar" class="btn btn-primary">Aceptar</button>
        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cerrar</button>
    </div>
</div>

<div id="mdlHistorial" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Historial de cambios</h3>
    </div>
    <div class="modal-body">
        <div class="well-small well alert-info">
            En esta sección se muestran todas las notificaciones generadas.
        </div>
        <div class="well-small well main_sub_contenedor_largo" style="margin-bottom: 0px;">
            <div style ="height:250px; overflow-x:hidden; overflow-y:auto;">

            <table id="tblHistorial" class="table table-hover table-condensed" style="margin-bottom: 0px; font-size: 12px;">
                <thead>
                    <tr>
                        <th style="line-height: 12px;" >Fecha</th>
                        <th style="line-height: 12px;" >Usuario</th>
                        <th style="line-height: 12px;" >Evento</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
                </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cerrar</button>
    </div>
</div>
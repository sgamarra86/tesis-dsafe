<%-- 
    Document   : administrar_documento
    Created on : 14/10/2013, 06:25:21 PM
    Author     : Sergio Gamarra
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script src="<c:url value='/js/bootstrap-paginator.js'/>" type="text/javascript"></script> 
<script src="<c:url value='/js/documento/administrar_documento.js'/>" type="text/javascript"></script> 

<h3 class="titulo">Mis documentos a�adidos</h2>

<div class="well-small well alert-info">
    En esta secci�n usted puede revisar los documentos que usted haya a�adido, tanto como la opci�n de poder eliminarlos.
    <b>Aviso:</b> Los documentos de color rojo han sido reportados, revise el historial del documento para mayor detalle.
</div>

<div class="well-small well main_sub_contenedor_largo">
    <c:if test="${empty documentos }">
        <p>No se cuenta con documentos a�adidos.</p>
    </c:if>    
    <c:if test="${not empty documentos }">
        <table id="tblDocumentos" class="table table-hover main_table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Fecha</th>
                    <th style="text-align: center">Firmas</th>
                    <th style="text-align: center">Observadores</th>
                    <th style="text-align: center">Ver</th>
                    <th style="text-align: center">Eliminar</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="documento" items="${documentos}"  varStatus="status">
                    <tr <c:if test="${documento.reportado}">style="color:red"</c:if>>
                        <td>${documento.nombre}</td>
                        <td>${documento.fecha}</td>
                        <td style="text-align: center">
                            <a id="${documento.id}" href="#" class="lnkFirmas" >${documento.firmasNecesarias}</a>
                        </td>                     
                        <td style="text-align: center">
                            <a id="${documento.id}" href="#" class="lnkObservados" >${documento.destinatarios}</a>
                        </td>
                        <td style="text-align: center">
                            <a id="ver" href="ver_documento.html?idDocumento=${documento.id}"><i class="icon-eye-open"></i></a>
                        </td>
                        <td style="text-align: center">
                            <a id="${documento.id}" href='#' class="lnkEliminar">
                                <span class="icon-trash"></span>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

        <div id="paginador"></div>
    </c:if>
</div>

<div id="mdlFirmas" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id ="titulo"></h3>
    </div>
    <div class="modal-body">
        <table id="tblFirmas" class="table table-hover">
            <thead>
                <tr>
                    <th style="text-align: center">Firmante</th>
                    <th style="text-align: center">Estado</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
        <h5>Progreso:</h5>
        <div class="progress">
            <div class="bar" style="width: 0%;"><div>0%</div></div>
        </div>
    </div>
</div>

<div id="mdlObservadores" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id ="titulo"></h3>
    </div>
    <div class="modal-body">
        <table id="tblObservadores" class="table table-hover">
            <thead>
                <tr>
                    <th style="text-align: center">Observador</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

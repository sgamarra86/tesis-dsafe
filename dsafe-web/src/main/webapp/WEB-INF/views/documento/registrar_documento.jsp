<%-- 
    Document   : registrar_documento
    Created on : 12/03/2014, 10:12:42 AM
    Author     : Sergio Gamarra
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<link rel="stylesheet" type="text/css" href="<c:url value="/css/multi-select.css"/>" />
<script src="<c:url value='/js/jquery.multi-select.js'/>" type="text/javascript"></script> 
<script src="<c:url value='/js/jquery.quicksearch.js'/>" type="text/javascript"></script> 
<script src="<c:url value='/js/documento/registrar_documento.js'/>" type="text/javascript"></script> 


<style type="text/css">
    body {
        background-color: #0B496E;
    }

    .controls .help-block{
        color: red;
    }
</style>


<h3 class="titulo">A�adir documento</h3>

<div class="well-small well alert-info">
    En esta secci�n usted puede a�adir un documento a su cuenta para que se le realicen las firmas.
</div>

<c:url var="url" value="/registrar_documento.html" />
<form:form id="frmRegistrarDocumento" modelAttribute="registrarDocumento" method="post" action="${url}" cssClass="form-horizontal"  enctype="multipart/form-data">
    <div style ="height:400px; overflow-x:hidden; overflow-y:auto;">
        <div class="control-group">
            <label for="inputArchivo" class="control-label">Archivo</label>
            <div class="controls">
                <form:input id="archivo" path="archivo" type="file"  placeholder="Seleccione un archivo"/>
                <form:errors cssClass="help-block" path="archivo"/>
            </div>
        </div>
        <div class="control-group">
            <label for="inputDescripcion" class="control-label">Descripci�n</label>
            <div class="controls">
                <form:textarea id="descripcion" path="descripcion" cssClass="span9" placeholder="Descripci�n del documento." cssStyle="height:50px;"/>
                <form:errors cssClass="help-block" path="descripcion"/>
            </div>
        </div>
        <div class="control-group">
            <label for="inputFirmas" class="control-label">Firmas</label>
            <div class="controls">
                <input type="text" id="inpNomFirmantes" class="span9" placeholder="Firmas requeridas del documento.">
                <form:hidden id="idFirmantes" path="idFirmantes" placeholder="Firmas requeridas del documento."/>
                <form:errors cssClass="help-block" path="idFirmantes"/>
            </div>
        </div>
        <div class="control-group">
            <label for="inputMailFirmantes" class="control-label">Mensaje</label>
            <div class="controls">
                <form:textarea id="mailFirmantes" path="mailFirmantes" cssClass="span9" placeholder="Mensaje a enviar a los usuarios de firma requerida." cssStyle="height:50px;"/>
                <form:errors cssClass="help-block" path="mailFirmantes"/>
            </div>
        </div>
        <div class="control-group">
            <label for="inputObservadores" class="control-label">Observadores</label>
            <div class="controls">
                <input type="text" id="inpNomObservadores" class="span9" placeholder="Observadores del documento.">
                <form:hidden id="idObservadores" path="idObservadores" placeholder="Observadores del documento."/>
                <form:errors cssClass="help-block" path="idObservadores"/>
            </div>
        </div>
        <div class="control-group">
            <label for="inputMailObservadores" class="control-label">Mensaje</label>
            <div class="controls">
                <form:textarea id="mailObservadores" path="mailObservadores" cssClass="span9" placeholder="Mensaje a enviar a los usuarios observadores." cssStyle="height:50px;"/>
                <form:errors cssClass="help-block" path="mailObservadores"/>
            </div>
        </div>


        <div class="text-center">
            <button type="submit" class="btn btn-primary"><spring:message code="btn.guardar"/></button>
            <button id="btnCancelar" type="button" class="btn btn-info"><spring:message code="btn.cancelar"/></button>
        </div>

    </div>    
</form:form>

<div id="modalFirmas" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Firmas requeridas</h3>
    </div>
    <div>
        <p></p>
    </div>
    <div class="modal-body">
        <div>
            <select id='selContactos' multiple='multiple'>
                <c:forEach var="contacto" items="${contactos}" varStatus="status">
                    <option id="opt${contacto.id}" value='${contacto.id}'>${contacto.nombres} ${contacto.apellidos}</option>
                </c:forEach>
            </select>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary">Aceptar</button>
        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cerrar</button>
    </div>
</div>

<div id="modalObservadores" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Observadores</h3>
    </div>
    <div>
        <p></p>
    </div>
    <div class="modal-body">
        <div>
            <select id='selObservadores' multiple='multiple'>
                <c:forEach var="contacto" items="${contactos}" varStatus="status">
                    <option id="opt${contacto.id}" value='${contacto.id}'>${contacto.nombres} ${contacto.apellidos}</option>
                </c:forEach>
            </select>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary">Aceptar</button>
        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cerrar</button>
    </div>
</div>
<%-- 
    Document   : listar_documento
    Created on : 14/10/2013, 06:25:21 PM
    Author     : Sergio Gamarra
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script src="<c:url value='/js/bootstrap-paginator.js'/>" type="text/javascript"></script>
<script src="<c:url value='/js/documento/listar_documento.js'/>" type="text/javascript"></script>

<h3 class="titulo">Documentos digitales</h2>

<div class="well-small well alert-info">
    En esta sección usted puede revisar los documentos que requieren su firma, los ya firmados y los en observación.
    <b>Aviso:</b> Los documentos de color rojo han sido reportados, revise el historial del documento para mayor detalle.
</div>

<ul class="nav nav-tabs" id="myTab" style="font-size: 12px; margin-bottom: 0px;" tabSeleccionado="${tab}">
    <li id="tabPendientes"><a id="lnkTabPendientes" href="#pendientes">Pendientes</a></li>
    <li id="tabFirmados"><a id="lnkTabFirmados" href="#firmados">Firmados</a></li>
    <li id="tabObservados"><a id="lnkTabObservados" href="#observados">Observando</a></li>
</ul>

<div class="tab-content">
    <div id="pendientes" class="tab-pane active well-small well main_sub_contenedor_largo">
        <c:if test="${empty pendientes }">
            <p>No se cuenta con documentos pendientes...</p>
        </c:if>
        <c:if test="${not empty pendientes }">
            <div style="height: 220px;">
                <table id="tblPendientes" class="table table-hover main_table">
                    <thead>
                        <tr>
                            <th style="width: 10px;"></th>
                            <th class="span3">Nombre</th>
                            <th class="span1" style="text-align: center">Fecha</th>
                            <th class="span1" style="text-align: center">Firmas</th>
                            <th class="span1" style="text-align: center">Observadores</th>
                            <th class="span1" style="text-align: center">Ver</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="pendiente" items="${pendientes}"  varStatus="status">                        
                            <tr title="${pendiente.descripcion}" <c:if test="${pendiente.reportado}">style="color:red"</c:if>>
                                <td><input type="checkbox" value="${pendiente.id}"></td>
                                <td id="tdNombre${pendiente.id}">
                                    <a id="ver" href="ver_documento.html?idDocumento=${pendiente.id}">
                                        ${pendiente.nombre}
                                    </a>
                                </td>
                                <td style="text-align: center">${pendiente.fecha}</td>
                                <td style="text-align: center">
                                    <a id="${pendiente.id}" href="#" class="lnkFirmas" >${pendiente.firmasRealizadas}/${pendiente.firmasNecesarias}</a>
                                </td>
                                <td style="text-align: center">
                                    <a id="${pendiente.id}" href="#" class="lnkObservados" >${pendiente.destinatarios}</a>
                                </td>
                                <td style="text-align: center">
                                    <a id="ver" href="ver_documento.html?idDocumento=${pendiente.id}"><i class="icon-eye-open"></i></a>
<!--                                    <a id="ver" href="ver_documento.html?idDocumento=${pendiente.id}"><img src="<c:url value="/img/icon-eye.png"/>"></a>-->
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
            <div id="paginadorPendientes"></div>

            <div class="text-right">
                <button id="btnFirmar" type="button" class="btn btn-primary"><i class=" icon-check icon-white"></i>Firmar</button>
            </div>
        </c:if>
    </div>

    <div class="tab-pane well-small well main_sub_contenedor_largo" id="firmados">
        <c:if test="${empty firmados }">
            <p>No se cuenta con documentos firmados...</p>
        </c:if>
        <c:if test="${not empty firmados }">
            <div style="height: 220px;">
                <table id="tblFirmados" class="table table-hover main_table">
                    <thead>
                        <tr>
                            <th style="width: 10px;"></th>
                            <th class="span3">Nombre</th>
                            <th class="span1" style="text-align: center">Fecha</th>
                            <th class="span1" style="text-align: center">Firmas</th>
                            <th class="span1" style="text-align: center">Observadores</th>
                            <th class="span1" style="text-align: center">Ver</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="firmado" items="${firmados}"  varStatus="status">
                            <tr title="${firmado.descripcion}" <c:if test="${firmado.reportado}">style="color:red"</c:if>>
                                    <td></td>
                                    <td>
                                        <a id="ver" href="ver_documento.html?idDocumento=${firmado.id}">
                                        ${firmado.nombre}
                                    </a>
                                </td>
                                <td style="text-align: center">${firmado.fecha}</td>
                                <td style="text-align: center">
                                    <a id="${firmado.id}" href="#" class="lnkFirmas" >${firmado.firmasRealizadas}/${firmado.firmasNecesarias}</a>
                                </td>
                                <td style="text-align: center">
                                    <a id="${firmado.id}" href="#" class="lnkObservados" >${firmado.destinatarios}</a>
                                </td>
                                <td style="text-align: center">
                                    <a id="ver" href="ver_documento.html?idDocumento=${firmado.id}"><i class="icon-eye-open"></i></a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
            <div id="paginadorFirmados"></div>

        </c:if>
    </div>
    <div class="tab-pane well-small well main_sub_contenedor_largo" id="observados">
        <c:if test="${empty observados }">
            <p>No se cuenta con documentos en observación...</p>
        </c:if>
        <c:if test="${not empty observados }">
            <div style="height: 220px;">
                <table id="tblObservados" class="table table-hover main_table">
                    <thead>
                        <tr>
                            <th style="width: 10px;"></th>
                            <th class="span3">Nombre</th>
                            <th class="span1" style="text-align: center">Fecha</th>
                            <th class="span1" style="text-align: center">Firmas</th>
                            <th class="span1" style="text-align: center">Observadores</th>
                            <th class="span1" style="text-align: center">Ver</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="observado" items="${observados}"  varStatus="status">
                            <tr title="${observado.descripcion}" <c:if test="${observado.reportado}">style="color:red"</c:if>>
                                    <td style="width: 10px;"></td>
                                    <td>
                                        <a id="ver" href="ver_documento.html?idDocumento=${observado.id}">
                                        ${observado.nombre}
                                    </a>
                                </td>
                                <td style="text-align: center">${observado.fecha}</td>
                                <td style="text-align: center">
                                    <a id="${observado.id}" href="#" class="lnkFirmas" >${observado.firmasRealizadas}/${observado.firmasNecesarias}</a>
                                </td>
                                <td style="text-align: center">
                                    <a id="${observado.id}" href="#" class="lnkObservados" >${observado.destinatarios}</a>
                                </td>
                                <td style="text-align: center">
                                    <a id="ver" href="ver_documento.html?idDocumento=${observado.id}"><i class="icon-eye-open"></i></a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
            <div id="paginadorObservados"></div>
        </c:if>
    </div>
</div>


<div id="mdlFirmarDocumento" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Firmar documentos</h3>
    </div>
    <div class="modal-body">
        <div class="well-small well alert-info">
            Ingrese los datos solicitados para realizar la firma del(los) documento(s).
        </div>
        <c:url var="url" value="/firmar_documento.html" />
        <form:form id="frmFirmarDocumento" modelAttribute="firmarDocumento" method="post" action="${url}" cssClass="form-horizontal"  enctype="multipart/form-data">
            <div class="control-group">
                <label for="pkcs12" class="control-label">PKCS12</label>
                <div class="controls">
                    <form:input id="pkcs12" path="pkcs12" type="file" placeholder="Seleccione su PKCS12"/>
                    <form:errors cssClass="help-block" path="pkcs12" />
                </div>
            </div>
            <div class="control-group">
                <label for="password" class="control-label">Contraseña</label>
                <div class="controls">
                    <form:password id="password" path="password" placeholder="Ingrese su contraseña."/>
                    <form:errors cssClass="help-block" path="password" />
                </div>
            </div>
            <div class="control-group">
                <label for="inpDocumentos" class="control-label">Documentos</label>
                <div class="controls">
                    <ul id="lstDocumentos"></ul>
                    <form:hidden id="inpDocumentos" path="documentos" placeholder="Documentos"/>
                    <form:errors cssClass="help-block" path="documentos" />
                </div>
            </div>
        </form:form>
    </div>
    <div class="modal-footer">
        <button id="btnAceptar" class="btn btn-primary">Aceptar</button>
        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cerrar</button>
    </div>
</div>

<div id="mdlFirmas" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id ="titulo"></h3>
    </div>
    <div class="modal-body">
        <div class="well-small well main_sub_contenedor_largo" style="margin-bottom: 0px;">
            <table id="tblFirmas" class="table table-hover">
                <thead>
                    <tr>
                        <th style="text-align: center">Firmante</th>
                        <th style="text-align: center">Estado</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>

            <h5>Progreso:</h5>
            <div class="progress">
                <div class="bar" style="width: 0%;"><div>0%</div></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cerrar</button>
    </div>
</div>


<div id="mdlObservadores" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id ="titulo"></h3>
    </div>
    <div class="modal-body">
        <div class="well-small well main_sub_contenedor_largo" style="margin-bottom: 0px;">
            <table id="tblObservadores" class="table table-hover">
                <thead>
                    <tr>
                        <th style="text-align: center">Observador</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cerrar</button>
    </div>
</div>

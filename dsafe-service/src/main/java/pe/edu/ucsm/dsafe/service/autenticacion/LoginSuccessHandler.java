package pe.edu.ucsm.dsafe.service.autenticacion;

import java.io.FileOutputStream;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Service;
import pe.edu.ucsm.dsafe.common.base.Constants;
import pe.edu.ucsm.dsafe.dataaccess.domain.UsuarioDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.UsuarioVO;

@Service("loginSuccessHandler")
public class LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    @Autowired
    private UsuarioDAO usuarioDAO;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws ServletException, IOException {
        UsuarioVO usuarioVO = this.usuarioDAO.obtenerUsuarioPorEmail(authentication.getName());
        response.sendRedirect(response.encodeRedirectURL(Constants.REDIRECT_WEB_PRINCIPAL));
        usuarioVO.setEstado(Short.valueOf("1"));
        this.usuarioDAO.update(usuarioVO);
        this.crearImagenEnTemporales(request.getRealPath(""), usuarioVO.getEmail()+"_avatar", usuarioVO.getAvatar());
        this.crearImagenEnTemporales(request.getRealPath(""), usuarioVO.getEmail()+"_firma", usuarioVO.getFirma());
        
    }

    private String crearImagenEnTemporales(String path, String filename, byte[] imagen) {
        String url = "img/" + filename + ".jpg";
        FileOutputStream out;
        try {
            out = new FileOutputStream(path + "/" + url);
            out.write(imagen);
            out.flush();
            out.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return url;
    }
    
        private String crearCertificadoEnTemporales(String path, String filename, byte[] archivo) {
        String url = "crt/" + filename + ".cer";
        FileOutputStream out;
        try {
            out = new FileOutputStream(path + "/" + url);
            out.write(archivo);
            out.flush();
            out.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return url;
    }
}
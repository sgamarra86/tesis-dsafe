/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.notificacion.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.ucsm.dsafe.common.base.Constants;
import pe.edu.ucsm.dsafe.common.base.DSafeFecha;
import pe.edu.ucsm.dsafe.dataaccess.domain.NotificacionDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.NotificacionVO;
import pe.edu.ucsm.dsafe.service.notificacion.GestionarNotificacion;
import pe.edu.ucsm.dsafe.service.notificacion.dto.RegistrarNotificacionDTO;
import pe.edu.ucsm.dsafe.service.notificacion.dto.ListarNotificacionDTO;

/**
 *
 * @author Sergio Gamarra
 */
@Service("gestionarNotificacion")
public class GestionarNotificacionImpl implements GestionarNotificacion {

    @Autowired
    private NotificacionDAO notificacionDAO;

    @Override
    public List<ListarNotificacionDTO> obtenerNotificacionesPorIdDocumentos(Integer[] idDocumentos) {
        List<ListarNotificacionDTO> notificaciones = new ArrayList<ListarNotificacionDTO>();
        List<NotificacionVO> lstVO = this.notificacionDAO.obtenerNotificacionesPorIdDocumentos(idDocumentos);
        ListarNotificacionDTO dto;
        for (NotificacionVO vo : lstVO) {
            dto = new ListarNotificacionDTO();
            dto.setIdDocumento(vo.getIdDocumento());
            dto.setNomDocumento(vo.getNomDocumento());
            dto.setNomUsuario(vo.getNomUsuario());
            dto.setDescripcion(vo.getDescripcion());
            dto.setFecha(DSafeFecha.standard_hora(vo.getFecha().getTime()));
            dto.setTipo(vo.getTipo());
            notificaciones.add(dto);
        }
        return notificaciones;
    }
    
    @Override
    public List<ListarNotificacionDTO> obtenerNotificacionesPorIdDocumentosResumido(Integer[] idDocumentos) {
        List<ListarNotificacionDTO> notificaciones = new ArrayList<ListarNotificacionDTO>();
        List<NotificacionVO> lstVO = this.notificacionDAO.obtenerNotificacionesPorIdDocumentosResumido(idDocumentos);
        ListarNotificacionDTO dto;
        for (NotificacionVO vo : lstVO) {
            dto = new ListarNotificacionDTO();
            dto.setIdDocumento(vo.getIdDocumento());
            dto.setNomDocumento(vo.getNomDocumento());
            dto.setNomUsuario(vo.getNomUsuario());
            dto.setDescripcion(vo.getDescripcion());
            dto.setFecha(DSafeFecha.standard_hora(vo.getFecha().getTime()));
            dto.setTipo(vo.getTipo());
            notificaciones.add(dto);
        }
        return notificaciones;
    }

    @Override
    public Integer registrarNotificacion(RegistrarNotificacionDTO dto) {
        NotificacionVO vo = new NotificacionVO();
        vo.setIdUsuario(dto.getIdUsuario());
        vo.setIdDocumento(dto.getIdDocumento());
        vo.setNomUsuario(dto.getNomUsuario());
        vo.setNomDocumento(dto.getNomDocumento());
        vo.setDescripcion(dto.getDescripcion());

        switch (dto.getTipo()) {
            case Constants.DOCUMENTO_REGISTRADO:
                vo.setDescripcion("Documento registrado.");
                break;
            case Constants.DOCUMENTO_FIRMADO:
                vo.setDescripcion("Documento firmado.");
                break;
            case Constants.DOCUMENTO_REPORTADO:
                vo.setDescripcion("Documento reportado: " + dto.getDescripcion());
                break;
        }

        vo.setTipo(dto.getTipo());
        vo.setFecha(Calendar.getInstance());
        return this.notificacionDAO.insert(vo);
    }

    @Override
    public boolean esDocumentoReportado(Integer idDocumento) {
        return this.notificacionDAO.esDocumentoReportado(idDocumento);
    }
}

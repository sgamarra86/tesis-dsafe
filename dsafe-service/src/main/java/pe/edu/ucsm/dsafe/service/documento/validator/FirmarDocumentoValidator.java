/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.documento.validator;

import org.springframework.validation.Validator;
import org.springframework.validation.Errors;
import org.springframework.stereotype.Service;
import pe.edu.ucsm.dsafe.common.base.Validador;
import pe.edu.ucsm.dsafe.service.documento.dto.FirmarDocumentoDTO;
import pe.edu.ucsm.dsafe.service.documento.dto.RegistrarDocumentoDTO;

/**
 *
 * @author Sergio Gamarra
 */
@Service("firmarDocumentoValidator")
public class FirmarDocumentoValidator implements Validator {

    @SuppressWarnings("unchecked")
    @Override
    public boolean supports(Class<?> clazz) {
        return FirmarDocumentoDTO.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        FirmarDocumentoDTO firmaDTO = (FirmarDocumentoDTO) target;

        //Descripcion
        if (!Validador.noNuloNoVacio(firmaDTO.getPassword())) {
            errors.rejectValue("password", "val.campo_vacio");
        }
        
        //Descripcion
        if (!Validador.noNuloNoVacio(firmaDTO.getPassword())) {
            errors.rejectValue("password", "val.campo_vacio");
        }
    }
}
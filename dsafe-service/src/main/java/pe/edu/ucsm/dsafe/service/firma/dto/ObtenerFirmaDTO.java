/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.firma.dto;

/**
 *
 * @author Sergio Gamarra
 */
public class ObtenerFirmaDTO {
    private String usuario;
    private short estado;

    public ObtenerFirmaDTO(String usuario, short estado) {
        this.usuario = usuario;
        this.estado = estado;
    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the estado
     */
    public short getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(short estado) {
        this.estado = estado;
    }
}

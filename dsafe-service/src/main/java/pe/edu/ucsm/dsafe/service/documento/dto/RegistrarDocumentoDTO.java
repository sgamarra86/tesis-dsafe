/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.documento.dto;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 *
 * @author Sergio Gamarra
 */
public class RegistrarDocumentoDTO {

    private CommonsMultipartFile archivo;
    private Integer propietario;
    private String descripcion;
    private String idFirmantes;
    private String mailFirmantes;
    private String idObservadores;
    private String mailObservadores;

    /**
     * @return the archivo
     */
    public CommonsMultipartFile getArchivo() {
        return archivo;
    }

    /**
     * @param archivo the archivo to set
     */
    public void setArchivo(CommonsMultipartFile archivo) {
        this.archivo = archivo;
    }

    /**
     * @return the propietario
     */
    public Integer getPropietario() {
        return propietario;
    }

    /**
     * @param propietario the propietario to set
     */
    public void setPropietario(Integer propietario) {
        this.propietario = propietario;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the idFirmantes
     */
    public String getIdFirmantes() {
        return idFirmantes;
    }

    /**
     * @param idFirmantes the idFirmantes to set
     */
    public void setIdFirmantes(String idFirmantes) {
        this.idFirmantes = idFirmantes;
    }

    /**
     * @return the mailFirmantes
     */
    public String getMailFirmantes() {
        return mailFirmantes;
    }

    /**
     * @param mailFirmantes the mailFirmantes to set
     */
    public void setMailFirmantes(String mailFirmantes) {
        this.mailFirmantes = mailFirmantes;
    }

    /**
     * @return the idObservadores
     */
    public String getIdObservadores() {
        return idObservadores;
    }

    /**
     * @param idObservadores the idObservadores to set
     */
    public void setIdObservadores(String idObservadores) {
        this.idObservadores = idObservadores;
    }

    /**
     * @return the mailObservadores
     */
    public String getMailObservadores() {
        return mailObservadores;
    }

    /**
     * @param mailObservadores the mailObservadores to set
     */
    public void setMailObservadores(String mailObservadores) {
        this.mailObservadores = mailObservadores;
    }

}

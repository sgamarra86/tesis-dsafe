/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.documento.dto;

/**
 *
 * @author Sergio Gamarra
 */
public class DatosFirmaDTO {
    //Datos de Usuario
    private String usuario;
    private String email;
    private String fechaFirma;
    private String urlAvatar;
    private String urlFirma;
    private String urlCertificado;
    
    //Datos de Certificado
    private String propietario;
    private String organizacion;
    private String autorizador;
    private String fechaExpedicion;
    private String fechaExpiracion;
    private String mensaje;
    private boolean correcto;

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the fechaFirma
     */
    public String getFechaFirma() {
        return fechaFirma;
    }

    /**
     * @param fechaFirma the fechaFirma to set
     */
    public void setFechaFirma(String fechaFirma) {
        this.fechaFirma = fechaFirma;
    }

    /**
     * @return the urlFirma
     */
    public String getUrlFirma() {
        return urlFirma;
    }

    /**
     * @param urlFirma the urlFirma to set
     */
    public void setUrlFirma(String urlFirma) {
        this.urlFirma = urlFirma;
    }

    /**
     * @return the propietario
     */
    public String getPropietario() {
        return propietario;
    }

    /**
     * @param propietario the propietario to set
     */
    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    /**
     * @return the organizacion
     */
    public String getOrganizacion() {
        return organizacion;
    }

    /**
     * @param organizacion the organizacion to set
     */
    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }

    /**
     * @return the fechaExpedicion
     */
    public String getFechaExpedicion() {
        return fechaExpedicion;
    }

    /**
     * @param fechaExpedicion the fechaExpedicion to set
     */
    public void setFechaExpedicion(String fechaExpedicion) {
        this.fechaExpedicion = fechaExpedicion;
    }

    /**
     * @return the fechaExpiracion
     */
    public String getFechaExpiracion() {
        return fechaExpiracion;
    }

    /**
     * @param fechaExpiracion the fechaExpiracion to set
     */
    public void setFechaExpiracion(String fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    /**
     * @return the autorizador
     */
    public String getAutorizador() {
        return autorizador;
    }

    /**
     * @param autorizador the autorizador to set
     */
    public void setAutorizador(String autorizador) {
        this.autorizador = autorizador;
    }

    /**
     * @return the urlCertificado
     */
    public String getUrlCertificado() {
        return urlCertificado;
    }

    /**
     * @param urlCertificado the urlCertificado to set
     */
    public void setUrlCertificado(String urlCertificado) {
        this.urlCertificado = urlCertificado;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the correcto
     */
    public boolean isCorrecto() {
        return correcto;
    }

    /**
     * @param correcto the correcto to set
     */
    public void setCorrecto(boolean correcto) {
        this.correcto = correcto;
    }

    /**
     * @return the urlAvatar
     */
    public String getUrlAvatar() {
        return urlAvatar;
    }

    /**
     * @param urlAvatar the urlAvatar to set
     */
    public void setUrlAvatar(String urlAvatar) {
        this.urlAvatar = urlAvatar;
    }

}

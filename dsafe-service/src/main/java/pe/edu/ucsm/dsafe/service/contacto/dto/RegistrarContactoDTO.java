/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.contacto.dto;

/**
 *
 * @author Sergio Gamarra
 */
public class RegistrarContactoDTO {

    private String emailUsuario;
    private String emailContacto;

    /**
     * @return the emailUsuario
     */
    public String getEmailUsuario() {
        return emailUsuario;
    }

    /**
     * @param emailUsuario the emailUsuario to set
     */
    public void setEmailUsuario(String emailUsuario) {
        this.emailUsuario = emailUsuario;
    }

    /**
     * @return the emailContacto
     */
    public String getEmailContacto() {
        return emailContacto;
    }

    /**
     * @param emailContacto the emailContacto to set
     */
    public void setEmailContacto(String emailContacto) {
        this.emailContacto = emailContacto;
    }
}

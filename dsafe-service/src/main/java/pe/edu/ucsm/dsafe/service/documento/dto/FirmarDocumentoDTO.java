/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.documento.dto;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 *
 * @author Sergio Gamarra
 */
public class FirmarDocumentoDTO {
    private String[] documentos;
    private String password;
    private CommonsMultipartFile pkcs12;

    /**
     * @return the documentos
     */
    public String[] getDocumentos() {
        return documentos;
    }

    /**
     * @param documentos the documentos to set
     */
    public void setDocumentos(String[] documentos) {
        this.documentos = documentos;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the pkcs12
     */
    public CommonsMultipartFile getPkcs12() {
        return pkcs12;
    }

    /**
     * @param pkcs12 the pkcs12 to set
     */
    public void setPkcs12(CommonsMultipartFile pkcs12) {
        this.pkcs12 = pkcs12;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.usuario.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Validator;
import org.springframework.validation.Errors;
import org.springframework.stereotype.Service;
import pe.edu.ucsm.dsafe.common.base.Constants;
import pe.edu.ucsm.dsafe.common.base.Validador;
import pe.edu.ucsm.dsafe.dataaccess.domain.UsuarioDAO;
import pe.edu.ucsm.dsafe.service.usuario.dto.RegistrarUsuarioDTO;

/**
 *
 * @author Sergio Gamarra
 */
@Service("registrarUsuarioValidator")
public class RegistrarUsuarioValidator implements Validator {

    @SuppressWarnings("unchecked")
    @Override
    public boolean supports(Class<?> clazz) {
        return RegistrarUsuarioDTO.class.isAssignableFrom(clazz);
    }
    @Autowired
    private UsuarioDAO usuarioDAO;

    @Override
    public void validate(Object target, Errors errors) {
        RegistrarUsuarioDTO usuarioDTO = (RegistrarUsuarioDTO) target;

        //Nombres
        if (!Validador.noNuloNoVacio(usuarioDTO.getNombres())) {
            errors.rejectValue("nombres", "val.campo_vacio");
        } else if (!Validador.contieneSoloLetras(usuarioDTO.getNombres())) {
            errors.rejectValue("nombres", "val.solo_letras");
        }

        //Apellidos
        if (!Validador.noNuloNoVacio(usuarioDTO.getApellidos())) {
            errors.rejectValue("apellidos", "val.campo_vacio");
        } else if (!Validador.contieneSoloLetras(usuarioDTO.getApellidos())) {
            errors.rejectValue("apellidos", "val.solo_letras");
        }

        //Email
        if (!Validador.noNuloNoVacio(usuarioDTO.getEmail())) {
            errors.rejectValue("email", "val.usuario.email_vacio");
        } else if (!Validador.esEmail(usuarioDTO.getEmail())) {
            errors.rejectValue("email", "val.usuario.email_formato");
        } else if (usuarioDAO.existeUsuarioPorEmail(usuarioDTO.getEmail())) {
            errors.rejectValue("email", "val.usuario.email_existe");
        }

        //Password
        if (!Validador.noNuloNoVacio(usuarioDTO.getPassword())) {
            errors.rejectValue("password", "val.campo_vacio");
        } else if (!Validador.esAlfanumerico(usuarioDTO.getPassword())) {
            errors.rejectValue("password", "val.solo_letras_numeros");
        }

        //DNI
        if (Validador.noNuloNoVacio(usuarioDTO.getDni())) {
            if (!Validador.esDNI(usuarioDTO.getDni())) {
                errors.rejectValue("dni", "val.formato_dni");
            }
        }

        //Fono
        if (Validador.noNuloNoVacio(usuarioDTO.getFono())) {
            if (!Validador.contieneSoloNumeros(usuarioDTO.getFono())) {
                errors.rejectValue("fono", "val.solo_numeros");
            }
        }

        //Avatar
        if (!Validador.esArchivoVacio(usuarioDTO.getAvatar().getSize())) {
            if (!usuarioDTO.getAvatar().getOriginalFilename().endsWith(Constants.FORMATO_PNG)
                    && !usuarioDTO.getAvatar().getOriginalFilename().endsWith(Constants.FORMATO_JPG)) {
                errors.rejectValue("avatar", "val.formato_img");
            }
        }

        //Firma
        if (!Validador.esArchivoVacio(usuarioDTO.getFirma().getSize())) {
            if (!usuarioDTO.getFirma().getOriginalFilename().endsWith(Constants.FORMATO_PNG)
                    && !usuarioDTO.getFirma().getOriginalFilename().endsWith(Constants.FORMATO_JPG)) {
                errors.rejectValue("firma", "val.formato_img");
            }
        }

        /*
         //Documento
         if (!Validador.esMayorCero(paciente.getNomDocumento())) {
         errors.rejectValue("nomDocumento", "val.paciente.nomDocumento_registro");
         errors.rejectValue("numDocumento", "val.paciente.numDocumento_registro");
         }

         //**********numDocumento-depende del tipo de documento
         switch (paciente.getNomDocumento()) {
         case 1:
         if (!Validador.esDNI(paciente.getNumDocumento())) {
         errors.rejectValue("numDocumento", "val.paciente.numDocumento_registro");
         }
         break;
         case 2:
         if (!Validador.esLibretaMilitar(paciente.getNumDocumento())) {
         errors.rejectValue("numDocumento", "val.paciente.numDocumento_registro");
         }
         break;
         case 3:
         if (!Validador.esCarnetExtranjeria(paciente.getNumDocumento())) {
         errors.rejectValue("numDocumento", "val.paciente.numDocumento_registro");
         }
         break;
         case 4:
         if (!Validador.esPasaporte(paciente.getNumDocumento())) {
         errors.rejectValue("numDocumento", "val.paciente.numDocumento_registro");
         }
         break;
         }

         //Apellidos y Nombre
         if (!Validador.noNuloNoVacio(paciente.getApePaterno())) {
         errors.rejectValue("apePaterno", "val.paciente.apellidos_registro");
         }

         if (!Validador.noNuloNoVacio(paciente.getNombre())) {
         errors.rejectValue("nombre", "val.paciente.nombre_registro");
         }

         //Sexo
         if (!Validador.esMayorCero(paciente.getSexo())) {
         errors.rejectValue("sexo", "val.paciente.sexo_registro");
         }
         //Fecha de Nacimiento
         if (!Validador.noNuloNoVacio(paciente.getFecNacimiento())) {
         errors.rejectValue("fecNacimiento", "val.paciente.fecNacimiento_registro");
         } else if (!Validador.esFechaAnteriorAHoy(paciente.getFecNacimiento(), "dd/MM/yyyy")) {
         errors.rejectValue("fecNacimiento", "val.paciente.fecNacimiento_registro");
         }

         //Correo
         if (!Validador.noNuloNoVacio(paciente.getCorreo())) {
         errors.rejectValue("correo", "val.paciente.correo_registro");
         } else if (!Validador.esEmail(paciente.getCorreo())) {
         errors.rejectValue("correo", "val.paciente.correo_registro");
         }
         //Direccion
         if (!Validador.noNuloNoVacio(paciente.getDireccion())) {
         errors.rejectValue("direccion", "val.paciente.direccion_registro");
         }
         //Ubigeo
         if (!Validador.esMayorCero(Integer.parseInt(paciente.getCodDep()))) {
         errors.rejectValue("codDep", "val.paciente.departamento_registro");
         }
         if (!Validador.esMayorCero(Integer.parseInt(paciente.getCodProv()))) {
         errors.rejectValue("codProv", "val.paciente.provincia_registro");
         }
         if (!Validador.esMayorCero(Integer.parseInt(paciente.getCodDist()))) {
         errors.rejectValue("codDist", "val.paciente.distrito_registro");
         }
         //Telefono
         if (!Validador.contieneSoloNumeros(paciente.getTelefono())) {
         errors.rejectValue("telefono", "val.paciente.telefono_registro");
         }
         //Usuario
         if (!Validador.noNuloNoVacio(paciente.getUsuario())) {
         errors.rejectValue("usuario", "val.paciente.usuario_registro");
         } else if (pacienteDAO.existeUsuario(paciente.getUsuario())) {
         errors.rejectValue("usuario", "val.paciente.usuario_registro_existe");
         }
         //Foto
         if (!Validador.esArchivoVacio(paciente.getFoto().getSize())) {
         if ((!paciente.getFoto().getOriginalFilename().endsWith(Constants.FORMATO_JPEG))
         && (!paciente.getFoto().getOriginalFilename().endsWith(Constants.FORMATO_JPG))
         && (!paciente.getFoto().getOriginalFilename().endsWith(Constants.FORMATO_PNG))) {
         errors.rejectValue("foto", "val.paciente.foto_registro");
         }

         }
         //Archivo Resumen Historia Clinica
         if (!Validador.esArchivoVacio(paciente.getArchClinico().getSize())) {
         if (!paciente.getArchClinico().getOriginalFilename().endsWith(Constants.EXT_PDF)) {
         errors.rejectValue("archClinico", "val.paciente.archivo_registro_ext");
         }
         }


         */

    }
}
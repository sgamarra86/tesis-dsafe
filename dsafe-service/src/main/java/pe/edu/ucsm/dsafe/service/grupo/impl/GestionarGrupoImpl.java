/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.grupo.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.ucsm.dsafe.dataaccess.domain.GrupoDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.GrupoVO;
import pe.edu.ucsm.dsafe.dataaccess.model.UsuarioVO;
import pe.edu.ucsm.dsafe.service.grupo.GestionarGrupo;
import pe.edu.ucsm.dsafe.service.grupo.dto.GrupoDTO;
import pe.edu.ucsm.dsafe.service.grupo.dto.RegistrarGrupoDTO;
import pe.edu.ucsm.dsafe.service.grupo.dto.VerGrupoDTO;

/**
 *
 * @author Sergio Gamarra
 */
@Service("gestionarGrupo")
public class GestionarGrupoImpl implements GestionarGrupo {

    //@Autowired
    private GrupoDAO grupoDAO;

    @Override
    public int registrarGrupo(RegistrarGrupoDTO grupoDTO) {
        GrupoVO grupoVO = new GrupoVO();
        grupoVO.setNombre(grupoDTO.getNombre());
        grupoVO.setUsuarioVO(new UsuarioVO(grupoDTO.getIdUsuario()));
        return grupoDAO.insert(grupoVO);
    }

    @Override
    public List obtenerGruposPorUsuario(Integer id) {
        List<GrupoDTO> lstGrupoDTO = new ArrayList<GrupoDTO>();
        List<GrupoVO> lstGrupoVO = grupoDAO.obtenerGruposPorUsuario(id);
        for (GrupoVO vo : lstGrupoVO) {
            lstGrupoDTO.add(new GrupoDTO(vo.getId(), vo.getNombre(), 0));   //Debe ir la cantidad
        }
        return lstGrupoDTO;
    }

    @Override
    public VerGrupoDTO obtenerGrupoPorId(Integer id) {
        VerGrupoDTO grupoDTO = new VerGrupoDTO();

        return grupoDTO;
    }
}
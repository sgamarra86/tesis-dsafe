/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.documento.dto;

/**
 *
 * @author Sergio Gamarra
 */
public class FirmanteDTO {
    private String nombre;
    private String email;
    private String fecFirma;
    private String organizacion;
    private String fecExpedicion;
    private String fecExpiracion;

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the fecFirma
     */
    public String getFecFirma() {
        return fecFirma;
    }

    /**
     * @param fecFirma the fecFirma to set
     */
    public void setFecFirma(String fecFirma) {
        this.fecFirma = fecFirma;
    }

    /**
     * @return the organizacion
     */
    public String getOrganizacion() {
        return organizacion;
    }

    /**
     * @param organizacion the organizacion to set
     */
    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }

    /**
     * @return the fecExpedicion
     */
    public String getFecExpedicion() {
        return fecExpedicion;
    }

    /**
     * @param fecExpedicion the fecExpedicion to set
     */
    public void setFecExpedicion(String fecExpedicion) {
        this.fecExpedicion = fecExpedicion;
    }

    /**
     * @return the fecExpiracion
     */
    public String getFecExpiracion() {
        return fecExpiracion;
    }

    /**
     * @param fecExpiracion the fecExpiracion to set
     */
    public void setFecExpiracion(String fecExpiracion) {
        this.fecExpiracion = fecExpiracion;
    }
}

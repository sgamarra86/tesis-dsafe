/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.grupo;

import java.util.List;
import pe.edu.ucsm.dsafe.service.grupo.dto.RegistrarGrupoDTO;
import pe.edu.ucsm.dsafe.service.grupo.dto.VerGrupoDTO;

/**
 *
 * @author Sergio Gamarra
 */
public interface GestionarGrupo {

    
    public VerGrupoDTO obtenerGrupoPorId(Integer id);
    
    /**
     * Registra un nuevo grupo.
     *
     * @param grupoDTO
     * @return id del grupo creado.
     */
    public int registrarGrupo(RegistrarGrupoDTO grupoDTO);

    /**
     * Obtiene los grupos de un usuario.
     *
     * @param id
     * @return grupos del usuario
     */
    public List obtenerGruposPorUsuario(Integer id);
}

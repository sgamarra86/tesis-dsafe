package pe.edu.ucsm.dsafe.service.autenticacion;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.stereotype.Service;
import pe.edu.ucsm.dsafe.dataaccess.domain.UsuarioDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.UsuarioVO;
import pe.edu.ucsm.dsafe.service.autenticacion.dto.UserDetailsDTO;
import pe.edu.ucsm.dsafe.service.contacto.dto.ContactoDTO;

@Service("webAuthenticationProvider")
public class WebAuthenticationProvider implements AuthenticationProvider {

    //private static Logger logger = Logger.getLogger(AdminAuthenticationProviderPropio.class);
    @Autowired
    private UsuarioDAO usuarioDAO;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        //Obtener USERDETAILS por authentication
        UserDetailsDTO userDetailsDTO = obtenerDatosCliente(
                (String) authentication.getPrincipal(),
                (String) authentication.getCredentials());

        //Crear ACCESOS de usuario
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (String acceso : userDetailsDTO.getLstAccesos()) {
            authorities.add(new GrantedAuthorityImpl(acceso));
        }

        //Crear TOKEN
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(authentication.getPrincipal(), authentication.getCredentials(), authorities);
        token.setDetails(userDetailsDTO);
        return token;
    }

    private UserDetailsDTO obtenerDatosCliente(String nomUsuario, String clvUsuario) {
        UserDetailsDTO userDetailsDTO = new UserDetailsDTO();

        if (!nomUsuario.trim().isEmpty() && !clvUsuario.trim().isEmpty()) {
            UsuarioVO usuario = this.usuarioDAO.obtenerUsuarioPorEmail(nomUsuario);
            if (usuario != null) {
                //if (usuario.getClvUsuario().equals(EncriptacionUtil.encriptar(clvUsuario))) {
                if (usuario.getPassword().equals(clvUsuario)) {
                    userDetailsDTO.setId(usuario.getId());
                    userDetailsDTO.setEmail(usuario.getEmail());
                    userDetailsDTO.setNombres(usuario.getNombres());
                    userDetailsDTO.setApellidos(usuario.getApellidos());
                    userDetailsDTO.setDni(usuario.getDni());
                    userDetailsDTO.setFono(usuario.getFono());
                    userDetailsDTO.setEstado(usuario.getEstado());
                    //FIX: prueba de bytes en sesion?
                    userDetailsDTO.setAvatar(usuario.getAvatar());
                    userDetailsDTO.setFirma(usuario.getFirma());

                    List<ContactoDTO> contactos = new ArrayList<ContactoDTO>();
                    for (UsuarioVO vo : new ArrayList<UsuarioVO>(usuario.getContactos())) {
                        ContactoDTO dto = new ContactoDTO();
                        dto.setId(vo.getId());
                        dto.setEmail(vo.getEmail());
                        dto.setNombres(vo.getNombres());
                        dto.setApellidos(vo.getApellidos());
                        dto.setEstado(vo.getEstado());
                        contactos.add(dto);
                    }
                    userDetailsDTO.setContactos(contactos);

                    //TODO: Asignar accesos, aun no es necesario.
                    List<String> accesos = new ArrayList<String>();
                    accesos.add("USER");
//                            for (AccesoVO acceso : new ArrayList<AccesoVO>(usuario.getAccesos())) {
//                                accesos.add(acceso.getNomAcceso());
//                            }
                    userDetailsDTO.setLstAccesos(accesos);
                } else {
                    throw new BadCredentialsException("La contraseña ingresada no es correcta");
                }
            } else {
                throw new AuthenticationServiceException("El usuario ingresado no existe en el sistema.");
            }
        } else {
            throw new AuthenticationServiceException("Debe ingresar los datos requeridos.");
        }
        return userDetailsDTO;
    }

    @Override
    public boolean supports(Class<?> type) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(type));
    }
}

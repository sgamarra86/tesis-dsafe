/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.documento.dto;

import java.util.List;

/**
 *
 * @author Sergio Gamarra
 */
public class VerDocumentoDTO {
    private Integer id;
    private String nombre;
    private String descripcion;
    private String fecha;
    private Integer firmasRequeridas;
    private Integer firmasRealizadas;
    private List<FirmanteDTO> firmantes;

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the firmasRequeridas
     */
    public Integer getFirmasRequeridas() {
        return firmasRequeridas;
    }

    /**
     * @param firmasRequeridas the firmasRequeridas to set
     */
    public void setFirmasRequeridas(Integer firmasRequeridas) {
        this.firmasRequeridas = firmasRequeridas;
    }

    /**
     * @return the firmasRealizadas
     */
    public Integer getFirmasRealizadas() {
        return firmasRealizadas;
    }

    /**
     * @param firmasRealizadas the firmasRealizadas to set
     */
    public void setFirmasRealizadas(Integer firmasRealizadas) {
        this.firmasRealizadas = firmasRealizadas;
    }

    /**
     * @return the firmantes
     */
    public List<FirmanteDTO> getFirmantes() {
        return firmantes;
    }

    /**
     * @param firmantes the firmantes to set
     */
    public void setFirmantes(List<FirmanteDTO> firmantes) {
        this.firmantes = firmantes;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
}

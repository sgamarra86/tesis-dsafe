/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.documento.dto;

import java.io.Serializable;

/**
 *
 * @author Sergio Gamarra
 */
public class PieChartDTO implements Serializable {

    private String tipo;
    private Integer cantidad;

    public PieChartDTO(String tipo, Integer cantidad) {
        this.tipo = tipo;
        this.cantidad = cantidad;
    }
    
    

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the cantidad
     */
    public Integer getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }
}

package pe.edu.ucsm.dsafe.service.autenticacion;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.stereotype.Service;
import pe.edu.ucsm.dsafe.dataaccess.domain.UsuarioDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.UsuarioVO;


@Service("logoutSuccessHandler")
public class LogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {
 
    @Autowired
    private UsuarioDAO usuarioDAO;
    
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) 
            throws IOException, ServletException {
        
        UsuarioVO usuarioVO = this.usuarioDAO.obtenerUsuarioPorEmail(authentication.getName());
        usuarioVO.setEstado(Short.valueOf("0"));
        this.usuarioDAO.update(usuarioVO);
        
        HttpSession httpSession = request.getSession(false);
        if (httpSession != null) {
            httpSession.invalidate();
        }        
        response.sendRedirect("");
    }
}
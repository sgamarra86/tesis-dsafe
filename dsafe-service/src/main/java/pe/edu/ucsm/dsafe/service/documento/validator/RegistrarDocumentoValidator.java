/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.documento.validator;

import org.springframework.validation.Validator;
import org.springframework.validation.Errors;
import org.springframework.stereotype.Service;
import pe.edu.ucsm.dsafe.common.base.Validador;
import pe.edu.ucsm.dsafe.service.documento.dto.RegistrarDocumentoDTO;

/**
 *
 * @author Sergio Gamarra
 */
@Service("registrarDocumentoValidator")
public class RegistrarDocumentoValidator implements Validator {

    @SuppressWarnings("unchecked")
    @Override
    public boolean supports(Class<?> clazz) {
        return RegistrarDocumentoDTO.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        RegistrarDocumentoDTO documentoDTO = (RegistrarDocumentoDTO) target;

        //Documento
        if (Validador.esArchivoVacio(documentoDTO.getArchivo().getSize())) {
            errors.rejectValue("archivo", "val.campo_vacio");
        }

        //Descripcion
        if (!Validador.noNuloNoVacio(documentoDTO.getDescripcion())) {
            errors.rejectValue("descripcion", "val.campo_vacio");
        }

        //IdFirmantes
        if (!Validador.noNuloNoVacio(documentoDTO.getIdFirmantes())) {
            System.out.println("ERROR ID FIRMANTES VACIO:"+documentoDTO.getIdFirmantes());
            errors.rejectValue("idFirmantes", "val.campo_vacio");
        }

        /* NO REQUERIDOS
        //Mail firmantes
        if (!Validador.noNuloNoVacio(documentoDTO.getMailFirmantes())) {
            errors.rejectValue("mailFirmantes", "val.campo_vacio");
        }

        //IdObservadores
        if (Validador.noNuloNoVacio(documentoDTO.getIdObservadores())) {
            if (!Validador.noNuloNoVacio(documentoDTO.getMailObservadores())) {
                errors.rejectValue("mailObservadores", "val.campo_vacio");
            }
        }
        */

    }
}
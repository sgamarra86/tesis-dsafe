/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.documento.impl;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.ucsm.dsafe.common.base.Constants;
import pe.edu.ucsm.dsafe.common.base.DSafeFecha;
import pe.edu.ucsm.dsafe.common.base.Mail;
import pe.edu.ucsm.dsafe.dataaccess.domain.DocumentoDAO;
import pe.edu.ucsm.dsafe.dataaccess.domain.FirmaDAO;
import pe.edu.ucsm.dsafe.dataaccess.domain.NotificacionDAO;
import pe.edu.ucsm.dsafe.dataaccess.domain.ObservadoDAO;
import pe.edu.ucsm.dsafe.dataaccess.domain.UsuarioDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.DocumentoVO;
import pe.edu.ucsm.dsafe.dataaccess.model.FirmaVO;
import pe.edu.ucsm.dsafe.dataaccess.model.ObservadoVO;
import pe.edu.ucsm.dsafe.dataaccess.model.UsuarioVO;
import pe.edu.ucsm.dsafe.service.documento.GestionarDocumento;
import pe.edu.ucsm.dsafe.service.documento.dto.DatosFirmaDTO;
import pe.edu.ucsm.dsafe.service.documento.dto.DatosVerificarDocumentoDTO;
import pe.edu.ucsm.dsafe.service.documento.dto.ListarDocumentoDTO;
import pe.edu.ucsm.dsafe.service.documento.dto.RegistrarDocumentoDTO;
import pe.edu.ucsm.dsafe.sign.DSafeFirma;
import pe.edu.ucsm.dsafe.sign.data.FirmanteDTO;
import pe.edu.ucsm.dsafe.sign.data.PrepararFirmaPdfDTO;
import pe.edu.ucsm.dsafe.sign.data.ResultadoPrepararPdfDTO;
import pe.edu.ucsm.dsafe.sign.data.ResultadoVerificacionDTO;
import pe.edu.ucsm.dsafe.sign.data.VerificarDTO;

/**
 *
 * @author Sergio Gamarra
 */
@Service("gestionarDocumento")
public class GestionarDocumentoImpl implements GestionarDocumento {

    @Autowired
    private DocumentoDAO documentoDAO;
    @Autowired
    private FirmaDAO firmaDAO;
    @Autowired
    private ObservadoDAO observadoDAO;
    @Autowired
    private UsuarioDAO usuarioDAO;
    @Autowired
    private NotificacionDAO notificacionDAO;

    @Override
    public DocumentoVO obtenerDocumentoPorIdDocumento(Integer idDocumento) {
        return this.documentoDAO.obtenerDocumentoPorIdDocumento(idDocumento);
    }

    @Override
    public List<ListarDocumentoDTO> obtenerDocumentosPorIdUsuario(Integer idUsuario) {
        List<ListarDocumentoDTO> documentosDTO = new ArrayList<ListarDocumentoDTO>();
        List<DocumentoVO> documentosVO = this.documentoDAO.obtenerDocumentosPorIdUsuario(idUsuario);
        ListarDocumentoDTO dto;
        for (DocumentoVO vo : documentosVO) {
            List<FirmaVO> firmas = this.firmaDAO.obtenerFirmasPorIdDocumento(vo.getId());
            List<ObservadoVO> observados = this.observadoDAO.obtenerObservadosPorIdDocumento(vo.getId());
            dto = new ListarDocumentoDTO();
            dto.setId(vo.getId());
            dto.setNombre(vo.getNombre());
            dto.setFecha(DSafeFecha.standard_hora(vo.getFecha()));
            dto.setFirmasNecesarias(firmas.size());
            dto.setDestinatarios(observados.size());
            dto.setReportado(this.notificacionDAO.esDocumentoReportado(vo.getId()));
            documentosDTO.add(dto);
        }
        return documentosDTO;
    }

    @Override
    public Integer registrarDocumento(RegistrarDocumentoDTO documentoDTO) {
        DocumentoVO vo = new DocumentoVO();
        vo.setNombre(documentoDTO.getArchivo().getOriginalFilename());
        vo.setExtension(documentoDTO.getArchivo().getContentType());
        vo.setDescripcion(documentoDTO.getDescripcion());
        vo.setArchivo(documentoDTO.getArchivo().getBytes());
        vo.setFecha(Calendar.getInstance());
        vo.setMensaje_firma(documentoDTO.getMailFirmantes());
        vo.setPropietario(new UsuarioVO(documentoDTO.getPropietario()));
        return this.documentoDAO.insert(vo);
    }

    @Override
    public Integer registrarDocumentoPdf(RegistrarDocumentoDTO documentoDTO, byte[] archivoPdf) {
        DocumentoVO vo = new DocumentoVO();
        vo.setNombre(documentoDTO.getArchivo().getOriginalFilename());
        vo.setExtension(documentoDTO.getArchivo().getContentType());
        vo.setDescripcion(documentoDTO.getDescripcion());
        vo.setArchivo(archivoPdf);
        vo.setArchivoPdf(archivoPdf);
        vo.setFecha(Calendar.getInstance());
        vo.setMensaje_firma(documentoDTO.getMailFirmantes());
        vo.setPropietario(new UsuarioVO(documentoDTO.getPropietario()));
        return this.documentoDAO.insert(vo);
    }

    @Override
    public DatosVerificarDocumentoDTO verificarDocumentoPorIdDocumento(Integer idDocumento, Integer idUsuario, String path) {
        DatosVerificarDocumentoDTO documento = new DatosVerificarDocumentoDTO();
        DocumentoVO vo = this.documentoDAO.obtenerDocumentoPorIdDocumento(idDocumento);

        documento.setId(idDocumento);
        documento.setNombre(vo.getNombre());
        documento.setDescripcion(vo.getDescripcion());
        documento.setFirmaRequerida(false);
        documento.setFirmasRequeridas(vo.getFirmas().size());
        documento.setFirmasRealizadas(this.calcularCantidadFirmasRealizadas(vo.getFirmas()));
        documento.setFechaRegistro(DSafeFecha.standard(vo.getFecha()));
        documento.setReportado(this.notificacionDAO.esDocumentoReportado(idDocumento));

        DatosFirmaDTO firma;
        List<DatosFirmaDTO> firmas = new ArrayList<DatosFirmaDTO>();
        for (FirmaVO fVO : vo.getFirmas()) {
            if (fVO.getEstado() == Constants.FIRMA_REALIZADA) {
                ResultadoVerificacionDTO rvDTO = this.verificarFirma(vo.getArchivo(), fVO.getArchivo());
                firma = new DatosFirmaDTO();
                firma.setUsuario(fVO.getUsuario().getNombres() + " " + fVO.getUsuario().getApellidos());
                firma.setEmail(fVO.getUsuario().getEmail());
                firma.setUrlAvatar(this.crearImagenEnTemporales(path, fVO.getUsuario().getEmail() + "_avatar", fVO.getUsuario().getAvatar()));
                firma.setUrlFirma(this.crearImagenEnTemporales(path, fVO.getUsuario().getEmail() + "_firma", fVO.getUsuario().getFirma()));
                firma.setFechaFirma(DSafeFecha.standard_hora(fVO.getFecha()));
                firma.setCorrecto(rvDTO.isCorrecto());
                firma.setMensaje(rvDTO.getMensaje());
                if (rvDTO.isCorrecto()) {
                    firma.setUrlCertificado(this.crearCertificadoEnTemporales(path, fVO.getUsuario().getEmail() + "_certificado", rvDTO.getCertificado()));
                    firma.setPropietario(rvDTO.getPropietario());
                    firma.setAutorizador(rvDTO.getEmitidoPor());
                    firma.setFechaExpedicion(DSafeFecha.standard(rvDTO.getFechaVigencia()));
                    firma.setFechaExpiracion(DSafeFecha.standard(rvDTO.getFechaExpiracion()));

                }
                firmas.add(firma);
            } else {
                if (fVO.getUsuario().getId().equals(idUsuario)) {
                    documento.setFirmaRequerida(true);
                }
            }
        }

        documento.setFirmas(firmas);
        return documento;
    }

    private Integer calcularCantidadFirmasRealizadas(Set<FirmaVO> firmas) {
        Integer cantidad = 0;
        for (FirmaVO vo : firmas) {
            if (vo.getEstado() == (short) 1) {
                cantidad++;
            }
        }
        return cantidad;
    }

    private String crearImagenEnTemporales(String path, String filename, byte[] imagen) {
        String url = "img/" + filename + ".jpg";
        FileOutputStream out;
        try {
            out = new FileOutputStream(path + "/" + url);
            out.write(imagen);
            out.flush();
            out.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return url;
    }

    private String crearCertificadoEnTemporales(String path, String filename, byte[] archivo) {
        String url = "crt/" + filename + ".cer";
        FileOutputStream out;
        try {
            out = new FileOutputStream(path + "/" + url);
            out.write(archivo);
            out.flush();
            out.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return url;
    }

    private ResultadoVerificacionDTO verificarFirma(byte[] archOriginal, byte[] archFirmado) {
        VerificarDTO verifyDTO = new VerificarDTO();
        verifyDTO.setArchivo(archOriginal);
        verifyDTO.setArchivoFirmado(archFirmado);
        ResultadoVerificacionDTO rvDTO = DSafeFirma.verificar(verifyDTO);
        return rvDTO;
    }

    @Override
    public Integer[] obtenerIdDocumentosPorIdUsuario(Integer idUsuario) {
        List<Integer> idDocumentos = this.firmaDAO.obtenerIdDocumentosDeFirmasPorIdUsuario(idUsuario);
        List<Integer> idAxuliar = this.observadoDAO.obtenerIdDocumentosDeObservadosPorIdUsuario(idUsuario);

        for (Integer id : idAxuliar) {
            idDocumentos.add(id);
        }

        Integer[] ids = new Integer[idDocumentos.size()];
        int i = 0;
        for (Integer id : idDocumentos) {
            ids[i++] = id;
        }

        return ids;
    }

    @Override
    public void modificarDocumento(DocumentoVO vo) {
        this.documentoDAO.update(vo);
    }

    @Override
    public ResultadoPrepararPdfDTO prepararDocumentoPdf(Integer[] idUsuarios, byte[] archivoPdf) {
        List<UsuarioVO> usuarios = this.usuarioDAO.obtenerUsuariosPorIds(idUsuarios);
        List<FirmanteDTO> firmantes = new ArrayList<FirmanteDTO>();
        FirmanteDTO firmanteDTO;
        for (UsuarioVO usuarioVO : usuarios) {
            firmanteDTO = new FirmanteDTO();
            firmanteDTO.setEmail(usuarioVO.getEmail());
            firmanteDTO.setNombres(usuarioVO.getNombres());
            firmanteDTO.setApellidos(usuarioVO.getApellidos());
            firmantes.add(firmanteDTO);
        }
        PrepararFirmaPdfDTO prepararFirmaPdfDTO = new PrepararFirmaPdfDTO();
        prepararFirmaPdfDTO.setFirmantes(firmantes);
        prepararFirmaPdfDTO.setDocumento(archivoPdf);

        return DSafeFirma.prepararPDF(prepararFirmaPdfDTO);
    }

    @Override
    public Integer eliminarDocumentoPorIdDocumento(Integer idDocumento) {
        List<FirmaVO> firmas = this.firmaDAO.obtenerFirmasPorIdDocumento(idDocumento);
        for (FirmaVO firmaVO : firmas) {
            this.firmaDAO.deleteById(firmaVO.getId());
        }
        
        List<ObservadoVO> observadores = this.observadoDAO.obtenerObservadosPorIdDocumento(idDocumento);
        for (ObservadoVO observadoVO : observadores) {
            this.observadoDAO.deleteById(observadoVO.getId());
        }
        
        return this.documentoDAO.deleteById(idDocumento);
    }
}
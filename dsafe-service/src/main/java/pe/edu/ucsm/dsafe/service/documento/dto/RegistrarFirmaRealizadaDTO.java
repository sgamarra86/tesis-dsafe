/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.documento.dto;

/**
 *
 * @author Sergio Gamarra
 */
public class RegistrarFirmaRealizadaDTO {
    private Integer idUsuario;
    private Integer idDocumento;
    private byte[] archivo;

    public RegistrarFirmaRealizadaDTO(Integer idUsuario, Integer idDocumento, byte[] archivo) {
        this.idUsuario = idUsuario;
        this.idDocumento = idDocumento;
        this.archivo = archivo;
    }

    /**
     * @return the idUsuario
     */
    public Integer getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the idDocumento
     */
    public Integer getIdDocumento() {
        return idDocumento;
    }

    /**
     * @param idDocumento the idDocumento to set
     */
    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    /**
     * @return the archivo
     */
    public byte[] getArchivo() {
        return archivo;
    }

    /**
     * @param archivo the archivo to set
     */
    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }
}

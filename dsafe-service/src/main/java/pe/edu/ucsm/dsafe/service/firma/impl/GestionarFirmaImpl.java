/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.firma.impl;

import java.io.InputStream;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.ucsm.dsafe.common.base.Constants;
import pe.edu.ucsm.dsafe.dataaccess.domain.FirmaDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.DocumentoVO;
import pe.edu.ucsm.dsafe.dataaccess.model.FirmaVO;
import pe.edu.ucsm.dsafe.dataaccess.model.UsuarioVO;
import pe.edu.ucsm.dsafe.service.documento.dto.RegistrarFirmaRealizadaDTO;
import pe.edu.ucsm.dsafe.service.firma.GestionarFirma;
import java.util.ArrayList;
import java.util.Calendar;
import javax.servlet.ServletContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import pe.edu.ucsm.dsafe.dataaccess.domain.DocumentoDAO;
import pe.edu.ucsm.dsafe.service.autenticacion.dto.UserDetailsDTO;
import pe.edu.ucsm.dsafe.service.documento.dto.ListarPendienteDTO;
import pe.edu.ucsm.dsafe.service.firma.dto.FirmarDocumentoRequerimientoDTO;
import pe.edu.ucsm.dsafe.service.firma.dto.FirmarDocumentoResultadoDTO;
import pe.edu.ucsm.dsafe.sign.DSafeFirma;
import pe.edu.ucsm.dsafe.sign.data.FirmaDTO;
import pe.edu.ucsm.dsafe.sign.data.FirmaPdfDTO;
import pe.edu.ucsm.dsafe.sign.data.ResultadoFirmaDTO;
import pe.edu.ucsm.dsafe.sign.data.ResultadoFirmaPdfDTO;
import sun.misc.IOUtils;

/**
 *
 * @author Sergio Gamarra
 */
@Service("gestionarFirma")
public class GestionarFirmaImpl implements GestionarFirma {

    @Autowired
    private FirmaDAO firmaDAO;
    @Autowired
    private DocumentoDAO documentoDAO;

    @Override
    public List obtenerFirmasPendientesPorIdUsuario(Integer IdUsuario) {
        return this.firmaDAO.obtenerFirmasPendientesPorIdUsuario(IdUsuario);
    }

    @Override
    public List obtenerFirmasPendientesPorIdUsuarioResumido(Integer IdUsuario) {
        List<ListarPendienteDTO> pendientes = new ArrayList<ListarPendienteDTO>();
        List<FirmaVO> firmas = this.firmaDAO.obtenerFirmasPendientesPorIdUsuarioResumido(IdUsuario);
        ListarPendienteDTO dto;
        for (FirmaVO vo : firmas) {
            dto = new ListarPendienteDTO();
            dto.setId(vo.getDocumento().getId());
            dto.setNombre(vo.getDocumento().getNombre());
            dto.setMensaje(vo.getDocumento().getMensaje_firma());
            pendientes.add(dto);
        }
        return pendientes;
    }

    @Override
    public List obtenerFirmasRealizadasPorIdUsuario(Integer IdUsuario) {
        return this.firmaDAO.obtenerFirmasRealizadasPorIdUsuario(IdUsuario);
    }

    @Override
    public void registrarFirmasRequeridas(Integer[] idUsuarios, Integer idDocumento) {
        FirmaVO vo;
        for (Integer idUsuario : idUsuarios) {
            vo = new FirmaVO();
            vo.setUsuario(new UsuarioVO(idUsuario));
            vo.setDocumento(new DocumentoVO(idDocumento));
            vo.setEstado(Constants.FIRMA_REQUERIDA);
            this.firmaDAO.insert(vo);
        }
    }

    @Override
    public void registrarFirmaRealizada(RegistrarFirmaRealizadaDTO dto) {
        FirmaVO vo = this.firmaDAO.obtenerFirmaPorIdUsuarioYIdDocumento(dto.getIdUsuario(), dto.getIdDocumento());
        vo.setFecha(Calendar.getInstance());
        vo.setEstado(Constants.FIRMA_REALIZADA);
        vo.setArchivo(dto.getArchivo());
        this.firmaDAO.saveOrUpdate(vo);
    }

    @Override
    public FirmarDocumentoResultadoDTO firmarDocumento(FirmarDocumentoRequerimientoDTO requerimiento) {
        FirmarDocumentoResultadoDTO resultadoFinal;
        UserDetailsDTO usuario = this.getDatosUsuario();
        DocumentoVO documento = this.documentoDAO.obtenerDocumentoPorIdDocumento(requerimiento.getIdDocumento());

        //Firma de documento convencional
        FirmaDTO firmaDTO = new FirmaDTO();
        firmaDTO.setDocumento(documento.getArchivo());
        firmaDTO.setPkcs12(requerimiento.getPkcs12());
        firmaDTO.setPkcs12Password(requerimiento.getPkcs12Password());
        firmaDTO.setCertificadoRaiz(requerimiento.getCertificadoRaiz());

        ResultadoFirmaDTO resultadoFirma = DSafeFirma.firmar(firmaDTO);
        resultadoFinal = this.cargarResultado(documento.getNombre(), resultadoFirma);

        if (resultadoFirma.isCorrecto()) {
            if (documento.getExtension().equals(Constants.FORMATO_PDF)) {
                //Firma de documento pdf
                FirmaPdfDTO firmaPdfDTO = new FirmaPdfDTO();
                firmaPdfDTO.setDocumento(documento.getArchivoPdf());
                firmaPdfDTO.setEmail(usuario.getEmail());
                firmaPdfDTO.setImagenFirma(usuario.getFirma());
                firmaPdfDTO.setPkcs12(requerimiento.getPkcs12());
                firmaPdfDTO.setPkcs12Password(requerimiento.getPkcs12Password());
                ResultadoFirmaPdfDTO resultadoFirmaPdf = DSafeFirma.firmarPDF(firmaPdfDTO);
                if (resultadoFirmaPdf.isCorrecto()) {
                    //Actualizar documento firmado
                    //documento.setArchivo(resultadoFirmaPdf.getPdfFirmado());
                    documento.setArchivoPdf(resultadoFirmaPdf.getPdfFirmado());
                    this.documentoDAO.update(documento);
                } else {
                    resultadoFinal.setCorrecto(false);
                    resultadoFinal.setMensaje(resultadoFirmaPdf.getMensaje());
                }
            }
        }

        return resultadoFinal;
    }

    private UserDetailsDTO getDatosUsuario() {
        UserDetailsDTO userDetailDTO;
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            userDetailDTO = (UserDetailsDTO) authentication.getDetails();
        } catch (Exception ex) {
            userDetailDTO = null;
        }
        return userDetailDTO;
    }

    private FirmarDocumentoResultadoDTO cargarResultado(String documento, ResultadoFirmaDTO rf) {
        FirmarDocumentoResultadoDTO r = new FirmarDocumentoResultadoDTO();
        r.setCorrecto(rf.isCorrecto());
        r.setFirma(rf.getFirma());
        r.setMensaje(rf.getMensaje());
        r.setDocumento(documento);
        return r;
    }

    @Override
    public List obtenerFirmasRequeridasPorIdDocumento(Integer idDocumento) {
        return this.firmaDAO.obtenerFirmasRequeridasPorIdDocumento(idDocumento);
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.contacto;

import java.util.List;
import pe.edu.ucsm.dsafe.service.contacto.dto.ContactoDTO;

/**
 *
 * @author Sergio Gamarra
 */
public interface GestionarContacto {

    /**
     * Obtener contactos de un usuario.
     *
     * @param email
     * @return contactos
     */
    public List<ContactoDTO> obtenerContactosPorEmail(String email);

    /**
     * Obtener contactos que solicitan amistad.
     *
     * @param parametro
     * @return
     */
    public List<ContactoDTO> obtenerSolicitudesRecibidasPorEmail(String email);

    /**
     * Obtener contactos que cumplan con el parametro.
     *
     * @param parametro
     * @return Lista de contactos
     */
    public List<ContactoDTO> obtenerContactosPorParametro(String parametro);

    /**
     * Eliminar contacto segun parámetros.
     *
     * @param idUsuario
     * @param idContacto
     */
    public void eliminarContacto(Integer idUsuario, Integer idContacto);

    /**
     * Registrar solicitud de amistad hacia un usuario
     *
     * @param idSolicitante
     * @param idSolicitado
     */
    public void registrarSolicitud(Integer idSolicitante, Integer idSolicitado);

    /**
     * Aceptar solicitud de amistad de un usuario.
     *
     * @param idSolicitado
     * @param idSolicitante
     */
    public void aceptarSolicitud(Integer idSolicitado, Integer idSolicitante);

    /**
     * Rechazar solicitud de amistad de un usuario.
     *
     * @param idSolicitado
     * @param idSolicitante
     */
    public void rechazarSolicitud(Integer idSolicitado, Integer idSolicitante);    
}

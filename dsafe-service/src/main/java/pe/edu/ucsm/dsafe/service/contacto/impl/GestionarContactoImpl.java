/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.contacto.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.ucsm.dsafe.dataaccess.domain.UsuarioDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.UsuarioVO;
import pe.edu.ucsm.dsafe.service.contacto.GestionarContacto;
import pe.edu.ucsm.dsafe.service.contacto.dto.ContactoDTO;

/**
 *
 * @author Sergio Gamarra
 */
@Service("gestionarContacto")
public class GestionarContactoImpl implements GestionarContacto {

    @Autowired
    private UsuarioDAO usuarioDAO;

    @Override
    public List<ContactoDTO> obtenerContactosPorEmail(String email) {
        UsuarioVO usuario = usuarioDAO.obtenerUsuarioPorEmail(email);
        List<ContactoDTO> contactos = new ArrayList<ContactoDTO>();
        for (UsuarioVO vo : new ArrayList<UsuarioVO>(usuario.getContactos())) {
            ContactoDTO dto = new ContactoDTO();
            dto.setId(vo.getId());
            dto.setEmail(vo.getEmail());
            dto.setDni(vo.getDni());
            dto.setNombres(vo.getNombres());
            dto.setApellidos(vo.getApellidos());
            dto.setEstado(vo.getEstado());
            contactos.add(dto);
        }
        return contactos;
    }

    //TODO: esto se repite cada vez que se carga la pagina!
    @Override
    public List<ContactoDTO> obtenerSolicitudesRecibidasPorEmail(String email) {
        UsuarioVO usuario = usuarioDAO.obtenerUsuarioPorEmail(email);
        List<ContactoDTO> contactos = new ArrayList<ContactoDTO>();
        for (UsuarioVO vo : new ArrayList<UsuarioVO>(usuario.getRecibidas())) {
            ContactoDTO dto = new ContactoDTO();
            dto.setId(vo.getId());
            dto.setEmail(vo.getEmail());
            dto.setNombres(vo.getNombres());
            dto.setApellidos(vo.getApellidos());
            dto.setEstado(vo.getEstado());
            contactos.add(dto);
        }
        return contactos;
    }

    @Override
    public List<ContactoDTO> obtenerContactosPorParametro(String parametro) {
        List<UsuarioVO> usuarios = usuarioDAO.obtenerUsuariosPorParametro(parametro);
        List<ContactoDTO> contactos = new ArrayList<ContactoDTO>();
        for (UsuarioVO vo : usuarios) {
            ContactoDTO dto = new ContactoDTO();
            dto.setId(vo.getId());
            dto.setEmail(vo.getEmail());
            dto.setNombres(vo.getNombres());
            dto.setApellidos(vo.getApellidos());
            dto.setEstado(vo.getEstado());
            contactos.add(dto);
        }
        return contactos;
    }

    @Override
    public void eliminarContacto(Integer idUsuario, Integer idContacto) {       
        UsuarioVO usuario = this.usuarioDAO.get(idUsuario);
        UsuarioVO contacto = this.usuarioDAO.get(idContacto);
        
        usuario.setContactos(this.eliminarUsuarioDeSetUsuarios(usuario.getContactos(), idContacto));
        contacto.setContactos(this.eliminarUsuarioDeSetUsuarios(contacto.getContactos(), idUsuario));
        
        this.usuarioDAO.merge(usuario);
        this.usuarioDAO.merge(contacto);
    }

    @Override
    public void registrarSolicitud(Integer idSolicitante, Integer idSolicitado) {
        UsuarioVO solicitante = this.usuarioDAO.obtenerUsuarioPorIdConSolicitudes(idSolicitante);
        UsuarioVO solicitado = this.usuarioDAO.obtenerUsuarioPorIdConSolicitudes(idSolicitado);
        solicitante.getEnviadas().add(solicitado);
        solicitado.getRecibidas().add(solicitante);
        this.usuarioDAO.merge(solicitante);
        this.usuarioDAO.merge(solicitado);
    }

    @Override
    public void aceptarSolicitud(Integer idSolicitado, Integer idSolicitante) {
        //Esto debe de eliminar la solicitud y crear el nuevo amigo.
        UsuarioVO solicitante = this.usuarioDAO.obtenerUsuarioPorIdConSolicitudes(idSolicitante);
        UsuarioVO solicitado = this.usuarioDAO.obtenerUsuarioPorIdConSolicitudes(idSolicitado);
        

        //Agregar a contactos
        solicitante.getContactos().add(solicitado);
        solicitado.getContactos().add(solicitante);

        //Aca remover de las listas de recibidas y enviadas los registros.
        solicitante.setEnviadas(this.eliminarSolicitanteDeSet(solicitante.getEnviadas(), idSolicitado));
        solicitado.setRecibidas(this.eliminarSolicitanteDeSet(solicitado.getRecibidas(), idSolicitante));

        this.usuarioDAO.merge(solicitante);
        this.usuarioDAO.merge(solicitado);
    }

    @Override
    public void rechazarSolicitud(Integer idSolicitado, Integer idSolicitante) {
        //Esto debe de eliminar la solicitud y crear el nuevo amigo.
        UsuarioVO solicitante = this.usuarioDAO.obtenerUsuarioPorIdConSolicitudes(idSolicitante);
        UsuarioVO solicitado = this.usuarioDAO.obtenerUsuarioPorIdConSolicitudes(idSolicitado);

        //Aca remover de las listas de recibidas y enviadas los registros.
        solicitante.setEnviadas(this.eliminarSolicitanteDeSet(solicitante.getEnviadas(), idSolicitado));
        solicitado.setRecibidas(this.eliminarSolicitanteDeSet(solicitado.getRecibidas(), idSolicitante));

        this.usuarioDAO.merge(solicitante);
        this.usuarioDAO.merge(solicitado);
    }

    public Set<UsuarioVO> eliminarSolicitanteDeSet(Set<UsuarioVO> solicitudes, Integer idContacto) {
        Set<UsuarioVO> aux = new HashSet<UsuarioVO>();
        System.out.println("Contacto que no debe entrar:" + idContacto);
        for (UsuarioVO vo : new ArrayList<UsuarioVO>(solicitudes)) {
            if (vo.getId() != idContacto) {
                System.out.println("Contacto para lista:" + vo.getId());
                aux.add(vo);
            }
        }
        return aux;
    }
    
    public Set<UsuarioVO> eliminarUsuarioDeSetUsuarios(Set<UsuarioVO> usuarios, Integer idUsuario) {
        Set<UsuarioVO> aux = new HashSet<UsuarioVO>();
        for (UsuarioVO vo : new ArrayList<UsuarioVO>(usuarios)) {
            if (vo.getId() != idUsuario) {
                aux.add(vo);
            }
        }
        return aux;
    }
}
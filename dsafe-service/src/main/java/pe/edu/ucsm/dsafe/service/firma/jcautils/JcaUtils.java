package pe.edu.ucsm.dsafe.service.firma.jcautils;

import java.io.ByteArrayInputStream;
import java.security.Principal;
import java.security.cert.CertPathBuilder;
import java.security.cert.CertStore;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collections;
import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;

/**
 * Utils
 */
public class JcaUtils
{
    
    /**
     * Return a boolean array representing passed in keyUsage mask.
     *
     * @param mask keyUsage mask.
     */
    public static boolean[] getKeyUsage(int mask)
    {
        byte[] bytes = new byte[] { (byte)(mask & 0xff), (byte)((mask & 0xff00) >> 8) };
        boolean[] keyUsage = new boolean[9];

        for (int i = 0; i != 9; i++)
        {
            keyUsage[i] = (bytes[i / 8] & (0x80 >>> (i % 8))) != 0;
        }

        return keyUsage;
    }

    /**
     * Build a path using the given root as the trust anchor, and the passed
     * in end constraints and certificate store.
     * <p>
     * Note: the path is built with revocation checking turned off.
     */
    public static PKIXCertPathBuilderResult buildPath(
        X509Certificate  rootCert,
        X509CertSelector endConstraints,
        CertStore certsAndCRLs)
        throws Exception
    {
        CertPathBuilder       builder = CertPathBuilder.getInstance("PKIX", "BC");
        PKIXBuilderParameters buildParams = new PKIXBuilderParameters(Collections.singleton(new TrustAnchor(rootCert, null)), endConstraints);

        buildParams.addCertStore(certsAndCRLs);
        buildParams.setRevocationEnabled(false);

        return (PKIXCertPathBuilderResult)builder.build(buildParams);
    }
    
    public static X509Certificate getCertificado (byte[] bytesCertificate)
    {
       X509Certificate certificate = null;
       try{
        // open an input stream to the file
        ByteArrayInputStream bais = new ByteArrayInputStream(bytesCertificate);
        
        // instantiate a CertificateFactory for X.509
        CertificateFactory cf = CertificateFactory.getInstance("X.509");

        certificate = (X509Certificate)cf.generateCertificate(bais);
        }catch(Exception e){
            e.printStackTrace();
        }
       return certificate;
    }
    
    public static String obtenerCommonName(Principal principal)
    {
       String commonName = ""; 
       try{ 
            LdapName dn = new LdapName(principal.getName());
            for (int i = 0; i < dn.size(); i++) {
                String atribute = dn.get(i);
                if(atribute.startsWith("CN=")||atribute.startsWith("cn=")||atribute.startsWith("Cn="))
                {
                    String[] atributeArray = atribute.split("=");
                    commonName = atributeArray[1];
                }
            }
       }catch(InvalidNameException  e) {
            commonName = "No se pudo parsear el nombre del atributo";
       }
       return commonName;
    }
}

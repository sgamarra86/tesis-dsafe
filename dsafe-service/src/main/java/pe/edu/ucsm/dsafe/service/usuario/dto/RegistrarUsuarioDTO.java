package pe.edu.ucsm.dsafe.service.usuario.dto;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class RegistrarUsuarioDTO {

    private Integer id;
    private String email;
    private String password;
    private String nombres;
    private String apellidos;
    private String dni;
    private String fono;
    private Short estado;
    private CommonsMultipartFile avatar;
    private CommonsMultipartFile firma;

    public RegistrarUsuarioDTO() {
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the nombres
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * @param nombres the nombres to set
     */
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    /**
     * @return the apellidos
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * @param apellidos the apellidos to set
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * @return the dni
     */
    public String getDni() {
        return dni;
    }

    /**
     * @param dni the dni to set
     */
    public void setDni(String dni) {
        this.dni = dni;
    }

    /**
     * @return the fono
     */
    public String getFono() {
        return fono;
    }

    /**
     * @param fono the fono to set
     */
    public void setFono(String fono) {
        this.fono = fono;
    }

    /**
     * @return the estado
     */
    public Short getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(Short estado) {
        this.estado = estado;
    }

    /**
     * @return the avatar
     */
    public CommonsMultipartFile getAvatar() {
        return avatar;
    }

    /**
     * @param avatar the avatar to set
     */
    public void setAvatar(CommonsMultipartFile avatar) {
        this.avatar = avatar;
    }

    /**
     * @return the firma
     */
    public CommonsMultipartFile getFirma() {
        return firma;
    }

    /**
     * @param firma the firma to set
     */
    public void setFirma(CommonsMultipartFile firma) {
        this.firma = firma;
    }



}
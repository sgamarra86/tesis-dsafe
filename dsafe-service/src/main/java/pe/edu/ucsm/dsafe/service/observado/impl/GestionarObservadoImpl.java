/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.observado.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.ucsm.dsafe.dataaccess.domain.ObservadoDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.DocumentoVO;
import pe.edu.ucsm.dsafe.dataaccess.model.ObservadoVO;
import pe.edu.ucsm.dsafe.dataaccess.model.UsuarioVO;
import pe.edu.ucsm.dsafe.service.documento.dto.ListarPendienteDTO;
import pe.edu.ucsm.dsafe.service.observado.GestionarObservado;

/**
 *
 * @author Sergio Gamarra
 */
@Service("gestionarObservado")
public class GestionarObservadoImpl implements GestionarObservado {

    @Autowired
    private ObservadoDAO observadoDAO;

    @Override
    public List obtenerObservadosPorIdUsuario(Integer idUsuario) {
        return this.observadoDAO.obtenerObservadosPorIdUsuario(idUsuario);
    }

    @Override
    public List obtenerObservadosPorIdUsuarioResumido(Integer idUsuario) {
        List<ListarPendienteDTO> response = new ArrayList<ListarPendienteDTO>();
        List<ObservadoVO> observados = this.observadoDAO.obtenerObservadosPorIdUsuarioResumido(idUsuario);
        ListarPendienteDTO dto;
        for (ObservadoVO vo : observados) {
            dto = new ListarPendienteDTO();
            dto.setId(vo.getDocumento().getId());
            dto.setNombre(vo.getDocumento().getNombre());
            dto.setMensaje(vo.getDocumento().getMensaje_firma());
            response.add(dto);
        }
        return response;
    }

    @Override
    public Integer registrarObservado(Integer idUsuario, Integer idDocumento) {
        ObservadoVO vo = new ObservadoVO();
        vo.setUsuario(new UsuarioVO(idUsuario));
        vo.setDocumento(new DocumentoVO(idDocumento));
        return this.observadoDAO.insert(vo);
    }

    @Override
    public void registrarObservados(Integer[] idUsuarios, Integer idDocumento) {
        ObservadoVO vo;
        for (Integer idUsuario : idUsuarios) {
            vo = new ObservadoVO();
            vo.setUsuario(new UsuarioVO(idUsuario));
            vo.setDocumento(new DocumentoVO(idDocumento));
            this.observadoDAO.insert(vo);
        }
    }
}

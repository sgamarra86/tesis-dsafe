/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.documento.dto;

/**
 *
 * @author Sergio Gamarra
 */
public class ListarDocumentoDTO {
    private Integer id;
    private String nombre;
    private String fecha;
    private Integer firmasNecesarias;
    private Integer firmasRealizadas;
    private Integer destinatarios;
    private Integer estado;
    private String descripcion;
    private boolean reportado;

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the firmasNecesarias
     */
    public Integer getFirmasNecesarias() {
        return firmasNecesarias;
    }

    /**
     * @param firmasNecesarias the firmasNecesarias to set
     */
    public void setFirmasNecesarias(Integer firmasNecesarias) {
        this.firmasNecesarias = firmasNecesarias;
    }

    /**
     * @return the firmasRealizadas
     */
    public Integer getFirmasRealizadas() {
        return firmasRealizadas;
    }

    /**
     * @param firmasRealizadas the firmasRealizadas to set
     */
    public void setFirmasRealizadas(Integer firmasRealizadas) {
        this.firmasRealizadas = firmasRealizadas;
    }

    /**
     * @return the destinatarios
     */
    public Integer getDestinatarios() {
        return destinatarios;
    }

    /**
     * @param destinatarios the destinatarios to set
     */
    public void setDestinatarios(Integer destinatarios) {
        this.destinatarios = destinatarios;
    }

    /**
     * @return the estado
     */
    public Integer getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the reportado
     */
    public boolean isReportado() {
        return reportado;
    }

    /**
     * @param reportado the reportado to set
     */
    public void setReportado(boolean reportado) {
        this.reportado = reportado;
    }
    
    
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.firma;

import java.util.List;
import pe.edu.ucsm.dsafe.service.documento.dto.RegistrarFirmaRealizadaDTO;
import pe.edu.ucsm.dsafe.service.firma.dto.FirmarDocumentoRequerimientoDTO;
import pe.edu.ucsm.dsafe.service.firma.dto.FirmarDocumentoResultadoDTO;

/**
 *
 * @author Sergio Gamarra
 */
public interface GestionarFirma {
       
    public List obtenerFirmasRequeridasPorIdDocumento(Integer idDocumento);
    
    public List obtenerFirmasPendientesPorIdUsuario(Integer IdUsuario);
    
    public List obtenerFirmasPendientesPorIdUsuarioResumido(Integer IdUsuario);
    
    public List obtenerFirmasRealizadasPorIdUsuario(Integer IdUsuario);

    public void registrarFirmasRequeridas(Integer[] idUsuarios, Integer idDocumento);
    
    public void registrarFirmaRealizada(RegistrarFirmaRealizadaDTO dto);    
    
    public FirmarDocumentoResultadoDTO firmarDocumento(FirmarDocumentoRequerimientoDTO requerimiento);
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.documento.dto;

import java.util.List;

/**
 *
 * @author Sergio Gamarra
 */
public class DatosVerificarDocumentoDTO {
    private Integer id;
    private String nombre;
    private String descripcion;
    private String fechaRegistro;
    private boolean reportado;
    private boolean firmaRequerida;
    private Integer firmasRequeridas;
    private Integer firmasRealizadas;
    private List<DatosFirmaDTO> firmas;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the fechaRegistro
     */
    public String getFechaRegistro() {
        return fechaRegistro;
    }

    /**
     * @param fechaRegistro the fechaRegistro to set
     */
    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    /**
     * @return the firmasRequeridas
     */
    public Integer getFirmasRequeridas() {
        return firmasRequeridas;
    }

    /**
     * @param firmasRequeridas the firmasRequeridas to set
     */
    public void setFirmasRequeridas(Integer firmasRequeridas) {
        this.firmasRequeridas = firmasRequeridas;
    }

    /**
     * @return the firmasRealizadas
     */
    public Integer getFirmasRealizadas() {
        return firmasRealizadas;
    }

    /**
     * @param firmasRealizadas the firmasRealizadas to set
     */
    public void setFirmasRealizadas(Integer firmasRealizadas) {
        this.firmasRealizadas = firmasRealizadas;
    }

    /**
     * @return the firmas
     */
    public List<DatosFirmaDTO> getFirmas() {
        return firmas;
    }

    /**
     * @param firmas the firmas to set
     */
    public void setFirmas(List<DatosFirmaDTO> firmas) {
        this.firmas = firmas;
    }

    /**
     * @return the firmaRequerida
     */
    public boolean isFirmaRequerida() {
        return firmaRequerida;
    }

    /**
     * @param firmaRequerida the firmaRequerida to set
     */
    public void setFirmaRequerida(boolean firmaRequerida) {
        this.firmaRequerida = firmaRequerida;
    }

    /**
     * @return the reportado
     */
    public boolean isReportado() {
        return reportado;
    }

    /**
     * @param reportado the reportado to set
     */
    public void setReportado(boolean reportado) {
        this.reportado = reportado;
    }
    
}

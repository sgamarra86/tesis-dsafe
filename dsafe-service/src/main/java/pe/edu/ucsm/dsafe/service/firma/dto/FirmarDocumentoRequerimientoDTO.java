/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.firma.dto;

import java.io.Serializable;

/**
 *
 * @author Sergio Gamarra
 */
public class FirmarDocumentoRequerimientoDTO implements Serializable{
    private Integer idDocumento;
    private byte[] pkcs12;
    private String pkcs12Password;
    private byte[] certificadoRaiz;

    /**
     * @return the pkcs12
     */
    public byte[] getPkcs12() {
        return pkcs12;
    }

    /**
     * @param pkcs12 the pkcs12 to set
     */
    public void setPkcs12(byte[] pkcs12) {
        this.pkcs12 = pkcs12;
    }

    /**
     * @return the pkcs12Password
     */
    public String getPkcs12Password() {
        return pkcs12Password;
    }

    /**
     * @param pkcs12Password the pkcs12Password to set
     */
    public void setPkcs12Password(String pkcs12Password) {
        this.pkcs12Password = pkcs12Password;
    }

    /**
     * @return the certificadoRaiz
     */
    public byte[] getCertificadoRaiz() {
        return certificadoRaiz;
    }

    /**
     * @param certificadoRaiz the certificadoRaiz to set
     */
    public void setCertificadoRaiz(byte[] certificadoRaiz) {
        this.certificadoRaiz = certificadoRaiz;
    }

    /**
     * @return the idDocumento
     */
    public Integer getIdDocumento() {
        return idDocumento;
    }

    /**
     * @param idDocumento the idDocumento to set
     */
    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }
}

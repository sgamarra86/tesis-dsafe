/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.mail;

import pe.edu.ucsm.dsafe.service.mail.dto.DatosMailDTO;

/**
 *
 * @author Sergio Gamarra
 */
public interface GestionarMail {
    
    public void enviarMailNuevoUsuario(Integer idUsuario);
    public void enviarMailNuevoContacto(Integer idUsuario);
    public void enviarMailFirmantes(Integer[] idUsuarios, String documento, String mensaje);
    public void enviarMailObservadores(Integer[] idUsuarios, String documento, String mensaje);
    public void enviarMailDocumentoReportado(DatosMailDTO datosMail);
    public void enviarMailDocumentoFirmado(DatosMailDTO datosMail);
    
}

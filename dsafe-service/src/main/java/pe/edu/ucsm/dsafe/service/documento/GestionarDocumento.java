/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.documento;

import java.util.List;
import pe.edu.ucsm.dsafe.dataaccess.model.DocumentoVO;
import pe.edu.ucsm.dsafe.service.documento.dto.RegistrarDocumentoDTO;
import pe.edu.ucsm.dsafe.service.documento.dto.DatosVerificarDocumentoDTO;
import pe.edu.ucsm.dsafe.service.documento.dto.ListarDocumentoDTO;
import pe.edu.ucsm.dsafe.sign.data.ResultadoPrepararPdfDTO;

/**
 *
 * @author Sergio Gamarra
 */
public interface GestionarDocumento {

    public DocumentoVO obtenerDocumentoPorIdDocumento(Integer idDocumento);
    
    public List<ListarDocumentoDTO> obtenerDocumentosPorIdUsuario(Integer idUsuario);
    
    public Integer[] obtenerIdDocumentosPorIdUsuario(Integer idUsuario);
    
    public Integer registrarDocumento(RegistrarDocumentoDTO documentoDTO);
    
    public Integer registrarDocumentoPdf(RegistrarDocumentoDTO documentoDTO, byte[] archivoPdf);
    
    public ResultadoPrepararPdfDTO prepararDocumentoPdf(Integer[] idUsuarios, byte[] archivoPdf);
    
    public DatosVerificarDocumentoDTO verificarDocumentoPorIdDocumento (Integer idDocumento, Integer idUsuario, String path);
    
    public void modificarDocumento(DocumentoVO vo);
    
    public Integer eliminarDocumentoPorIdDocumento(Integer idDocumento); 
}

package pe.edu.ucsm.dsafe.service.autenticacion.dto;

import java.io.Serializable;
import java.util.List;
import pe.edu.ucsm.dsafe.service.contacto.dto.ContactoDTO;

/**
 * Clase utilizada para transportar los datos de un usuario logeado.
 *
 * @author Sergio Gamarra Ramirez
 */
public class UserDetailsDTO implements Serializable{

    private Integer id;
    private String email;
    private String nombres;
    private String apellidos;
    private String dni;
    private String fono;
    private Short estado;
    private List<String> lstAccesos;
    private List<ContactoDTO> contactos;
    
    //FIX: prueba de bytes en sesion?
    private byte[] avatar;
    private byte[] firma;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the nombres
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * @param nombres the nombres to set
     */
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    /**
     * @return the apellidos
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * @param apellidos the apellidos to set
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * @return the dni
     */
    public String getDni() {
        return dni;
    }

    /**
     * @param dni the dni to set
     */
    public void setDni(String dni) {
        this.dni = dni;
    }

    /**
     * @return the fono
     */
    public String getFono() {
        return fono;
    }

    /**
     * @param fono the fono to set
     */
    public void setFono(String fono) {
        this.fono = fono;
    }

    /**
     * @return the estado
     */
    public Short getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(Short estado) {
        this.estado = estado;
    }

    /**
     * @return the lstAccesos
     */
    public List<String> getLstAccesos() {
        return lstAccesos;
    }

    /**
     * @param lstAccesos the lstAccesos to set
     */
    public void setLstAccesos(List<String> lstAccesos) {
        this.lstAccesos = lstAccesos;
    }

    /**
     * @return the contactos
     */
    public List<ContactoDTO> getContactos() {
        return contactos;
    }

    /**
     * @param contactos the contactos to set
     */
    public void setContactos(List<ContactoDTO> contactos) {
        this.contactos = contactos;
    }

    /**
     * @return the avatar
     */
    public byte[] getAvatar() {
        return avatar;
    }

    /**
     * @param avatar the avatar to set
     */
    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    /**
     * @return the firma
     */
    public byte[] getFirma() {
        return firma;
    }

    /**
     * @param firma the firma to set
     */
    public void setFirma(byte[] firma) {
        this.firma = firma;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.usuario;

import java.util.List;
import pe.edu.ucsm.dsafe.dataaccess.model.UsuarioVO;
import pe.edu.ucsm.dsafe.service.usuario.dto.RegistrarUsuarioDTO;

/**
 *
 * @author Sergio Gamarra
 */
public interface GestionarUsuario {

    /**
     * Obtener un usuario.
     *
     * @param email
     * @return usuario
     */
    public UsuarioVO obtenerUsuarioPorEmail(String email);

    /**
     * Registra un nuevo usuario.
     *
     * @param usuario
     * @return Id del usuario creado.
     */
    public int registrarUsuario(RegistrarUsuarioDTO usuarioDTO);

    /**
     * Modica los datos un usuario.
     *
     * @param usuario
     */
    public void modificarUsuario(UsuarioVO usuario);
    
//    public List obtenerUsuariosPorIds(String[] idUsuarios);
}

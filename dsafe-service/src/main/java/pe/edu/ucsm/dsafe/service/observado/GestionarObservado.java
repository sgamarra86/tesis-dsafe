/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.observado;

import java.util.List;

/**
 *
 * @author Sergio Gamarra
 */
public interface GestionarObservado {
    
    public List obtenerObservadosPorIdUsuario(Integer idUsuario);
    
    public List obtenerObservadosPorIdUsuarioResumido(Integer idUsuario);
        
    public Integer registrarObservado(Integer idUsuario, Integer idDocumento);
    
    public void registrarObservados(Integer[] idUsuarios, Integer idDocumento);
    
    
}

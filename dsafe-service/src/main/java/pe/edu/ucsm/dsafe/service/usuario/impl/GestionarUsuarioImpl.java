/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.usuario.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.ucsm.dsafe.common.base.EstadoUsuario;
import pe.edu.ucsm.dsafe.dataaccess.domain.UsuarioDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.UsuarioVO;
import pe.edu.ucsm.dsafe.service.usuario.GestionarUsuario;
import pe.edu.ucsm.dsafe.service.usuario.dto.RegistrarUsuarioDTO;

/**
 *
 * @author Sergio Gamarra
 */
@Service("gestionarUsuario")
public class GestionarUsuarioImpl implements GestionarUsuario {

    @Autowired
    private UsuarioDAO usuarioDAO;

    @Override
    public UsuarioVO obtenerUsuarioPorEmail(String email) {
        return this.usuarioDAO.obtenerUsuarioPorEmail(email);
    }

    @Override
    public int registrarUsuario(RegistrarUsuarioDTO usuarioDTO) {
        //TODO: cifrado de password.
        UsuarioVO usuarioVO = new UsuarioVO();
        usuarioVO.setApellidos(usuarioDTO.getApellidos());
        usuarioVO.setNombres(usuarioDTO.getNombres());
        usuarioVO.setEmail(usuarioDTO.getEmail());
        usuarioVO.setPassword(usuarioDTO.getPassword());
        usuarioVO.setDni(usuarioDTO.getDni());
        usuarioVO.setFono(usuarioDTO.getFono());
        usuarioVO.setEstado(EstadoUsuario.DESCONECTADO.getVal());
        usuarioVO.setAvatar(usuarioDTO.getAvatar().getBytes());
        usuarioVO.setFirma(usuarioDTO.getFirma().getBytes());
        return this.usuarioDAO.insert(usuarioVO);
    }

    @Override
    public void modificarUsuario(UsuarioVO usuario) {
        this.usuarioDAO.update(usuario);
    }

//    @Override
//    public List obtenerUsuariosPorIds(String[] idUsuarios) {
//        return this.usuarioDAO.obtenerUsuariosPorIds(idUsuarios);
//    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.notificacion;

import java.util.List;
import pe.edu.ucsm.dsafe.service.notificacion.dto.RegistrarNotificacionDTO;
import pe.edu.ucsm.dsafe.service.notificacion.dto.ListarNotificacionDTO;

/**
 *
 * @author Sergio Gamarra
 */
public interface GestionarNotificacion {
    
    public List<ListarNotificacionDTO> obtenerNotificacionesPorIdDocumentos(Integer[] idDocumentos);
    
    public List<ListarNotificacionDTO> obtenerNotificacionesPorIdDocumentosResumido(Integer[] idDocumentos);
    
    public Integer registrarNotificacion(RegistrarNotificacionDTO dto);
    
    public boolean esDocumentoReportado(Integer idDocumento);
        
}

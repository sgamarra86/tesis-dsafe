/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.mail.dto;

/**
 *
 * @author Sergio Gamarra
 */
public class DatosMailDTO {
    private Integer[] idUsuarios;
    private Integer idDocumento;
    private String nomDocumento;
    private String mensaje;

    public DatosMailDTO(Integer[] idUsuarios, Integer idDocumento, String nomDocumento, String mensaje) {
        this.idUsuarios = idUsuarios;
        this.idDocumento = idDocumento;
        this.nomDocumento = nomDocumento;
        this.mensaje = mensaje;
    }
    
    /**
     * @return the idUsuarios
     */
    public Integer[] getIdUsuarios() {
        return idUsuarios;
    }

    /**
     * @param idUsuarios the idUsuarios to set
     */
    public void setIdUsuarios(Integer[] idUsuarios) {
        this.idUsuarios = idUsuarios;
    }

    /**
     * @return the idDocumento
     */
    public Integer getIdDocumento() {
        return idDocumento;
    }

    /**
     * @param idDocumento the idDocumento to set
     */
    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    /**
     * @return the nomDocumento
     */
    public String getNomDocumento() {
        return nomDocumento;
    }

    /**
     * @param nomDocumento the nomDocumento to set
     */
    public void setNomDocumento(String nomDocumento) {
        this.nomDocumento = nomDocumento;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

}

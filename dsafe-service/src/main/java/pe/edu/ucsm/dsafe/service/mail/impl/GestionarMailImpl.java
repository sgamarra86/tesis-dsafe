/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.service.mail.impl;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.ucsm.dsafe.common.base.Mail;
import pe.edu.ucsm.dsafe.dataaccess.domain.UsuarioDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.UsuarioVO;
import pe.edu.ucsm.dsafe.service.documento.impl.GestionarDocumentoImpl;
import pe.edu.ucsm.dsafe.service.mail.GestionarMail;
import pe.edu.ucsm.dsafe.service.mail.dto.DatosMailDTO;

/**
 *
 * @author Sergio Gamarra
 */
@Service("gestionarMail")
public class GestionarMailImpl implements GestionarMail {

    @Autowired
    private UsuarioDAO usuarioDAO;

    @Override
    public void enviarMailFirmantes(Integer[] idUsuarios, String documento, String mensaje) {
        Mail mail;
        UsuarioVO usuario;
        String ip;
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException ex) {
            ip = "localhost";
        }
        String titulo = "Notificación DSAFE: Un nuevo documento requiere su firma.";
        String contenido = "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px/1.25em 'Helvetica Neue',Arial,Helvetica\">"
                + "<tbody>"
                + "<tr width=\"100%\">"
                + "<td valign=\"top\" align=\"left\" style=\"background:#eef0f1;font:15px/1.25em 'Helvetica Neue',Arial,Helvetica\">"
                + "<table style=\"border:none;padding:0 18px;margin:50px auto;width:500px\">"
                + "<tbody>"
                + "<tr width=\"100%\" height=\"60\">"
                + "<td valign=\"top\" align=\"left\" style=\"border-top-left-radius:4px;border-top-right-radius:4px;background:#27709b;padding:10px 18px;text-align:center;color:white;text-align: center;vertical-align: middle;font-size: 25px;font-weight: bolder;\">"
                + "DSAFE"
                + "</td></tr>"
                + "<tr width=\"100%\">"
                + "<td valign=\"top\" align=\"left\" style=\"border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#fff;padding:18px\">"
                + "<p>Estimado usuario, se requiere que realice la firma digital del documento <b>" + documento + "</b>, con el sgte mensaje:</p>"
                + "<p><i>\"" + mensaje + "\"</i></p>"
                + "<p>Haga click en el siguiente <a href='https://" + ip + ":8443/dsafe-web/listar_documento.html?tab=0'>link</a> para que pueda utilizar <b>DSAFE</b> y firmarlo.</p>"
                + "</td></tr></tbody>"
                + "</td></tr></tbody></table>";
        for (Integer id : idUsuarios) {
            mail = new Mail();
            usuario = this.usuarioDAO.obtenerUsuarioPorId(id);
            try {
                mail.enviarEmail(usuario.getEmail(), titulo, contenido);
            } catch (MessagingException ex) {
                Logger.getLogger(GestionarDocumentoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void enviarMailObservadores(Integer[] idUsuarios, String documento, String mensaje) {
        Mail mail;
        UsuarioVO usuario;
        String ip;
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException ex) {
            ip = "localhost";
        }
        String titulo = "Notificación DSAFE: Un nuevo documento requiere su observación.";
        String contenido = "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px/1.25em 'Helvetica Neue',Arial,Helvetica\">"
                + "<tbody>"
                + "<tr width=\"100%\">"
                + "<td valign=\"top\" align=\"left\" style=\"background:#eef0f1;font:15px/1.25em 'Helvetica Neue',Arial,Helvetica\">"
                + "<table style=\"border:none;padding:0 18px;margin:50px auto;width:500px\">"
                + "<tbody>"
                + "<tr width=\"100%\" height=\"60\">"
                + "<td valign=\"top\" align=\"left\" style=\"border-top-left-radius:4px;border-top-right-radius:4px;background:#27709b;padding:10px 18px;text-align:center;color:white;text-align: center;vertical-align: middle;font-size: 25px;font-weight: bolder;\">"
                + "DSAFE"
                + "</td></tr>"
                + "<tr width=\"100%\">"
                + "<td valign=\"top\" align=\"left\" style=\"border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#fff;padding:18px\">"
                + "<p>Estimado usuario, ha sido asignado como observador del documento <b>" + documento + "</b>, con el sgte mensaje:</p>"
                + "<p><i>\"" + mensaje + "\"</i></p>"
                + "<p>Haga click en el siguiente <a href='https://" + ip + ":8443/dsafe-web/listar_documento.html?tab=0'>link</a> para que pueda utilizar <b>DSAFE</b> y verificarlo.</p>"
                + "</td></tr></tbody>"
                + "</td></tr></tbody></table>";

        for (Integer id : idUsuarios) {
            mail = new Mail();
            usuario = this.usuarioDAO.obtenerUsuarioPorId(id);
            try {
                mail.enviarEmail(usuario.getEmail(), titulo, contenido);
            } catch (MessagingException ex) {
                Logger.getLogger(GestionarDocumentoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void enviarMailNuevoUsuario(Integer idUsuario) {
        Mail mail = new Mail();
        UsuarioVO usuario = this.usuarioDAO.obtenerUsuarioPorId(idUsuario);
        String ip;
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException ex) {
            ip = "localhost";
        }

        String titulo = "Notificación DSAFE: Nuevo usuario registrado.";
        String contenido = "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px/1.25em 'Helvetica Neue',Arial,Helvetica\">"
                + "<tbody>"
                + "<tr width=\"100%\">"
                + "<td valign=\"top\" align=\"left\" style=\"background:#eef0f1;font:15px/1.25em 'Helvetica Neue',Arial,Helvetica\">"
                + "<table style=\"border:none;padding:0 18px;margin:50px auto;width:500px\">"
                + "<tbody>"
                + "<tr width=\"100%\" height=\"60\">"
                + "<td valign=\"top\" align=\"left\" style=\"border-top-left-radius:4px;border-top-right-radius:4px;background:#27709b;padding:10px 18px;text-align:center;color:white;text-align: center;vertical-align: middle;font-size: 25px;font-weight: bolder;\">"
                + "DSAFE"
                + "</td></tr>"
                + "<tr width=\"100%\">"
                + "<td valign=\"top\" align=\"left\" style=\"border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#fff;padding:18px\">"
                + "<p>Estimado usuario <b>" + usuario.getNombres() + " " + usuario.getApellidos() + "</b>:</p>"
                + "<p>Su registro se ha completado satisfactoriamente con los siguientes datos:</p>"
                + "<p><ul>"
                + "<li>DNI: " + usuario.getDni() + "</li>"
                + "<li>Teléfono: " + usuario.getFono() + "</li>"
                + "<li>Email: " + usuario.getEmail() + "</li>"
                + "</ul></p>"
                + "<p>Haga click en el siguiente <a href='https://" + ip + ":8443/dsafe-web/main.html'>link</a> para que pueda utilizar <b>DSAFE</b></p>"
                + "</td></tr></tbody>"
                + "</td></tr></tbody></table>";

        try {
            mail.enviarEmail(usuario.getEmail(), titulo, contenido);
        } catch (MessagingException ex) {
            Logger.getLogger(GestionarDocumentoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void enviarMailNuevoContacto(Integer idUsuario) {
        Mail mail = new Mail();
        UsuarioVO usuario = this.usuarioDAO.obtenerUsuarioPorId(idUsuario);
        String ip;
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException ex) {
            ip = "localhost";
        }

        String titulo = "Notificación DSAFE: Nueva solicitud de contacto.";
        String contenido = "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px/1.25em 'Helvetica Neue',Arial,Helvetica\">"
                + "<tbody>"
                + "<tr width=\"100%\">"
                + "<td valign=\"top\" align=\"left\" style=\"background:#eef0f1;font:15px/1.25em 'Helvetica Neue',Arial,Helvetica\">"
                + "<table style=\"border:none;padding:0 18px;margin:50px auto;width:500px\">"
                + "<tbody>"
                + "<tr width=\"100%\" height=\"60\">"
                + "<td valign=\"top\" align=\"left\" style=\"border-top-left-radius:4px;border-top-right-radius:4px;background:#27709b;padding:10px 18px;text-align:center;color:white;text-align: center;vertical-align: middle;font-size: 25px;font-weight: bolder;\">"
                + "DSAFE"
                + "</td></tr>"
                + "<tr width=\"100%\">"
                + "<td valign=\"top\" align=\"left\" style=\"border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#fff;padding:18px\">"
                + "<p>Estimado usuario, usted ha recibido una nueva solicitud de contacto.</p>"
                + "<p>Haga click en el siguiente <a href='https://" + ip + ":8443/dsafe-web/listar_solicitud_recibida.html'>link</a> para que pueda utilizar <b>DSAFE</b> y revisarla.</p>"
                + "</td></tr></tbody>"
                + "</td></tr></tbody></table>";

        try {
            mail.enviarEmail(usuario.getEmail(), titulo, contenido);
        } catch (MessagingException ex) {
            Logger.getLogger(GestionarDocumentoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void enviarMailDocumentoReportado(DatosMailDTO datosMail) {
        Mail mail;
        UsuarioVO usuario;
        String ip;
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException ex) {
            ip = "localhost";
        }
        String titulo = "Notificación DSAFE: Se ha reportado uno de sus documentos.";
        String contenido = "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px/1.25em 'Helvetica Neue',Arial,Helvetica\">"
                + "<tbody>"
                + "<tr width=\"100%\">"
                + "<td valign=\"top\" align=\"left\" style=\"background:#eef0f1;font:15px/1.25em 'Helvetica Neue',Arial,Helvetica\">"
                + "<table style=\"border:none;padding:0 18px;margin:50px auto;width:500px\">"
                + "<tbody>"
                + "<tr width=\"100%\" height=\"60\">"
                + "<td valign=\"top\" align=\"left\" style=\"border-top-left-radius:4px;border-top-right-radius:4px;background:#27709b;padding:10px 18px;text-align:center;color:white;text-align: center;vertical-align: middle;font-size: 25px;font-weight: bolder;\">"
                + "DSAFE"
                + "</td></tr>"
                + "<tr width=\"100%\">"
                + "<td valign=\"top\" align=\"left\" style=\"border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#fff;padding:18px\">"
                + "<p>Estimado usuario, se ha reportado el documento <b>" + datosMail.getNomDocumento() + "</b>, con el sgte mensaje:</p>"
                + "<p><i>\"" + datosMail.getMensaje() + "\"</i></p>"
                + "<p>Haga click en el siguiente <a href='https://" + ip + ":8443/dsafe-web/ver_documento.html?idDocumento=" + datosMail.getIdDocumento() + "'>link</a> para que pueda utilizar <b>DSAFE</b> y revisarlo.</p>"
                + "</td></tr></tbody>"
                + "</td></tr></tbody></table>";
        for (Integer id : datosMail.getIdUsuarios()) {
            mail = new Mail();
            usuario = this.usuarioDAO.obtenerUsuarioPorId(id);
            try {
                mail.enviarEmail(usuario.getEmail(), titulo, contenido);
            } catch (MessagingException ex) {
                Logger.getLogger(GestionarDocumentoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void enviarMailDocumentoFirmado(DatosMailDTO datosMail) {
        Mail mail;
        UsuarioVO usuario;
        String ip;
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException ex) {
            ip = "localhost";
        }
        String titulo = "Notificación DSAFE: Se ha relizado la firma de un documento.";
        String contenido = "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px/1.25em 'Helvetica Neue',Arial,Helvetica\">"
                + "<tbody>"
                + "<tr width=\"100%\">"
                + "<td valign=\"top\" align=\"left\" style=\"background:#eef0f1;font:15px/1.25em 'Helvetica Neue',Arial,Helvetica\">"
                + "<table style=\"border:none;padding:0 18px;margin:50px auto;width:500px\">"
                + "<tbody>"
                + "<tr width=\"100%\" height=\"60\">"
                + "<td valign=\"top\" align=\"left\" style=\"border-top-left-radius:4px;border-top-right-radius:4px;background:#27709b;padding:10px 18px;text-align:center;color:white;text-align: center;vertical-align: middle;font-size: 25px;font-weight: bolder;\">"
                + "DSAFE"
                + "</td></tr>"
                + "<tr width=\"100%\">"
                + "<td valign=\"top\" align=\"left\" style=\"border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#fff;padding:18px\">"
                + "<p>Estimado usuario, el documento <b>" + datosMail.getNomDocumento() + "</b>, ha sido firmado.</p>"
                + "<p>Haga click en el siguiente <a href='https://" + ip + ":8443/dsafe-web/ver_documento.html?idDocumento=" + datosMail.getIdDocumento() + "'>link</a> para que pueda utilizar <b>DSAFE</b> y revisarlo.</p>"
                + "</td></tr></tbody>"
                + "</td></tr></tbody></table>";
        for (Integer id : datosMail.getIdUsuarios()) {
            mail = new Mail();
            usuario = this.usuarioDAO.obtenerUsuarioPorId(id);
            try {
                mail.enviarEmail(usuario.getEmail(), titulo, contenido);
            } catch (MessagingException ex) {
                Logger.getLogger(GestionarDocumentoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
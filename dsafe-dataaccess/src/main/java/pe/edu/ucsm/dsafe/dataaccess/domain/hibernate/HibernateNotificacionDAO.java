package pe.edu.ucsm.dsafe.dataaccess.domain.hibernate;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import pe.edu.ucsm.dsafe.common.base.Constants;
import pe.edu.ucsm.dsafe.dataaccess.base.hibernate.HibernateGenericDAO;
import pe.edu.ucsm.dsafe.dataaccess.domain.NotificacionDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.NotificacionVO;

/**
 * Implementación de la clase NotificacionDAO.
 *
 * @author Sergio Gamarra
 */
public class HibernateNotificacionDAO extends HibernateGenericDAO<NotificacionVO, Integer>
        implements NotificacionDAO {

    public HibernateNotificacionDAO() {
        super(NotificacionVO.class);
    }

    @Override
    public List<NotificacionVO> obtenerNotificacionesPorIdUsuario(Integer idUsuario) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(NotificacionVO.class);
        detachedCriteria.add(Restrictions.eq("idUsuario", idUsuario));
        List<NotificacionVO> notificaciones = listByCriteria(detachedCriteria);
        return notificaciones;
    }

    @Override
    public List<NotificacionVO> obtenerNotificacionesPorIdDocumentos(Integer[] idDocumentos) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(NotificacionVO.class);
        detachedCriteria.addOrder(Order.desc("fecha"));
        Disjunction or = Restrictions.disjunction();
        for (Integer idDocumento : idDocumentos) {
            or.add(Restrictions.eq("idDocumento", idDocumento));
        }
        detachedCriteria.add(or);

        List<NotificacionVO> notificaciones = listByCriteria(detachedCriteria);
        return notificaciones;
    }

    @Override
    public List<NotificacionVO> obtenerNotificacionesPorIdDocumentosResumido(Integer[] idDocumentos) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(NotificacionVO.class);
        detachedCriteria.addOrder(Order.desc("fecha"));
        Disjunction or = Restrictions.disjunction();
        for (Integer idDocumento : idDocumentos) {
            or.add(Restrictions.eq("idDocumento", idDocumento));
        }
        detachedCriteria.add(or);
        List<NotificacionVO> notificaciones = listByCriteria(detachedCriteria, 0, Constants.MAIN_LISTA_MAX);
        return notificaciones;
    }

    @Override
    public boolean esDocumentoReportado(Integer idDocumento) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(NotificacionVO.class);
        detachedCriteria.add(Restrictions.eq("idDocumento", idDocumento));
        detachedCriteria.add(Restrictions.eq("tipo", Constants.DOCUMENTO_REPORTADO));
        List<NotificacionVO> notificaciones = listByCriteria(detachedCriteria);
        if(!notificaciones.isEmpty())
            return true;
        else
            return false;
    }
}
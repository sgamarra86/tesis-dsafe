package pe.edu.ucsm.dsafe.dataaccess.domain.hibernate;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import pe.edu.ucsm.dsafe.dataaccess.base.hibernate.HibernateGenericDAO;
import pe.edu.ucsm.dsafe.dataaccess.domain.GrupoDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.GrupoVO;

/**
 * Implementación de la clase UsuarioDAO.
 *
 * @author Sergio Gamarra
 */
public class HibernateGrupoDAO extends HibernateGenericDAO<GrupoVO, Integer>
        implements GrupoDAO {

    public HibernateGrupoDAO() {
        super(GrupoVO.class);
    }

    @Override
    public List<GrupoVO> obtenerGruposPorUsuario(Integer id) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(GrupoVO.class);
        detachedCriteria.createAlias("usuarioVO", "usuario", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.add(Restrictions.eq("usuario.id", id));
        detachedCriteria.addOrder(Order.asc("nombre"));
        List<GrupoVO> grupos = listByCriteria(detachedCriteria);
        return grupos;
    }

    @Override
    public GrupoVO obtenerGrupoPorIdGrupo(Integer id) {
        //TODO: tengo que revisar esta parte segun como se jala de la base
        GrupoVO grupoVO = null;
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(GrupoVO.class);
        detachedCriteria.createAlias("usuarioVO", "usuario", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.add(Restrictions.eq("id", id));
        detachedCriteria.addOrder(Order.asc("nombre"));
        List<GrupoVO> grupos = listByCriteria(detachedCriteria);
        if (!grupos.isEmpty()) {
            grupoVO = (GrupoVO) grupos.get(0);
        }
        return grupoVO;
    }
}
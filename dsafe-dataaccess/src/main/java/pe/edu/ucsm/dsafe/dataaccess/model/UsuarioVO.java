package pe.edu.ucsm.dsafe.dataaccess.model;

import java.util.HashSet;
import java.util.Set;

public class UsuarioVO {

    private Integer id;
    private String email;
    private String password;
    private String nombres;
    private String apellidos;
    private String dni;
    private String fono;    //telefono...
    private Short estado;
    private Set<UsuarioVO> contactos = new HashSet<UsuarioVO>(0);
    private Set<UsuarioVO> enviadas = new HashSet<UsuarioVO>(0);
    private Set<UsuarioVO> recibidas = new HashSet<UsuarioVO>(0);
    private Set<FirmaVO> firmas = new HashSet<FirmaVO>(0);
    
    //imagen
    private byte[] avatar;
    private byte[] firma;

    public UsuarioVO() {
    }

    public UsuarioVO(Integer id) {
        this.id = id;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the nombres
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * @param nombres the nombres to set
     */
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    /**
     * @return the apellidos
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * @param apellidos the apellidos to set
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * @return the dni
     */
    public String getDni() {
        return dni;
    }

    /**
     * @param dni the dni to set
     */
    public void setDni(String dni) {
        this.dni = dni;
    }

    /**
     * @return the fono
     */
    public String getFono() {
        return fono;
    }

    /**
     * @param fono the fono to set
     */
    public void setFono(String fono) {
        this.fono = fono;
    }

    /**
     * @return the estado
     */
    public Short getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(Short estado) {
        this.estado = estado;
    }

    /**
     * @return the contactos
     */
    public Set<UsuarioVO> getContactos() {
        return contactos;
    }

    /**
     * @param contactos the contactos to set
     */
    public void setContactos(Set<UsuarioVO> contactos) {
        this.contactos = contactos;
    }

    /**
     * @return the enviadas
     */
    public Set<UsuarioVO> getEnviadas() {
        return enviadas;
    }

    /**
     * @param enviadas the enviadas to set
     */
    public void setEnviadas(Set<UsuarioVO> enviadas) {
        this.enviadas = enviadas;
    }

    /**
     * @return the recibidas
     */
    public Set<UsuarioVO> getRecibidas() {
        return recibidas;
    }

    /**
     * @param recibidas the recibidas to set
     */
    public void setRecibidas(Set<UsuarioVO> recibidas) {
        this.recibidas = recibidas;
    }

    /**
     * @return the firmas
     */
    public Set<FirmaVO> getFirmas() {
        return firmas;
    }

    /**
     * @param firmas the firmas to set
     */
    public void setFirmas(Set<FirmaVO> firmas) {
        this.firmas = firmas;
    }

    /**
     * @return the avatar
     */
    public byte[] getAvatar() {
        return avatar;
    }

    /**
     * @param avatar the avatar to set
     */
    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    /**
     * @return the firma
     */
    public byte[] getFirma() {
        return firma;
    }

    /**
     * @param firma the firma to set
     */
    public void setFirma(byte[] firma) {
        this.firma = firma;
    }
}
package pe.edu.ucsm.dsafe.dataaccess.domain;

import java.util.List;
import pe.edu.ucsm.dsafe.dataaccess.base.GenericDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.NotificacionVO;

public interface NotificacionDAO extends GenericDAO<NotificacionVO, Integer> {
   
    //parece k no se utiliza
    public List<NotificacionVO> obtenerNotificacionesPorIdUsuario(Integer idUsuario);
    
    public List<NotificacionVO> obtenerNotificacionesPorIdDocumentos(Integer[] idDocumentos);
    
    public List<NotificacionVO> obtenerNotificacionesPorIdDocumentosResumido(Integer[] idDocumentos);
    
    public boolean esDocumentoReportado(Integer idDocumento);
}
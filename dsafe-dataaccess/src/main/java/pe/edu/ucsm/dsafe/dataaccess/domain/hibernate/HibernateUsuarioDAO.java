package pe.edu.ucsm.dsafe.dataaccess.domain.hibernate;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import pe.edu.ucsm.dsafe.dataaccess.base.hibernate.HibernateGenericDAO;
import pe.edu.ucsm.dsafe.dataaccess.domain.UsuarioDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.UsuarioVO;

/**
 * Implementación de la clase UsuarioDAO.
 *
 * @author Sergio Gamarra
 */
public class HibernateUsuarioDAO extends HibernateGenericDAO<UsuarioVO, Integer>
        implements UsuarioDAO {

    public HibernateUsuarioDAO() {
        super(UsuarioVO.class);
    }

    @Override
    public boolean existeUsuarioPorEmail(String email) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(UsuarioVO.class);
        detachedCriteria.add(Restrictions.eq("email", email));
        List usuarios = listByCriteria(detachedCriteria);
        if (!usuarios.isEmpty()) {
            return true;
        }
        return false;
    }
    
    @Override
    public UsuarioVO obtenerUsuarioPorId(Integer idUsuario) {
        UsuarioVO usuarioVO = null;
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(UsuarioVO.class);
        detachedCriteria.add(Restrictions.eq("id", idUsuario));
        List usuarios = listByCriteria(detachedCriteria);
        if (!usuarios.isEmpty()) {
            usuarioVO = (UsuarioVO) usuarios.get(0);
        }
        return usuarioVO;
    }

    @Override
    public UsuarioVO obtenerUsuarioPorIdConSolicitudes(Integer idUsuario) {
        UsuarioVO usuarioVO = null;
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(UsuarioVO.class);
        detachedCriteria.add(Restrictions.eq("id", idUsuario));
        detachedCriteria.createAlias("contactos", "contactos", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("recibidas", "recibidas", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("enviadas", "enviadas", DetachedCriteria.LEFT_JOIN);
        List usuarios = listByCriteria(detachedCriteria);
        if (!usuarios.isEmpty()) {
            usuarioVO = (UsuarioVO) usuarios.get(0);
        }
        return usuarioVO;
    }

    @Override
    public UsuarioVO obtenerUsuarioPorEmail(String email) {
        UsuarioVO usuarioVO = null;
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(UsuarioVO.class);
        detachedCriteria.createAlias("contactos", "contactos", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("recibidas", "recibidas", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.add(Restrictions.eq("email", email));
        List usuarios = listByCriteria(detachedCriteria);
        if (!usuarios.isEmpty()) {
            usuarioVO = (UsuarioVO) usuarios.get(0);
        }
        return usuarioVO;
    }

    @Override
    public List obtenerUsuariosPorParametro(String parametro) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(UsuarioVO.class);
        Disjunction or = Restrictions.disjunction();
        or.add(Restrictions.ilike("email", "%" + parametro.toLowerCase() + "%"));
        or.add(Restrictions.ilike("nombres", "%" + parametro.toUpperCase() + "%"));
        or.add(Restrictions.ilike("apellidos", "%" + parametro.toUpperCase() + "%"));
        detachedCriteria.add(or);
        List usuarios = listByCriteria(detachedCriteria);
        return usuarios;
    }

    @Override
    public List obtenerUsuariosPorIds(Integer[] idUsuarios) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(UsuarioVO.class);
        Disjunction or = Restrictions.disjunction();
        for (Integer idUsuario : idUsuarios) {
            or.add(Restrictions.eq("id", idUsuario));
        }
        detachedCriteria.add(or);

        List<UsuarioVO> usuarios = listByCriteria(detachedCriteria);
        return usuarios;
    }
}
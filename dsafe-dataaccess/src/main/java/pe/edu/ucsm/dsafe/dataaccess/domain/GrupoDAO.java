package pe.edu.ucsm.dsafe.dataaccess.domain;

import java.io.Serializable;
import java.util.List;
import pe.edu.ucsm.dsafe.dataaccess.base.GenericDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.GrupoVO;

public interface GrupoDAO extends GenericDAO<GrupoVO, Integer> {
    
    public GrupoVO obtenerGrupoPorIdGrupo(Integer id);
    
    public List<GrupoVO> obtenerGruposPorUsuario(Integer idUsuario);
}
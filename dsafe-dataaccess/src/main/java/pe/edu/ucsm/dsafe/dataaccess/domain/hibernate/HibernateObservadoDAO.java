package pe.edu.ucsm.dsafe.dataaccess.domain.hibernate;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import pe.edu.ucsm.dsafe.common.base.Constants;
import pe.edu.ucsm.dsafe.dataaccess.base.hibernate.HibernateGenericDAO;
import pe.edu.ucsm.dsafe.dataaccess.domain.ObservadoDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.ObservadoVO;

/**
 * Implementación de la clase ObservandoDAO.
 *
 * @author Sergio Gamarra
 */
public class HibernateObservadoDAO extends HibernateGenericDAO<ObservadoVO, Integer>
        implements ObservadoDAO {

    public HibernateObservadoDAO() {
        super(ObservadoVO.class);
    }

    @Override
    public List<ObservadoVO> obtenerObservadosPorIdUsuario(Integer idUsuario) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ObservadoVO.class);
        detachedCriteria.createAlias("usuario", "usuario", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("documento", "documento", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.add(Restrictions.eq("usuario.id", idUsuario));
        detachedCriteria.addOrder(Order.desc("documento.fecha"));
        List<ObservadoVO> obervados = listByCriteria(detachedCriteria);
        return obervados;
    }
    
    @Override
    public List<ObservadoVO> obtenerObservadosPorIdUsuarioResumido(Integer idUsuario) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ObservadoVO.class);
        detachedCriteria.createAlias("usuario", "usuario", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("documento", "documento", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.add(Restrictions.eq("usuario.id", idUsuario));
        detachedCriteria.addOrder(Order.desc("documento.fecha"));
        List<ObservadoVO> obervados = listByCriteria(detachedCriteria,0, Constants.MAIN_LISTA_MAX);
        return obervados;
    }
    
    @Override
    public List<ObservadoVO> obtenerObservadosPorIdDocumento(Integer idDocumento){
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ObservadoVO.class);
        detachedCriteria.createAlias("usuario", "usuario", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("documento", "documento", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.add(Restrictions.eq("documento.id", idDocumento));
        List<ObservadoVO> obervados = listByCriteria(detachedCriteria);
        return obervados;
    }
    
    @Override
    public List obtenerIdDocumentosDeObservadosPorIdUsuario(Integer idUsuario) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(ObservadoVO.class);
        detachedCriteria.createAlias("usuario", "usuario", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("documento", "documento", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.add(Restrictions.eq("usuario.id", idUsuario));
        detachedCriteria.setProjection(Projections.property("documento.id"));
        List ids = listByCriteria(detachedCriteria);
        return ids;
    }


}
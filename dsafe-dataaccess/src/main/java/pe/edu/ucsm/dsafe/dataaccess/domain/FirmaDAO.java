package pe.edu.ucsm.dsafe.dataaccess.domain;

import java.util.List;
import pe.edu.ucsm.dsafe.dataaccess.base.GenericDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.FirmaVO;

public interface FirmaDAO extends GenericDAO<FirmaVO, Integer> {

    public List<FirmaVO> obtenerFirmasRequeridasPorIdDocumento(Integer idDocumento);
    
    public FirmaVO obtenerFirmaPorIdUsuarioYIdDocumento(Integer idUsuario, Integer idDocumento);

    public List obtenerIdDocumentosDeFirmasPorIdUsuario(Integer idUsuario);
    
    public List<FirmaVO> obtenerFirmasPorIdUsuario(Integer idUsuario);
    
    public List<FirmaVO> obtenerFirmasPorIdDocumento(Integer idDocumento);

    public List<FirmaVO> obtenerFirmasPendientesPorIdUsuario(Integer idUsuario);
    
    public List<FirmaVO> obtenerFirmasPendientesPorIdUsuarioResumido(Integer idUsuario);

    public List<FirmaVO> obtenerFirmasRealizadasPorIdUsuario(Integer idUsuario);
}
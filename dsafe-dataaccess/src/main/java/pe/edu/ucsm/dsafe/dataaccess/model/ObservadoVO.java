package pe.edu.ucsm.dsafe.dataaccess.model;

public class ObservadoVO {

    private Integer id;
    private UsuarioVO usuario;
    private DocumentoVO documento;
    

    public ObservadoVO() {
    }
    
    public ObservadoVO(Integer id) {
        this.id = id;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the usuario
     */
    public UsuarioVO getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(UsuarioVO usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the documento
     */
    public DocumentoVO getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(DocumentoVO documento) {
        this.documento = documento;
    }

    
}
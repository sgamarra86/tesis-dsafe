package pe.edu.ucsm.dsafe.dataaccess.domain;

import java.util.List;
import pe.edu.ucsm.dsafe.dataaccess.base.GenericDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.UsuarioVO;

public interface UsuarioDAO extends GenericDAO<UsuarioVO, Integer> {

    public UsuarioVO obtenerUsuarioPorEmail(String email);
    
    public UsuarioVO obtenerUsuarioPorId(Integer idUsuario);
    
    public UsuarioVO obtenerUsuarioPorIdConSolicitudes(Integer idUsuario);
    
    public boolean existeUsuarioPorEmail(String email);
    
    public List obtenerUsuariosPorParametro(String parametro);
    
    public List obtenerUsuariosPorIds(Integer[] idUsuarios);
}
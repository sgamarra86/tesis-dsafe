package pe.edu.ucsm.dsafe.dataaccess.domain.hibernate;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import pe.edu.ucsm.dsafe.dataaccess.base.hibernate.HibernateGenericDAO;
import pe.edu.ucsm.dsafe.dataaccess.domain.DocumentoDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.DocumentoVO;

/**
 * Implementación de la clase DocumentoDAO.
 *
 * @author Sergio Gamarra
 */
public class HibernateDocumentoDAO extends HibernateGenericDAO<DocumentoVO, Integer>
        implements DocumentoDAO {

    public HibernateDocumentoDAO() {
        super(DocumentoVO.class);
    }

    //S1: Deberia crear 1 metodo x cada tipo de consulta, este es usado x 3 y no necesariamente utilizan todos sus campos.
    //S2: Si gracias ya me entere de esto nuevamente, deberia ser 3 metodos distintos pero ya que madres.
    @Override
    public DocumentoVO obtenerDocumentoPorIdDocumento(Integer idDocumento) {
        DocumentoVO documento = null;
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(DocumentoVO.class);
        detachedCriteria.createAlias("propietario", "propietario", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("firmas", "firmas", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("firmas.usuario", "usuario1", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("observados", "observados", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("observados.usuario", "usuario2", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.add(Restrictions.eq("id", idDocumento));
        List<DocumentoVO> documentos = listByCriteria(detachedCriteria);

        if (!documentos.isEmpty()) {
            documento = (DocumentoVO) documentos.get(0);
        }
        return documento;
    }

    @Override
    public List<DocumentoVO> obtenerDocumentosPorIdUsuario(Integer idUsuario) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(DocumentoVO.class);
        detachedCriteria.createAlias("propietario", "propietario", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.add(Restrictions.eq("propietario.id", idUsuario));
        detachedCriteria.addOrder(Order.desc("fecha"));
        List<DocumentoVO> documentos = listByCriteria(detachedCriteria);
        return documentos;
    }
}
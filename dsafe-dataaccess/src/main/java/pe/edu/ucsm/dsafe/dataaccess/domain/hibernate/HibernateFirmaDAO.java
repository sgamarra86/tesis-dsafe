package pe.edu.ucsm.dsafe.dataaccess.domain.hibernate;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import pe.edu.ucsm.dsafe.common.base.Constants;
import pe.edu.ucsm.dsafe.dataaccess.base.hibernate.HibernateGenericDAO;
import pe.edu.ucsm.dsafe.dataaccess.domain.FirmaDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.FirmaVO;

/**
 * Implementación de la clase FirmaDAO.
 *
 * @author Sergio Gamarra
 */
public class HibernateFirmaDAO extends HibernateGenericDAO<FirmaVO, Integer>
        implements FirmaDAO {

    public HibernateFirmaDAO() {
        super(FirmaVO.class);
    }

    @Override
    public FirmaVO obtenerFirmaPorIdUsuarioYIdDocumento(Integer idUsuario, Integer idDocumento) {
        FirmaVO firma = new FirmaVO();
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(FirmaVO.class);
        detachedCriteria.createAlias("usuario", "usuario", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("documento", "documento", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.add(Restrictions.eq("usuario.id", idUsuario));
        detachedCriteria.add(Restrictions.eq("documento.id", idDocumento));
        List<FirmaVO> firmas = listByCriteria(detachedCriteria);
        if (!firmas.isEmpty()) {
            firma = (FirmaVO) firmas.get(0);
        }
        return firma;
    }

    @Override
    public List<FirmaVO> obtenerFirmasPorIdUsuario(Integer idUsuario) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(FirmaVO.class);
        detachedCriteria.createAlias("usuario", "usuario", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("documento", "documento", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.add(Restrictions.eq("usuario.id", idUsuario));
        List<FirmaVO> firmas = listByCriteria(detachedCriteria);
        return firmas;
    }
    
    @Override
    public List<FirmaVO> obtenerFirmasPorIdDocumento(Integer idDocumento){
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(FirmaVO.class);
        detachedCriteria.createAlias("usuario", "usuario", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("documento", "documento", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.add(Restrictions.eq("documento.id", idDocumento));
        List<FirmaVO> firmas = listByCriteria(detachedCriteria);
        return firmas;
    }

    @Override
    public List<FirmaVO> obtenerFirmasPendientesPorIdUsuario(Integer idUsuario) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(FirmaVO.class);
        detachedCriteria.createAlias("usuario", "usuario", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("documento", "documento", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.add(Restrictions.eq("usuario.id", idUsuario));
        detachedCriteria.add(Restrictions.eq("estado", Short.valueOf("0")));
        detachedCriteria.addOrder(Order.desc("documento.fecha"));
        List<FirmaVO> firmas = listByCriteria(detachedCriteria);
        return firmas;
    }
    
    @Override
    public List<FirmaVO> obtenerFirmasPendientesPorIdUsuarioResumido(Integer idUsuario) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(FirmaVO.class);
        detachedCriteria.createAlias("usuario", "usuario", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("documento", "documento", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.add(Restrictions.eq("usuario.id", idUsuario));
        detachedCriteria.add(Restrictions.eq("estado", Short.valueOf("0")));
        detachedCriteria.addOrder(Order.desc("documento.fecha"));
        List<FirmaVO> firmas = listByCriteria(detachedCriteria,0, Constants.MAIN_LISTA_MAX);
        return firmas;
    }

    @Override
    public List<FirmaVO> obtenerFirmasRealizadasPorIdUsuario(Integer idUsuario) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(FirmaVO.class);
        detachedCriteria.createAlias("usuario", "usuario", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("documento", "documento", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.add(Restrictions.eq("usuario.id", idUsuario));
        detachedCriteria.add(Restrictions.eq("estado", Short.valueOf("1")));
        detachedCriteria.addOrder(Order.desc("fecha"));
        List<FirmaVO> firmas = listByCriteria(detachedCriteria);
        return firmas;
    }

    @Override
    public List obtenerIdDocumentosDeFirmasPorIdUsuario(Integer idUsuario) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(FirmaVO.class);
        detachedCriteria.createAlias("usuario", "usuario", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("documento", "documento", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.add(Restrictions.eq("usuario.id", idUsuario));
        detachedCriteria.setProjection(Projections.property("documento.id"));
        List ids = listByCriteria(detachedCriteria);
        return ids;
    }

    @Override
    public List<FirmaVO> obtenerFirmasRequeridasPorIdDocumento(Integer idDocumento) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(FirmaVO.class);
        detachedCriteria.createAlias("usuario", "usuario", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.createAlias("documento", "documento", DetachedCriteria.LEFT_JOIN);
        detachedCriteria.add(Restrictions.eq("documento.id", idDocumento));
        List<FirmaVO> firmas = listByCriteria(detachedCriteria);
        return firmas;
    }
}
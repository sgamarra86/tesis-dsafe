package pe.edu.ucsm.dsafe.dataaccess.domain;

import java.util.List;
import pe.edu.ucsm.dsafe.dataaccess.base.GenericDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.ObservadoVO;

public interface ObservadoDAO extends GenericDAO<ObservadoVO, Integer> {

    public List<ObservadoVO> obtenerObservadosPorIdUsuario(Integer idUsuario);
       
    public List<ObservadoVO> obtenerObservadosPorIdUsuarioResumido(Integer idUsuario);
    
    public List<ObservadoVO> obtenerObservadosPorIdDocumento(Integer idDocumento);
        
    public List obtenerIdDocumentosDeObservadosPorIdUsuario(Integer idUsuario);
    
    
}
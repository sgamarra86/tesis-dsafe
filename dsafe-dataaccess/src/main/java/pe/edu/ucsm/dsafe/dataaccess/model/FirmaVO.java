package pe.edu.ucsm.dsafe.dataaccess.model;

import java.util.Calendar;

public class FirmaVO {

    private Integer id;
    private UsuarioVO usuario;
    private DocumentoVO documento;
    private Calendar fecha;
    private byte[] archivo;
    private short estado;

    public FirmaVO() {
    }
    
    public FirmaVO(Integer id) {
        this.id = id;
    }

    /**
     * @return the usuario
     */
    public UsuarioVO getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(UsuarioVO usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the documento
     */
    public DocumentoVO getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(DocumentoVO documento) {
        this.documento = documento;
    }

    /**
     * @return the fecha
     */
    public Calendar getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the archivo
     */
    public byte[] getArchivo() {
        return archivo;
    }

    /**
     * @param archivo the archivo to set
     */
    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }

    /**
     * @return the estado
     */
    public short getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(short estado) {
        this.estado = estado;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

}
package pe.edu.ucsm.dsafe.dataaccess.domain;

import java.util.List;
import pe.edu.ucsm.dsafe.dataaccess.base.GenericDAO;
import pe.edu.ucsm.dsafe.dataaccess.model.DocumentoVO;

public interface DocumentoDAO extends GenericDAO<DocumentoVO, Integer> {
   
    public DocumentoVO obtenerDocumentoPorIdDocumento(Integer idDocumento);
    
    public List<DocumentoVO> obtenerDocumentosPorIdUsuario(Integer idUsuario);

}
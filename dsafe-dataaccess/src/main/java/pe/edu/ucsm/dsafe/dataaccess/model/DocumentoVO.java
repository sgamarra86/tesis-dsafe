package pe.edu.ucsm.dsafe.dataaccess.model;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

public class DocumentoVO {

    private Integer id;
    private String nombre;
    private String extension;
    private String descripcion;
    private UsuarioVO propietario;
    private Set<FirmaVO> firmas = new HashSet<FirmaVO>(0);
    private Set<ObservadoVO> observados = new HashSet<ObservadoVO>(0);
    private Calendar fecha;
    private byte[] archivo;
    private byte[] archivoPdf;
    private String mensaje_firma;   //revisar

    public DocumentoVO() {
    }

    public DocumentoVO(Integer id) {
        this.id = id;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the extension
     */
    public String getExtension() {
        return extension;
    }

    /**
     * @param extension the extension to set
     */
    public void setExtension(String extension) {
        this.extension = extension;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the propietario
     */
    public UsuarioVO getPropietario() {
        return propietario;
    }

    /**
     * @param propietario the propietario to set
     */
    public void setPropietario(UsuarioVO propietario) {
        this.propietario = propietario;
    }

    /**
     * @return the fecha
     */
    public Calendar getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the archivo
     */
    public byte[] getArchivo() {
        return archivo;
    }

    /**
     * @param archivo the archivo to set
     */
    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }

    /**
     * @return the firmas
     */
    public Set<FirmaVO> getFirmas() {
        return firmas;
    }

    /**
     * @param firmas the firmas to set
     */
    public void setFirmas(Set<FirmaVO> firmas) {
        this.firmas = firmas;
    }

    /**
     * @return the mensaje_firma
     */
    public String getMensaje_firma() {
        return mensaje_firma;
    }

    /**
     * @param mensaje_firma the mensaje_firma to set
     */
    public void setMensaje_firma(String mensaje_firma) {
        this.mensaje_firma = mensaje_firma;
    }

    /**
     * @return the observados
     */
    public Set<ObservadoVO> getObservados() {
        return observados;
    }

    /**
     * @param observados the observados to set
     */
    public void setObservados(Set<ObservadoVO> observados) {
        this.observados = observados;
    }

    /**
     * @return the archivoPdf
     */
    public byte[] getArchivoPdf() {
        return archivoPdf;
    }

    /**
     * @param archivoPdf the archivoPdf to set
     */
    public void setArchivoPdf(byte[] archivoPdf) {
        this.archivoPdf = archivoPdf;
    }
}
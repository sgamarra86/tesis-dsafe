/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.sign;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.DigestAlgorithms;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.PrivateKeySignature;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.cert.CertStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cert.jcajce.JcaCertStoreBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSSignerDigestMismatchException;
import org.bouncycastle.cms.CMSTypedData;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoGeneratorBuilder;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.cms.jcajce.JcaX509CertSelectorConverter;
import org.bouncycastle.util.Store;
import pe.edu.ucsm.dsafe.sign.data.FirmaDTO;
import pe.edu.ucsm.dsafe.sign.data.FirmaPdfDTO;
import pe.edu.ucsm.dsafe.sign.data.FirmanteDTO;
import pe.edu.ucsm.dsafe.sign.data.PrepararFirmaPdfDTO;
import pe.edu.ucsm.dsafe.sign.data.RespuestaOCSPDTO;
import pe.edu.ucsm.dsafe.sign.data.ResultadoFirmaDTO;
import pe.edu.ucsm.dsafe.sign.data.ResultadoFirmaPdfDTO;
import pe.edu.ucsm.dsafe.sign.data.ResultadoPrepararPdfDTO;
import pe.edu.ucsm.dsafe.sign.data.ResultadoVerificacionDTO;
import pe.edu.ucsm.dsafe.sign.data.VerificarDTO;
import pe.edu.ucsm.dsafe.sign.jcautils.JcaUtils;

/**
 *
 * @author Luis
 */
public class DSafeFirma {
    static final int ANCHO_CELDA = 150;
    static final int FIRMAS_POSICION_X = 35;
    static final int FIRMAS_POSICION_Y_FROMTOP = 25;
    
    public DSafeFirma(){
        
    }
    
    public static ResultadoPrepararPdfDTO prepararPDF(PrepararFirmaPdfDTO prepararFirmaPdfDTO){
        List<FirmanteDTO> firmantes = prepararFirmaPdfDTO.getFirmantes();
        ByteArrayOutputStream osSigned = new ByteArrayOutputStream();
        ResultadoPrepararPdfDTO resultadoPrepararPdfDTO = new ResultadoPrepararPdfDTO();
        String mensaje = "Se añadieron los contenedores de Firma Digital al documento";
        boolean correcto = true;
        
        //Fuente para las cabeceras de la tabla
        Font smallfont = FontFactory.getFont("Helvetica", 8, Font.NORMAL);
        
        try{
            PdfReader reader = new PdfReader(prepararFirmaPdfDTO.getDocumento());
            //PdfStamper stamper = PdfStamper.createSignature(reader, osSigned, '\0');
            PdfStamper stamper = new PdfStamper(reader, osSigned);
            //PdfWriter writer = stamper.getWriter();
            
            int numFirmantes = firmantes.size();
            PdfPTable table = new PdfPTable(numFirmantes);
            table.setWidthPercentage(100);
            table.setTotalWidth(ANCHO_CELDA*numFirmantes);
            
            for (int i = 0; i<numFirmantes; i++){
                FirmanteDTO firmanteDTO = (FirmanteDTO)firmantes.get(i);
                String nombreFirmante = firmanteDTO.getNombres()+", "+firmanteDTO.getApellidos();
                PdfPCell myCell = new PdfPCell(new Phrase(nombreFirmante, smallfont)); 
                table.addCell(myCell);
            }

            for (int i = 0; i<numFirmantes; i++){
                FirmanteDTO firmanteDTO = (FirmanteDTO)firmantes.get(i);
                table.addCell(JcaUtils.crearContenedoresFirma(stamper,firmanteDTO.getEmail()));
            }
            
            Rectangle pageSize = reader.getPageSize(1);
            //Se establece qeu la tabla debe dibujarse en la página 1 encima de las demas capas
            PdfContentByte canvas = stamper.getOverContent(1);
            //Dibuja la tabla en la posicion especificada
            table.writeSelectedRows(0, -1, FIRMAS_POSICION_X, pageSize.getHeight() - FIRMAS_POSICION_Y_FROMTOP, canvas);
            
            // Cerrando el stamper
            stamper.close();
            reader.close();
            
        }catch(Exception e)
        {
            e.printStackTrace();
            mensaje = "No se pudo editar el documento para colocar los contenedores de Firma Digital";
            correcto = false;
        }    
        
        resultadoPrepararPdfDTO.setCorrecto(correcto);
        resultadoPrepararPdfDTO.setMensaje(mensaje);
        resultadoPrepararPdfDTO.setDocumentoPdf(osSigned.toByteArray());
        return resultadoPrepararPdfDTO;
    }
    
    public static ResultadoFirmaPdfDTO firmarPDF(FirmaPdfDTO firmaPdfDTO ){
        ResultadoFirmaPdfDTO resultFirmaPdfDTO = new ResultadoFirmaPdfDTO();
        ByteArrayOutputStream osSigned = new ByteArrayOutputStream();
        boolean correcto = true;
        String mensaje="";
        String digestAlgorithm = DigestAlgorithms.SHA256;
        String provider = "BC";
        
        MakeSignature.CryptoStandard subfilter = MakeSignature.CryptoStandard.CMS;
    
        PrivateKey pk = null;
        Certificate[] chain = null;
        char[] charPassword= firmaPdfDTO.getPkcs12Password().toCharArray();
        ByteArrayInputStream bais = new ByteArrayInputStream(firmaPdfDTO.getPkcs12());
        
        try{
            KeyStore ks = KeyStore.getInstance("PKCS12","BC");
            try{
                ks.load(bais, charPassword);
            }catch(IOException e){
                mensaje="La contraseña o la clave privada son incorrectos";
                correcto = false;
                e.printStackTrace();
            }

            if(correcto)
            {
                for (Enumeration e = ks.aliases(); e.hasMoreElements(); ) {
                    String alias = (String)e.nextElement();
                        if (ks.isKeyEntry(alias)) {
                            pk=(PrivateKey)ks.getKey(alias, charPassword);
                            chain = (Certificate[])ks.getCertificateChain(alias);
                            //cert = (X509Certificate)chain[0];
                        }
                }

                // Creando el reader y el stamper
                PdfReader reader = new PdfReader(firmaPdfDTO.getDocumento());
                PdfStamper stamper = PdfStamper.createSignature(reader, osSigned, '\0',null ,true);

                 // Creando la apariencia
                PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
                appearance.setVisibleSignature(firmaPdfDTO.getEmail());
                appearance.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC_AND_DESCRIPTION);
                appearance.setSignatureGraphic(Image.getInstance(firmaPdfDTO.getImagenFirma()));
                appearance.setCertificationLevel(PdfSignatureAppearance.NOT_CERTIFIED);

                // Creando la firma
                ExternalDigest digest = new BouncyCastleDigest();
                ExternalSignature signature =
                new PrivateKeySignature(pk, digestAlgorithm, provider);
                MakeSignature.signDetached(appearance, digest, signature, chain,
                null, null, null, 0, subfilter);
            }
        }catch(KeyStoreException e){
            mensaje="Ocurrió un problema al leer la clave privada";
            correcto = false;
            e.printStackTrace();
        }catch(IOException e){
            mensaje="No se pudo cargar el documento";
            correcto = false;
            e.printStackTrace();
        }catch(DocumentException e){
            mensaje="No se pudo crear el contenido de firma para el documento";
            correcto = false;
            e.printStackTrace();
        }catch(NoSuchProviderException e)
        {
            mensaje="No se pudo cargar la librería Bouncy Castle";
            correcto = false;
            e.printStackTrace();
        }catch(GeneralSecurityException e){
            mensaje="No se pudo crear el contenido de firma para el documento";
            correcto = false;
            e.printStackTrace();
        }
        
        resultFirmaPdfDTO.setCorrecto(correcto);
        if(correcto)
        {
            resultFirmaPdfDTO.setMensaje("Se ha firmado el documento");
            resultFirmaPdfDTO.setPdfFirmado(osSigned.toByteArray());
        }else{
            resultFirmaPdfDTO.setMensaje(mensaje);
        }
        
        return resultFirmaPdfDTO;
    }
    
    public static ResultadoFirmaDTO firmar(FirmaDTO firmaDTO ){
        byte fileSigned[] = null;
        PrivateKey key = null;
        Certificate[] chain = null;
        X509Certificate cert = null;
        ResultadoFirmaDTO resultFirmaDTO = new ResultadoFirmaDTO();
        boolean correcto = true;
        String mensaje = "";
        
        char[] charPassword= firmaDTO.getPkcs12Password().toCharArray();
        try{
            ByteArrayInputStream bais = new ByteArrayInputStream(firmaDTO.getPkcs12());
            KeyStore ks = KeyStore.getInstance("PKCS12","BC");
            try{
                ks.load(bais, charPassword);
            }catch(IOException e){
                mensaje="La contraseña o la clave privada son incorrectos";
                correcto = false;
            }

            if(correcto)
            {
                for (Enumeration e = ks.aliases(); e.hasMoreElements(); ) {
                    String alias = (String)e.nextElement();
                        if (ks.isKeyEntry(alias)) {
                            key=(PrivateKey)ks.getKey(alias, charPassword);
                            chain = (Certificate[])ks.getCertificateChain(alias);
                            cert = (X509Certificate)chain[0];
                        }
                }

                cert.checkValidity();

                X509Certificate caRootCert = JcaUtils.getCertificado(firmaDTO.getCertificadoRaiz());
                RespuestaOCSPDTO respuestaOCSPDTO = JcaUtils.verificarRevocacion(cert, caRootCert);

                if(!respuestaOCSPDTO.isCorrecto()){
                     mensaje=respuestaOCSPDTO.getMensaje();
                     correcto = false;
                }else{
                    Store certs = new JcaCertStore(Arrays.asList(chain));
                    // Preparando el Generator
                    CMSSignedDataGenerator gen = new CMSSignedDataGenerator();
                    gen.addSignerInfoGenerator(new JcaSimpleSignerInfoGeneratorBuilder().setProvider("BC").build(cert.getSigAlgName(), key, cert));
                    gen.addCertificates(certs);
                    // Crea objeto con la data firmada
                    CMSTypedData data = new CMSProcessableByteArray(firmaDTO.getDocumento());
                    //CMSSignedData signed = gen.generate(data,true);  // Flag para firma attached
                    CMSSignedData signed = gen.generate(data);  // Flag para firma dettached

                    fileSigned = signed.getEncoded();
                }
            }    
        }catch(KeyStoreException e){
            mensaje="Ocurrió un problema al leer la clave privada";
            correcto = false;
            e.printStackTrace();
        }catch (CertificateExpiredException e)
        {    
            mensaje="El certificado ha expirado";
            correcto = false;
            e.printStackTrace();
        }catch (CertificateNotYetValidException e)
        { 
            mensaje="El certificado aun no entra en vigencia";
            correcto = false;
            e.printStackTrace();
        }catch(NoSuchProviderException e)
        {
            mensaje="No se pudo cargar la librería Bouncy Castle";
            correcto = false;
            e.printStackTrace();
        }catch(Exception e)
        {
            mensaje="Ha ocurrido un error al momento de firmar el documento";
            correcto = false;
            e.printStackTrace();
        }
        resultFirmaDTO.setCorrecto(correcto);
        
        if(correcto)
        {
            resultFirmaDTO.setMensaje("Se ha firmado el documento con un certificado válido");
            resultFirmaDTO.setFirma(fileSigned);
        }else{
            resultFirmaDTO.setMensaje(mensaje);
        }
  
        return resultFirmaDTO;
    }
    
    public static ResultadoVerificacionDTO verificar(VerificarDTO verificarDTO){
        ResultadoVerificacionDTO resultVerificationDTO = new ResultadoVerificacionDTO();
        boolean resultValidation = true;
        String mensaje = "";
        X509Certificate certificate = null;
        byte[] bytesCertificado = null;
        
        try{
            CMSTypedData data = new CMSProcessableByteArray(verificarDTO.getArchivo());
            CMSSignedData signedData = new CMSSignedData(data, verificarDTO.getArchivoFirmado());
            Store certStore = signedData.getCertificates();

            CertStore certsAndCRLs = new JcaCertStoreBuilder().setProvider("BC").addCertificates(signedData.getCertificates()).build();
            SignerInformationStore signers = signedData.getSignerInfos();
            Iterator it = signers.getSigners().iterator();

            
            while (it.hasNext())
            {
                SignerInformation signer = (SignerInformation)it.next();
                X509CertSelector signerConstraints = new JcaX509CertSelectorConverter().getCertSelector(signer.getSID());
                signerConstraints.setKeyUsage(JcaUtils.getKeyUsage(KeyUsage.digitalSignature));
                X509Certificate cert = null;
                
                if(verificarDTO.getCaRootCert() == null){
                    Collection certCollection = certStore.getMatches(signer.getSID());
                    Iterator certIt = certCollection.iterator();
                    X509CertificateHolder certHolder = (X509CertificateHolder)certIt.next();
                    cert = new JcaX509CertificateConverter().setProvider( "BC" ).getCertificate( certHolder );
                    certificate = cert;
                }else{
                    X509Certificate caRootCert = JcaUtils.getCertificado(verificarDTO.getCaRootCert());
                    PKIXCertPathBuilderResult result = JcaUtils.buildPath(caRootCert, signerConstraints, certsAndCRLs);
                    cert = (X509Certificate)result.getCertPath().getCertificates().get(0);
                    certificate = cert;
                }

                try{
                    if(!signer.verify(new JcaSimpleSignerInfoVerifierBuilder().setProvider("BC")
                                                 .build(cert))){
                             resultValidation = false;
                             mensaje="Verificación de Firma Digital Incorrecta";
                             break;
                    } 
                } catch(CMSSignerDigestMismatchException  e){
                    mensaje="El mensaje ha sido modificado al momento de firmar";
                    resultValidation = false;
                    e.printStackTrace();
                }
                
                bytesCertificado = certificate.getEncoded();
            }
        }catch(CertificateEncodingException e)
        {
            mensaje="No se pudo obtener el archivo de certificado";
            resultValidation = false;
            e.printStackTrace();
        }catch(Exception e)
        {
            mensaje="Ha ocurrido un error al momento de verificar el documento";
            resultValidation = false;
            e.printStackTrace();
        }
        
        resultVerificationDTO.setCorrecto(resultValidation);
 
        if(resultValidation){
            resultVerificationDTO.setMensaje("Verificación Correcta");
            resultVerificationDTO.setFechaVigencia(certificate.getNotBefore());
            resultVerificationDTO.setFechaExpiracion(certificate.getNotAfter());
            resultVerificationDTO.setFechaExpiracion(certificate.getNotAfter());
            String emitidoPor = JcaUtils.obtenerCommonName(certificate.getIssuerDN());
            resultVerificationDTO.setEmitidoPor(emitidoPor);
            String propietario = JcaUtils.obtenerCommonName(certificate.getSubjectDN());
            resultVerificationDTO.setPropietario(propietario);
            resultVerificationDTO.setNumeroSerie(certificate.getSerialNumber());
            resultVerificationDTO.setNombreAlgoritmoFirma(certificate.getSigAlgName());
            resultVerificationDTO.setCertificado(bytesCertificado);
        }else{
            resultVerificationDTO.setMensaje(mensaje);
        }
        
        return resultVerificationDTO;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.sign.data;

import java.io.Serializable;

/**
 *
 * @author Luis
 */
public class FirmaPdfDTO implements Serializable{
    private byte[] documento;
    private String pkcs12Password;
    private byte[] pkcs12;
    private byte[] imagenFirma;
    private String email;

    /**
     * @return the documento
     */
    public byte[] getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(byte[] documento) {
        this.documento = documento;
    }

    /**
     * @return the pkcs12Password
     */
    public String getPkcs12Password() {
        return pkcs12Password;
    }

    /**
     * @param pkcs12Password the pkcs12Password to set
     */
    public void setPkcs12Password(String pkcs12Password) {
        this.pkcs12Password = pkcs12Password;
    }

    /**
     * @return the pkcs12
     */
    public byte[] getPkcs12() {
        return pkcs12;
    }

    /**
     * @param pkcs12 the pkcs12 to set
     */
    public void setPkcs12(byte[] pkcs12) {
        this.pkcs12 = pkcs12;
    }

    /**
     * @return the imagenFirma
     */
    public byte[] getImagenFirma() {
        return imagenFirma;
    }

    /**
     * @param imagenFirma the imagenFirma to set
     */
    public void setImagenFirma(byte[] imagenFirma) {
        this.imagenFirma = imagenFirma;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    }

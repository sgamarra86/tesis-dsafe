/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.sign.data;

import java.io.Serializable;

/**
 *
 * @author Luis
 */
public class VerificarDTO implements Serializable{
    private byte[] archivo;
    private byte[] archivoFirmado;
    private byte[] certificadoRaiz = null;

    public byte[] getArchivo() {
        return archivo;
    }

    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }

    public byte[] getArchivoFirmado() {
        return archivoFirmado;
    }

    public void setArchivoFirmado(byte[] archivoFirmado) {
        this.archivoFirmado = archivoFirmado;
    }

    public byte[] getCaRootCert() {
        return certificadoRaiz;
    }

    public void setCaRootCert(byte[] caRootCert) {
        this.certificadoRaiz = caRootCert;
    }
}

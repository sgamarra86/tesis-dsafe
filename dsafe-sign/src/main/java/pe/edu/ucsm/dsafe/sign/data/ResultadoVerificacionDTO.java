/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.sign.data;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 *
 * @author Luis
 */
public class ResultadoVerificacionDTO implements Serializable{
    private String nombreAlgoritmoFirma;
    private Date fechaVigencia;
    private Date fechaExpiracion;
    private String emitidoPor;
    private String propietario;
    private BigInteger numeroSerie;
    private boolean correcto;

    public byte[] getCertificado() {
        return certificado;
    }

    public void setCertificado(byte[] certificado) {
        this.certificado = certificado;
    }
    private String mensaje;
    private byte[] certificado;

    public String getNombreAlgoritmoFirma() {
        return nombreAlgoritmoFirma;
    }

    public void setNombreAlgoritmoFirma(String nombreAlgoritmoFirma) {
        this.nombreAlgoritmoFirma = nombreAlgoritmoFirma;
    }

    public Date getFechaVigencia() {
        return fechaVigencia;
    }

    public void setFechaVigencia(Date fechaVigencia) {
        this.fechaVigencia = fechaVigencia;
    }

    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    public String getEmitidoPor() {
        return emitidoPor;
    }

    public void setEmitidoPor(String emitidoPor) {
        this.emitidoPor = emitidoPor;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public BigInteger getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(BigInteger numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public boolean isCorrecto() {
        return correcto;
    }

    public void setCorrecto(boolean correcto) {
        this.correcto = correcto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
    
}

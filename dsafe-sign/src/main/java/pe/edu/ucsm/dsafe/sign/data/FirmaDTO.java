/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.sign.data;

import java.io.Serializable;

/**
 *
 * @author Luis
 */
public class FirmaDTO implements Serializable{
    private byte[] documento;   //archivo a firmar
    private String pkcs12Password;  //pass en texto plano.
    private byte[] pkcs12;
    private byte[] certificadoRaiz;

    public byte[] getDocumento() {
        return documento;
    }

    public void setDocumento(byte[] documento) {
        this.documento = documento;
    }

    public String getPkcs12Password() {
        return pkcs12Password;
    }

    public void setPkcs12Password(String pkcs12Password) {
        this.pkcs12Password = pkcs12Password;
    }

    public byte[] getPkcs12() {
        return pkcs12;
    }

    public void setPkcs12(byte[] pkcs12) {
        this.pkcs12 = pkcs12;
    }

    public byte[] getCertificadoRaiz() {
        return certificadoRaiz;
    }

    public void setCertificadoRaiz(byte[] certificadoRaiz) {
        this.certificadoRaiz = certificadoRaiz;
    }
    
}

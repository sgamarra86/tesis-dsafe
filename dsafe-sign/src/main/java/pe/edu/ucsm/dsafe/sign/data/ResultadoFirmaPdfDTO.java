/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.sign.data;

import java.io.Serializable;

/**
 *
 * @author Luis
 */
public class ResultadoFirmaPdfDTO implements Serializable{
    private boolean correcto;
    private String mensaje;
    byte[] pdfFirmado;

    public boolean isCorrecto() {
        return correcto;
    }

    public void setCorrecto(boolean correcto) {
        this.correcto = correcto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public byte[] getPdfFirmado() {
        return pdfFirmado;
    }

    public void setPdfFirmado(byte[] pdfFirmado) {
        this.pdfFirmado = pdfFirmado;
    }
    
    
}

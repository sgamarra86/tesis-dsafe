/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.sign.data;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Luis
 */
public class PrepararFirmaPdfDTO implements Serializable{
    private byte[] documento;
    private List<FirmanteDTO> firmantes;

    public byte[] getDocumento() {
        return documento;
    }

    public void setDocumento(byte[] documento) {
        this.documento = documento;
    }

    public List<FirmanteDTO> getFirmantes() {
        return firmantes;
    }

    public void setFirmantes(List<FirmanteDTO> firmantes) {
        this.firmantes = firmantes;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.sign.data;

import java.io.Serializable;

/**
 *
 * @author Luis
 */
public class ResultadoFirmaDTO implements Serializable{
    private boolean correcto;
    private String mensaje;
    byte[] firma;

    public boolean isCorrecto() {
        return correcto;
    }

    public void setCorrecto(boolean isCorrecto) {
        this.correcto = isCorrecto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public byte[] getFirma() {
        return firma;
    }

    public void setFirma(byte[] firma) {
        this.firma = firma;
    }

}

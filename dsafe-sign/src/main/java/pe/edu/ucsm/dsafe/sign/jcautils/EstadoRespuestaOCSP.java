/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.ucsm.dsafe.sign.jcautils;

/**
 *
 * @author Luis
 */
public abstract class EstadoRespuestaOCSP {
  public static final int VALIDO = 1;
  public static final int REVOCADO = 2;
  public static final int DESCONOCIDO = 3;
  public static final int NOPROCESABLE = 4;
}

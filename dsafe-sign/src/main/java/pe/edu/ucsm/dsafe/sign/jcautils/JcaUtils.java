package pe.edu.ucsm.dsafe.sign.jcautils;

import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfAnnotation;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Principal;
import java.security.cert.CertPathBuilder;
import java.security.cert.CertStore;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collections;
import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.DLSequence;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.ocsp.BasicOCSPResp;
import org.bouncycastle.cert.ocsp.CertificateID;
import org.bouncycastle.cert.ocsp.CertificateStatus;
import org.bouncycastle.cert.ocsp.OCSPException;
import org.bouncycastle.cert.ocsp.OCSPReq;
import org.bouncycastle.cert.ocsp.OCSPReqBuilder;
import org.bouncycastle.cert.ocsp.OCSPResp;
import org.bouncycastle.cert.ocsp.SingleResp;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.x509.extension.X509ExtensionUtil;
import pe.edu.ucsm.dsafe.sign.data.RespuestaOCSPDTO;

/**
 *
 * @author Luis
 */
public class JcaUtils
{
    
    public static boolean[] getKeyUsage(int mask)
    {
        byte[] bytes = new byte[] { (byte)(mask & 0xff), (byte)((mask & 0xff00) >> 8) };
        boolean[] keyUsage = new boolean[9];

        for (int i = 0; i != 9; i++)
        {
            keyUsage[i] = (bytes[i / 8] & (0x80 >>> (i % 8))) != 0;
        }

        return keyUsage;
    }

    public static PKIXCertPathBuilderResult buildPath(
        X509Certificate  rootCert,
        X509CertSelector endConstraints,
        CertStore certsAndCRLs)
        throws Exception
    {
        CertPathBuilder       builder = CertPathBuilder.getInstance("PKIX", "BC");
        PKIXBuilderParameters buildParams = new PKIXBuilderParameters(Collections.singleton(new TrustAnchor(rootCert, null)), endConstraints);
        buildParams.addCertStore(certsAndCRLs);
        buildParams.setRevocationEnabled(false);
        
        return (PKIXCertPathBuilderResult)builder.build(buildParams);
    }
    
    
    public static RespuestaOCSPDTO verificarRevocacion(X509Certificate  subjectCert,X509Certificate  rootCert){
        RespuestaOCSPDTO respuestaOCSPDTO = new RespuestaOCSPDTO();
        boolean correcto = false;
        String mensaje = ""; 
        try{
            //Generamos el ID del cetificado que estamos buscando
            final CertificateID id = new CertificateID(
                    new JcaDigestCalculatorProviderBuilder().setProvider("BC").build().get(CertificateID.HASH_SHA1), 
                    new X509CertificateHolder(rootCert.getEncoded()), subjectCert.getSerialNumber());
            final OCSPReqBuilder ocspPeticionBuilder = new OCSPReqBuilder();
            ocspPeticionBuilder.addRequest(id);

            final OCSPReq ocspPeticion = ocspPeticionBuilder.build();

            //Se establece la conexión con el OCSP responder
            final URL url = new URL(obtenerUrlOcsp(subjectCert));
            final HttpURLConnection con = (HttpURLConnection)url.openConnection();

            //Se configuran las propiedades de la petición HTTP
            con.setRequestProperty("Content-Type", "application/ocsp-request");
            con.setRequestProperty("Accept", "application/ocsp-response");
            con.setDoOutput(true);
            final OutputStream out = con.getOutputStream();
            final DataOutputStream dataOut = new DataOutputStream(new BufferedOutputStream(out));

            //Se obtiene la respuesta del servidor OCSP
            dataOut.write(ocspPeticion.getEncoded());
            dataOut.flush();
            dataOut.close();

            //Se parsea la respuesta y se obtiene el estado del certificado retornado por el OCSP
            final InputStream in = (InputStream)con.getContent();
            final OCSPResp ocspRespuesta = new OCSPResp(in);
            int estado = ocspRespuesta.getStatus();

            //Analizamos la respuesta
            if (estado==OCSPResp.SUCCESSFUL) {
                int resultadoOCSP = tratarRespuestaOK(ocspRespuesta);
                
                if(resultadoOCSP == EstadoRespuestaOCSP.VALIDO)
                {
                    mensaje = "El certificado es Válido";
                    correcto = true;
                }else if(resultadoOCSP == EstadoRespuestaOCSP.REVOCADO){
                    mensaje = "El certificado fué Revocado";
                }else if(resultadoOCSP == EstadoRespuestaOCSP.DESCONOCIDO){
                    mensaje = "El estado del certificado es Desconocido";
                }else if(resultadoOCSP == EstadoRespuestaOCSP.NOPROCESABLE){
                    mensaje = "No se pudo procesar la respuesta del Servidor OCSP";
                }
                
            } else if (estado==OCSPResp.MALFORMED_REQUEST) {
                mensaje = "Petición OCSP mal formada";
            } else if (estado==OCSPResp.INTERNAL_ERROR) {
                mensaje = "Error interno en el servidor OCSP";
            } else if (estado==OCSPResp.TRY_LATER) {
                mensaje = "volver a intentar verificación OCSP mas Tarde";
            } else if (estado==OCSPResp.SIG_REQUIRED) {
                mensaje = "petición al Servidor OCSP debe ser firmada";
            } else if (estado==OCSPResp.UNAUTHORIZED) {
                mensaje = "La petición OCSP no esta autorizada";
            } else {
                mensaje = "Respuesata OCSP desconocida";
            }
            
        }catch(Exception e){
             mensaje = "Ocurrió un problema en la petición OCSP";
        }
        respuestaOCSPDTO.setCorrecto(correcto);
        respuestaOCSPDTO.setMensaje(mensaje);
        return respuestaOCSPDTO;
    }
    
    private static int tratarRespuestaOK(final OCSPResp ocspRespuesta) throws OCSPException, IOException {
        final Object responseObject = ocspRespuesta.getResponseObject();
        final BasicOCSPResp basicOCSPResp = (BasicOCSPResp) responseObject;
       
        final SingleResp[] responses = basicOCSPResp.getResponses();
        if (responses.length == 1) {
            final SingleResp resp = responses[0];
            final Object status = resp.getCertStatus();
            if (status == CertificateStatus.GOOD) {
                return EstadoRespuestaOCSP.VALIDO;
            }
            else if (status instanceof org.bouncycastle.cert.ocsp.RevokedStatus) {
                return EstadoRespuestaOCSP.REVOCADO;
            }
            else {
                return EstadoRespuestaOCSP.DESCONOCIDO;
                
            }
        }
        //"Se obtuvieron mas respuestas de lo esperado";
        return EstadoRespuestaOCSP.NOPROCESABLE;
    }
    
    
    private static String obtenerUrlOcsp(X509Certificate certificate) throws Exception {
        byte[] octetBytes = certificate
                .getExtensionValue(X509Extension.authorityInfoAccess.getId());

        DLSequence dlSequence = null;
        ASN1Encodable asn1Encodable = null;

        try {
            ASN1Primitive fromExtensionValue = X509ExtensionUtil
                    .fromExtensionValue(octetBytes);
            if (!(fromExtensionValue instanceof DLSequence))
                return null;
            dlSequence = (DLSequence) fromExtensionValue;
            for (int i = 0; i < dlSequence.size(); i++) {
                asn1Encodable = dlSequence.getObjectAt(i);
                if (asn1Encodable instanceof DLSequence)
                    break;
            }
            if (!(asn1Encodable instanceof DLSequence))
                return null;
            dlSequence = (DLSequence) asn1Encodable;
            for (int i = 0; i < dlSequence.size(); i++) {
                asn1Encodable = dlSequence.getObjectAt(i);
                if (asn1Encodable instanceof DERTaggedObject)
                    break;
            }
            if (!(asn1Encodable instanceof DERTaggedObject))
                return null;
            DERTaggedObject derTaggedObject = (DERTaggedObject) asn1Encodable;
            byte[] encoded = derTaggedObject.getEncoded();
            if (derTaggedObject.getTagNo() == 6) {
                int len = encoded[1];
                return new String(encoded, 2, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static X509Certificate getCertificado (byte[] bytesCertificate)
    {
       X509Certificate certificate = null;
       try{
        ByteArrayInputStream bais = new ByteArrayInputStream(bytesCertificate);
        
        CertificateFactory cf = CertificateFactory.getInstance("X.509");

        certificate = (X509Certificate)cf.generateCertificate(bais);
        }catch(Exception e){
            e.printStackTrace();
        }
       return certificate;
    }
    
    public static String obtenerCommonName(Principal principal)
    {
       String commonName = ""; 
       try{ 
            LdapName dn = new LdapName(principal.getName());
            for (int i = 0; i < dn.size(); i++) {
                String atribute = dn.get(i);
                if(atribute.startsWith("CN=")||atribute.startsWith("cn=")||atribute.startsWith("Cn="))
                {
                    String[] atributeArray = atribute.split("=");
                    commonName = atributeArray[1];
                }
            }
       }catch(InvalidNameException  e) {
            commonName = "No se pudo parsear el nombre del atributo";
       }
       return commonName;
    }
    
    public static PdfPCell crearContenedoresFirma(PdfStamper pdfStamper, String name) {
        PdfWriter writer = pdfStamper.getWriter();
	PdfPCell cell = new PdfPCell();
	cell.setMinimumHeight(50);
	PdfFormField field = PdfFormField.createSignature(writer);
        field.setFieldName(name);
	field.setFlags(PdfAnnotation.FLAGS_PRINT);
	cell.setCellEvent(new MySignatureFieldEvent(field,pdfStamper));
	return cell;
    }

    public static class MySignatureFieldEvent implements PdfPCellEvent {
            public PdfFormField field;
            public PdfStamper pdfStamper;
            public MySignatureFieldEvent(PdfFormField field, PdfStamper pdfStamper) {
                    this.field = field;
                    this.pdfStamper = pdfStamper;
            }

            public void cellLayout(PdfPCell cell, Rectangle position,
                    PdfContentByte[] canvases) {
                    PdfWriter writer = canvases[0].getPdfWriter();
                    field.setPage();
                    field.setWidget(position, PdfAnnotation.HIGHLIGHT_INVERT);
                    pdfStamper.addAnnotation(field, 1);
            }
    }
    
}